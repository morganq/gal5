const Door = (function() {
  ACTIVE_RANGE = 18
  
  function ctor(state, x, y, tx, ty, direction, key) {
    let self = new Phaser.Sprite(g, x, y, key || "door")
    self.active = false
    
    self.UI = new Phaser.BitmapText(g, 0, 0, "visitor", "[A] enter", 10)
    self.UI.anchor.set(0.5, 0.5)
    self.addChild(self.UI)
    self.UI.x = self.width/2
    self.UI.y = -10
    self.UI.tint = 0xbb499a
    self.UI.visible = false
    
    self.presentChoice = function() {
      self.UI.visible = true
    }
    
    self.revokeChoice = function() {
      self.UI.visible = false
    }
    
    self.update = function() {
      let dx = state.player.x - (self.x + self.width/2)
      let dy = state.player.y - (self.y + self.height/2)
      let d = dx*dx+dy*dy
      if(d <= ACTIVE_RANGE ** 2) {
        if(self.active) {
          if(g.useButton.downDuration(1)) {
            g.useButton.reset()
            self.activate()
            //self.exists=false
          }
        } else {
          self.presentChoice()
          self.active = true
          state.mapGrp.showRoomArrow(direction)
        }
      } else {
        if(self.active) {
          self.revokeChoice()
          self.active = false
          state.mapGrp.hideRoomArrow()
        }
      }
      if(self.active) {
        self.UI.y = -10 + Math.sin(g.now / 300) * 2
        
      }
      if(!state.player.justStartedLevel) {
        self.frame = 0
      }  
    }
    
    self.activate = function() {
      state.changeRooms(tx, ty, direction)      
    }
    
    return self
  }
  
  return ctor
})()
