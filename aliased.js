const Aliased = (function() {
  let self = {}
  
  self.drawLine = function(gfx, color, x1, y1, x2, y2) {
    x1 = Math.floor(x1); y1 = Math.floor(y1); x2 = Math.floor(x2); y2 = Math.floor(y2)
    let dx = Math.abs(x2-x1)
    let sx = (x1 < x2) ? 1 : -1
    let dy = Math.abs(y2-y1)
    let sy = (y1 < y2) ? 1 : -1
    let err = (dx > dy ? dx : -dy) /2
    let e2 = 0
    
    gfx.beginFill(color, 1)
    
    while(true) {
      gfx.drawRect(x1, y1, 1, 1)
      if(x1 == x2 && y1 == y2) { break }
      e2 = err
      if(e2 > -dx) {
        err -= dy
        x1 += sx
      }
      if(e2 < dy) {
        err += dx
        y1 += sy
      }
    }
    gfx.endFill()
  }
  
  return self
})()
