const EnemyPsychic = (function() {
  const DOWNFORCE = 500;
  const CRAWLSPEED = 30;
  const TARGET_RANGE = 150;
  const MIN_TARGET_ANGLE_ROTATE_SPEED = 0.0003
  const MAX_TARGET_ANGLE_ROTATE_SPEED = 0.005
  const MIN_TARGET_FIRE_TIME_MS = 1000
  const PREFIRE_FADE_TIME = 650

  const MOVES = [
[{"time":117,"btn":"jump","event":"down"},{"time":133,"btn":"jump","event":"down"},{"time":217,"btn":"jump","event":"up"},{"time":800,"btn":"jump","event":"down"},{"time":817,"btn":"jump","event":"down"},{"time":867,"btn":"jump","event":"up"},{"time":1400,"btn":"jump","event":"down"},{"time":1417,"btn":"jump","event":"down"},{"time":1467,"btn":"jump","event":"up"},{"time":1533,"btn":"right","event":"down"},{"time":1550,"btn":"right","event":"down"},{"time":1733,"btn":"right","event":"up"},{"time":1750,"btn":"left","event":"down"},{"time":1767,"btn":"left","event":"down"},{"time":1867,"btn":"left","event":"up"},{"time":1867,"btn":"right","event":"down"},{"time":1883,"btn":"right","event":"down"},{"time":1917,"btn":"right","event":"up"}],

[{"time":100,"btn":"fire","event":"down"},{"time":117,"btn":"fire","event":"down"},{"time":133,"btn":"fire","event":"down"},{"time":150,"btn":"fire","event":"down"},{"time":267,"btn":"fire","event":"down"},{"time":283,"btn":"fire","event":"down"},{"time":300,"btn":"fire","event":"down"},{"time":317,"btn":"fire","event":"down"},{"time":333,"btn":"fire","event":"down"},{"time":350,"btn":"fire","event":"down"},{"time":400,"btn":"right","event":"down"},{"time":417,"btn":"right","event":"down"},{"time":433,"btn":"right","event":"down"},{"time":450,"btn":"right","event":"down"},{"time":533,"btn":"fire","event":"down"},{"time":550,"btn":"fire","event":"down"},{"time":567,"btn":"fire","event":"down"},{"time":583,"btn":"fire","event":"down"},{"time":600,"btn":"fire","event":"down"},{"time":733,"btn":"fire","event":"down"},{"time":750,"btn":"fire","event":"down"},{"time":767,"btn":"fire","event":"down"}],

[{"time":33,"btn":"roll","event":"down"},{"time":50,"btn":"roll","event":"down"},{"time":67,"btn":"roll","event":"down"},{"time":83,"btn":"roll","event":"down"},{"time":100,"btn":"roll","event":"down"},{"time":117,"btn":"roll","event":"down"},{"time":134,"btn":"roll","event":"down"},{"time":150,"btn":"roll","event":"down"},{"time":450,"btn":"fire","event":"down"},{"time":467,"btn":"fire","event":"down"},{"time":484,"btn":"fire","event":"down"},{"time":617,"btn":"fire","event":"down"},{"time":634,"btn":"fire","event":"down"},{"time":650,"btn":"fire","event":"down"},{"time":667,"btn":"fire","event":"down"}],

[{"time":217,"btn":"roll","event":"down"},{"time":234,"btn":"roll","event":"down"},{"time":250,"btn":"roll","event":"down"},{"time":267,"btn":"roll","event":"down"},{"time":284,"btn":"roll","event":"down"},{"time":300,"btn":"roll","event":"down"},{"time":317,"btn":"roll","event":"down"},{"time":334,"btn":"roll","event":"down"},{"time":350,"btn":"roll","event":"down"},{"time":367,"btn":"roll","event":"down"},{"time":967,"btn":"right","event":"down"},{"time":984,"btn":"right","event":"down"},{"time":1000,"btn":"right","event":"down"},{"time":1017,"btn":"right","event":"down"},{"time":1034,"btn":"right","event":"down"},{"time":1050,"btn":"right","event":"down"},{"time":1067,"btn":"right","event":"down"},{"time":1084,"btn":"right","event":"down"},{"time":1100,"btn":"right","event":"down"},{"time":1117,"btn":"right","event":"down"},{"time":1134,"btn":"right","event":"down"},{"time":1200,"btn":"roll","event":"down"},{"time":1217,"btn":"roll","event":"down"},{"time":1234,"btn":"roll","event":"down"},{"time":1250,"btn":"roll","event":"down"},{"time":1267,"btn":"roll","event":"down"},{"time":1284,"btn":"roll","event":"down"},{"time":1300,"btn":"roll","event":"down"},{"time":1317,"btn":"roll","event":"down"}],
[{"time":100,"btn":"left","event":"down"},{"time":150,"btn":"left","event":"up"},{"time":150,"btn":"right","event":"down"},{"time":233,"btn":"right","event":"up"},{"time":250,"btn":"left","event":"down"},{"time":317,"btn":"left","event":"up"},{"time":317,"btn":"right","event":"down"},{"time":400,"btn":"left","event":"down"},{"time":400,"btn":"right","event":"up"},{"time":483,"btn":"left","event":"up"},{"time":483,"btn":"right","event":"down"},{"time":567,"btn":"left","event":"down"},{"time":567,"btn":"right","event":"up"},{"time":650,"btn":"left","event":"up"},{"time":650,"btn":"right","event":"down"},{"time":717,"btn":"left","event":"down"},{"time":717,"btn":"right","event":"up"},{"time":783,"btn":"left","event":"up"}]
]

  const STATES = {
    CRAWLING:"crawling",
    TARGETING:"targeting",
    FIRE:"fire",
    MINDCONTROLLING:"mindcontrolling"
  }

  function ctor(state, x, y) {
    let self = EnemyBase(state, x + 8, y - 8, "enemypsychic", 0)
    self.body.setSize(32,32, 0, 0)
    self.body.anchor = new Phaser.Point(0.5,0.5)
    g.slopes.enable(self)
    self.animations.add('crawl', [0,1,2,3,4,5,6,7], 8, true)
    self.animations.play('crawl')

    self.brain = new Phaser.Sprite(g, x, y, "enemypsychic-brain", 0)
    self.brain.anchor = new Phaser.Point(0.5, 0.5)
    self.brain.update = function() {
      if(!self || !self.exists) { self.brain.exists = false; }
      self.brain.x = self.x + self.scale.x * -3 + Math.sin(g.now / 310) * 0.75;
      self.brain.y = self.y - 6+ Math.cos(g.now / 510) * 1.5;
      //self.brain.angle = Math.round(Math.cos(g.now / 620) * 2) * 10;
      self.brain.scale.x = self.scale.x
    }
    self.brain.update()

    self.laser = new Phaser.Graphics(g, TARGET_RANGE * 2, TARGET_RANGE * 2);
    self.laser.anchor = new Phaser.Point(0.5, 0.5)
    self.brain.addChild(self.laser)
    self.laser.x = 0
    self.laser.y = 0

    self.targetStartTime = 0

    self.direction = 1;
    self.debugTiles = [null,null];

    self.targetAngle = 0
    self.controllingPlayer = false
    self.controlStartTime = 0
    self.controlMoves = []

    self.state = STATES.CRAWLING

    self.enemyUpdate = function() {
      g.physics.arcade.collide(self, state.tilemapLayer)
      self.desiredVelocity = self.body.velocity
      self.desiredVelocity.y += DOWNFORCE * g.time.elapsed / 1000.0

      let nextState = self.state

      if(self.state == STATES.CRAWLING) {
        self.laser.clear()
        self.desiredVelocity.x = CRAWLSPEED * self.direction

        let downright = state.tilemapLayer.getTileXY(self.right + 1, self.bottom + 1, new Phaser.Point())
        let downleft = state.tilemapLayer.getTileXY(self.left - 1, self.bottom + 1, new Phaser.Point())
        let left = Math.min(self.left, self.right)
        let right = Math.max(self.left, self.right)
        function hasTile(x,y) {
          let tiles = state.tilemapLayer.getTiles(x, y, 1, 1)
          if(tiles.length == 0) { return false }
          if(tiles[0].index < 0 || tiles[0].index > 38) { return false }
          return true
        }
        if(self.direction == -1 &&
          (!hasTile(left, self.bottom+8) ||
          hasTile(left, self.bottom-8))) {
          self.direction = 1
        }
        else if (self.direction == 1 &&
          (!hasTile(right, self.bottom+8) ||
          hasTile(right, self.bottom-8))) {
          self.direction = -1
        }

        if(g.now > self.targetStartTime + 4000) {
          // Try to find player
          if(g.math.distanceSq(self.x, self.y, state.player.x, state.player.y) < TARGET_RANGE * TARGET_RANGE) {
            if((self.direction == 1 && state.player.x > self.x) || (self.direction == -1 && state.player.x < self.x)) {
              let rayCast = state.tilemapLayer.getRayCastTiles(
                new Phaser.Line(self.x, self.y, state.player.x, state.player.y),
                20, true)
              if(rayCast.length == 0) {
                  nextState = STATES.TARGETING
                  self.targetStartTime = g.now
              }
            }
          }
        }
        self.targetAngle = (self.direction > 0) ? 0 : 3.14159
      }

      if(self.state == STATES.TARGETING) {
        self.animations.stop()
        self.desiredVelocity.x = 0
        let eventualTargetX = state.player.x
        let eventualTargetY = state.player.y
        let rayCast = state.tilemapLayer.getRayCastTiles(
          new Phaser.Line(self.x, self.y, state.player.x, state.player.y),
          20, true)
        let eventualTargetAngle = Math.atan2(eventualTargetY - self.y, eventualTargetX - self.x)
        let angleDist = g.math.wrapAngle(self.targetAngle - eventualTargetAngle, true)
        if(rayCast.length == 0) {
          let rotateSpeed = Math.abs(angleDist) / 3.14159 * (MAX_TARGET_ANGLE_ROTATE_SPEED-MIN_TARGET_ANGLE_ROTATE_SPEED) + MIN_TARGET_ANGLE_ROTATE_SPEED
          console.log(rotateSpeed)
          let frameRotate = rotateSpeed * g.time.elapsed
          if(angleDist < -frameRotate) {
            self.targetAngle += frameRotate
          } else if(angleDist > frameRotate){
            self.targetAngle -= frameRotate
          } else {
            self.targetAngle = eventualTargetAngle
          }
        }
        targetX = self.brain.x + Math.cos(self.targetAngle) * 900
        targetY = self.brain.y + Math.sin(self.targetAngle) * 900
        let alpha = 0.5
        alpha -= g.math.clamp((g.now - (self.targetStartTime + MIN_TARGET_FIRE_TIME_MS) + PREFIRE_FADE_TIME) / PREFIRE_FADE_TIME, 0, 1) * 0.5
        self.laser.clear()
        self.laser.lineStyle(2,0xff00ff, alpha)
        self.laser.moveTo(0, 0)
        self.laser.lineTo((targetX - self.brain.x) * self.scale.x, targetY - self.brain.y)

        if(g.now > self.targetStartTime + MIN_TARGET_FIRE_TIME_MS) {
          if(Math.abs(angleDist) < 0.5) {
            console.log("FIRE")
            nextState = STATES.FIRE
            self.fireTime = g.now
            if(angleDist < 0.2 && rayCast.length == 0) {
              self.controllingPlayer = true
              self.controlStartTime = g.now
              self.controlMoves = MOVES[Math.floor(g.rnd.frac() * MOVES.length)].slice()
              state.player.resetButtons()
            }
          }
          else {
            nextState = STATES.CRAWLING
            self.animations.play("crawling")
          }
        }
      }

      if(self.state == STATES.FIRE) {
        self.desiredVelocity.x = 0
        targetX = self.brain.x + Math.cos(self.targetAngle) * 900
        targetY = self.brain.y + Math.sin(self.targetAngle) * 900
        self.laser.clear()
        self.laser.lineStyle(2,
          Phaser.Color.getColor(
            Math.floor(g.math.clamp(128 + (g.now - self.fireTime),0,255)),
            0,
            255),
            Math.max(1.0 - (g.now - self.fireTime) / 500,0))
        self.laser.moveTo(0, 0)
        self.laser.lineTo((targetX - self.brain.x) * self.scale.x, targetY - self.brain.y)
        if(g.now > self.fireTime + 500) {
          nextState = STATES.CRAWLING
          self.animations.play("crawling")
        }
      }

      if(self.controllingPlayer) {
        state.player.mindControl = self
        if(self.controlMoves.length > 0) {
          let nextMove = self.controlMoves[0]
          while(nextMove && nextMove.time <= g.now - self.controlStartTime) {
            if(nextMove.event=="down") { state.player.buttons[nextMove.btn] = true }
            if(nextMove.event=="up") { state.player.buttons[nextMove.btn] = false }
            self.controlMoves.shift()
            nextMove = self.controlMoves[0]
          }
        } else {
          state.player.mindControl = null
          state.player.resetButtons()
        }
      } else {
        state.player.mindControl = null
      }

      if(nextState != self.state) {
        self.state = nextState
      }
      self.scale.x = self.direction
    }

    self.render = function() {
    }

    return self
  }

  return ctor

})()
