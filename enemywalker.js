const EnemyWalker = (function() {
  const CW = 0
  const CCW = 1
  const GRAVITY = 10
  const WALK_SPEED = 20
  const CLIMB_SPEED = 15
  const SLIDE_SPEED = 40
  const HANG_SPEED = 12
  const ATTACK_RANGE = 50
  const ATTACK_HEIGHT_DIFF = 15
  const ATTACK_SPEED = 140
  const DIR_RIGHT = 0
  const DIR_DOWN = 1
  const DIR_LEFT = 2
  const DIR_UP = 3
  const XSIZEOFF=4
  
  function ctor(state, enemyId, x, y) {
    let self = EnemyBase(state, enemyId, x, y-4, "enemywalker", 0)
    self.animations.add("walk", [0,1,2,3,4,5], 6, true)
    self.animations.add("climb", [6,7,8,9], 6, true)
    self.animations.add("slide", [6], 6, true)
    self.animations.add("hang", [10,11,12,13,14,15], 6, true)
    self.animations.add("spin", [16], 6, true)
    self.animations.add("hit", [17,18,19], 16, true)
    self.play("walk")
    self.body.setSize(XSIZEOFF*2,16, XSIZEOFF, 0)
    self.body.anchor = new Phaser.Point(0.5,0.5)
    g.slopes.enable(self)

    self.desiredVelocity.x = WALK_SPEED
    self.testNear = []

    self.debugTiles = [null,null,null,null, null, null, null]
    self.triggeredOverlappingTiles = []
    self.overlappedThisTurn = []
    self.careAboutPlatforms = true
    
    self.state = STATES.WALKING
    
    self.lastTurnTime = 0
    self.lastTileTriggered = null
    
    self.turning = false
    self.dir = DIR_RIGHT
    if(state.tilemapLayer.getTiles(x + 8, y, 1, 1, true).length > 0) {
      self.dir = DIR_UP
    }
    self.winding = 1
    self.health = 40
    
    self.attackDistance = 0
    self.nextAttackTime = 0
    
    self.tests = []
    self.hasTouchedBackOutsideTrigger = false
    
    self.create = function(){
      self.addEnemyState('walking')
      self.addEnemyState('changingdirection')
      self.addEnemyState('falling')
      self.addEnemyState('attacking')
      self.addEnemyState('latched')
      self.sm.transition('walking')
    }

    self.getHeadPos = function() {
      return new Phaser.Point(self.x + 8 * self.scale.x, self.y + (self.frame == 0 ? -2 : 2))
    }
    
    self.detectTurns = function() {
      let right = Math.floor(Math.max(self.body.left, self.body.right))
      let left = Math.floor(Math.min(self.body.left, self.body.right))
      let top = Math.floor(self.body.top)
      let bottom = Math.floor(self.body.bottom)
      
      
      self.tests = []
      function test(x,y) {
        self.tests.push(new Phaser.Point(x,y))
        let tiles = state.tilemapLayer.getTiles(Math.floor(x), Math.floor(y), 1, 1)
        if(tiles.length == 0) { return false }
        if(is_colliding_tile_index_all(tiles[0].index)) { return true }
        return false
      }
      
      let to = 4
      
      let startTurnOutsideTests = {
        CW_RIGHT: function(){ return ! test(right, bottom + to) },
        CW_DOWN: function(){ return ! test(left-to, bottom) },
        CW_LEFT: function(){ return ! test(left, top - to) },
        CW_UP: function(){ return ! test(right+to, top) },
        CCW_RIGHT: function(){ return ! test(right, top-to) },
        CCW_DOWN: function(){ return ! test(right+to, bottom) },
        CCW_LEFT: function(){ return ! test(left, bottom + to) },
        CCW_UP: function(){ return ! test(left-to, top) },        
      }      
      
      let finishTurnOutsideTests = {
        CW_RIGHT: function(){ return ! test(left, bottom + to) },
        CW_DOWN: function(){ return ! test(left-to, top) },
        CW_LEFT: function(){ return ! test(right, top - to) },
        CW_UP: function(){ return ! test(right+to, bottom) },
        CCW_RIGHT: function(){ return ! test(left, top-to) },
        CCW_DOWN: function(){ return ! test(right+to, top) },
        CCW_LEFT: function(){ return ! test(right, bottom + to) },
        CCW_UP: function(){ return ! test(left-to, bottom) },        
      }      
      
      let turnInsideTests = {
        CW_RIGHT: function(){ return test(right + 1, bottom - to) },
        CW_DOWN: function(){ return test(left + to, bottom + 1) },
        CW_LEFT: function(){ return test(left - 1, top + to) },
        CW_UP: function(){ return test(right - to, top - 1) },        
        CCW_RIGHT: function(){ return test(right + 1, top + to) },
        CCW_DOWN: function(){ return test(right - to, bottom + 1) },
        CCW_LEFT: function(){ return test(right + 1, bottom - to) },
        CCW_UP: function(){ return test(left + to, top - 1) },          
      }
      
      let testName = (self.winding > 0) ? "CW" : "CCW"
      testName += "_" + ["RIGHT","DOWN","LEFT","UP"][self.dir]
      
      
      if(turnInsideTests[testName]()) {
        //console.log(testName, "shouldTurnInside")
        if(self.winding > 0) {
          self.dir--
        } else {
          self.dir++
        }
      }      
      
      // We have to have started turning before we can actually turn.
      self.turning = startTurnOutsideTests[testName]()
      if(self.turning) {
        //console.log(testName, "startTurningOutside")
        if(!self.hasTouchedBackOutsideTrigger && !finishTurnOutsideTests[testName]()) {
          self.hasTouchedBackOutsideTrigger = true
        } 
        if(self.hasTouchedBackOutsideTrigger && finishTurnOutsideTests[testName]()) {
          self.hasTouchedBackOutsideTrigger = false
          //console.log(testName, "finishTurningOutside")
          if(self.winding > 0) {
            self.dir++
          } else {
            self.dir--
          }
        }
      }
      
      self.dir = self.dir % 4
      if(self.dir < 0) { self.dir += 4 }
      
    }
    
    self.isNearWall = function() {
      let nearWall = false
      // Make sure we're near a tile
      let tiles = state.tilemapLayer.getTiles(
        Math.floor(self.body.left - 4),
        Math.floor(self.body.top - 4), 
        Math.floor(self.body.right - self.body.left) + 8,
        Math.floor(self.body.bottom - self.body.top) + 8)
      tiles.forEach(function(t) {
        if(t && is_colliding_tile_index_all(t.index)) {
          nearWall = true
        }
      }) 
      return nearWall     
    }
    
    self.fallingUpdate = function() {
      self.desiredVelocity.x = self.body.velocity.x * 0.95
      self.desiredVelocity.y = self.body.velocity.y + GRAVITY * 40 * g.elapsed / 1000
      g.physics.arcade.collide(self, state.tilemapLayer)    
      if(self.isNearWall()) {
        self.sm.transition('walking')
      }
    }
    
    self.fallingExit = function() {
      if(self.winding > 0) {
        self.dir = DIR_RIGHT
      } else {
        self.dir = DIR_LEFT
      }
    }
    
    self.walkingEnter = function() {}

    self.walkingUpdate = function() {
      g.physics.arcade.collide(self, state.tilemapLayer)      
      

      if(!self.isNearWall()) {
        self.sm.transition('falling')
      }

      let anim = ""
      switch(self.dir) {
        case DIR_RIGHT:
          if(self.winding == 1) {
            self.desiredVelocity.x = WALK_SPEED
            self.desiredVelocity.y = self.body.velocity.y + GRAVITY * 40 * g.elapsed / 1000
            anim = "walk"
            if(self.sm.debugText) { self.sm.debugText.text = "cw r" }
          } else {
            self.desiredVelocity.x = HANG_SPEED
            self.desiredVelocity.y = self.body.velocity.y + GRAVITY * 40 * g.elapsed / 1000
            anim = "hang"
            if(self.sm.debugText) { self.sm.debugText.text = "ccw r" }
          }
          break
        case DIR_DOWN:
          if(self.winding == 1) {
            self.desiredVelocity.y = g.math.clamp(self.body.velocity.y + 0.05 * g.elapsed,SLIDE_SPEED/4, SLIDE_SPEED)
            self.desiredVelocity.x = self.body.velocity.x - GRAVITY * 40 * g.elapsed / 1000
            anim = "slide"
            if(self.sm.debugText) { self.sm.debugText.text = "cw d" }
          } else {
            self.desiredVelocity.y = g.math.clamp(self.body.velocity.y + 0.05 * g.elapsed,SLIDE_SPEED/4, SLIDE_SPEED)
            self.desiredVelocity.x = self.body.velocity.x + GRAVITY * 40 * g.elapsed / 1000
            anim = "slide"
            if(self.sm.debugText) { self.sm.debugText.text = "ccw d" }
          }
          break
        case DIR_LEFT:
          if(self.winding == 1) {
            self.desiredVelocity.x = -HANG_SPEED
            self.desiredVelocity.y = self.body.velocity.y - GRAVITY * 40 * g.elapsed / 1000
            anim = "hang"
            if(self.sm.debugText) { self.sm.debugText.text = "cw l" }
          } else {
            self.desiredVelocity.x = -WALK_SPEED
            self.desiredVelocity.y = self.body.velocity.y - GRAVITY * 40 * g.elapsed / 1000
            anim = "walk"
            if(self.sm.debugText) { self.sm.debugText.text = "ccw l" }
          }
          break
        case DIR_UP:
          if(self.winding == 1) {
            self.desiredVelocity.y = -CLIMB_SPEED
            self.desiredVelocity.x = self.body.velocity.x + GRAVITY * 40 * g.elapsed / 1000
            anim = "climb"
            if(self.sm.debugText) { self.sm.debugText.text = "cw u" }
          } else {
            self.desiredVelocity.y = -CLIMB_SPEED
            self.desiredVelocity.x = self.body.velocity.x - GRAVITY * 40 * g.elapsed / 1000
            anim = "climb"
            if(self.sm.debugText) { self.sm.debugText.text = "ccw u" }
          }
          break   
      }
      self.play(anim)
        
      self.detectTurns()
      
      if(!state.player.justStartedLevel && 
        g.now > self.nextAttackTime) {
          
        let dx = (state.player.x - self.x)
        let dy = (state.player.y - self.y)
        let dq = dx*dx+dy*dy
        if(dq < ATTACK_RANGE*ATTACK_RANGE) {
          if(state.tilemapLayer.getRayCastTiles(
            new Phaser.Line(self.x, self.y, state.player.x, state.player.y),
            2, true).length == 0) {

              self.sm.transition('attacking')
          }
        }      
      }
      
    }
    
    self.attackingEnter = function() {
      self.animations.paused = true
      self.startAttackPosition = self.position.clone()
      self.desiredVelocity.x = 0
      self.desiredVelocity.y = 0              
      
      self.body.setSize(6, 6, 5, 5)
      self.attackStartTime = g.now + 300
      self.attackEndTime = g.now + 1800
      self.nextAttackTime = g.now + 2500
      let dx = (state.player.x - self.x)
      let dy = (state.player.y - self.y)
      let dq = dx*dx+dy*dy   
      let dist = Math.sqrt(dq)         
      self.attackDX = dx / dist
      self.attackDY = dy / dist      
    }
    
    self.attackingUpdate = function() {
      if(g.now > self.attackStartTime) {
        self.play("spin")
        self.rotation = Math.floor(g.now / 100) * Math.PI/2
        let t = (g.now - self.attackStartTime) / (self.attackEndTime - self.attackStartTime)
        let speed = (1 - t * 2) * ATTACK_SPEED
        self.desiredVelocity.x = self.attackDX * speed
        self.desiredVelocity.y = self.attackDY * speed    
        g.physics.arcade.overlap(self, state.player, function(w,p) {
          if(state.player.canBeHit()) {
            nextState = STATES.LATCHED
            self.body.setSize(48, 48, -16, -16)
          }
        })
        if(t >= 1) {
          self.sm.transition('walking')    
        }
      }      
    }
    
    self.attackingExit = function() {
      self.body.velocity.x = 0
      self.body.velocity.y = 0
      self.desiredVelocity.x = 0
      self.desiredVelocity.y = 0
      self.body.setSize(XSIZEOFF*2,16, XSIZEOFF, 0)
      self.position = self.startAttackPosition.clone()
      self.rotation = 0      
    }

    self.latchedUpdate = function() {
      self.scale.x = state.player.scale.x
      self.x = state.player.x + self.scale.x * 1
      self.y = state.player.y-4
      self.rotation = 0
      self.play("hit")
    }

    self.enemyUpdate = function() {
      if(self.desiredVelocity.x < 0) {
        self.scale.x = -1
      } else if (self.desiredVelocity.x > 0){
        self.scale.x = 1
      }
    }

    self._enemyRender = function() {

      //g.debug.body(walker)
      //if(self.startAttackPosition) {
      //  g.debug.geom(new Phaser.Rectangle(self.startAttackPosition.x, self.startAttackPosition.y, 4, 4), 'rgba(255,0,0,1)')
      //}

      const colors = [
        'rgba(255,0,0,1)',
        'rgba(255,255,0,1)',
        'rgba(0,255,0,1)',
        'rgba(0,255,255,1)',
        'rgba(0,0,255,1)',
        'rgba(255,0,255,1)',
        'rgba(255,255,255,1)',
      ]

      self.tests.forEach(function(t, i) {
          g.debug.geom(new Phaser.Rectangle(t.x, t.y, 2, 2), colors[i])
      })
        
      /*
      self.testNear.forEach(function(t) {
        g.debug.geom(new Phaser.Line(self.x, self.y, t.worldX + 8, t.worldY + 8), 'rgba(0,0,0,1)')
      })
      */
    }

    return self
  }

  return ctor
})()
