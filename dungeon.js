const Dungeon = (function() {
  GW = 7
  GH = 7
  ROOMS = 11
  const ctor = function(floor) {
    let allLevels = ['a','b','c','d','e','f','g','h','i','j','k']
    let self = {}
    self.floor = floor
    self.grid = []
    self.placedRooms = []
    self.rooms = []
    for(let j = 0; j < GH; j++) {
      let row = []
      for(let i = 0; i < GW; i++) {
        row.push(null)
      }
      self.grid.push(row)
    }
    
    let leftMost = null
    let rightMost = null
    let topMost = null
    let bottomMost = null
    self.policeDistance = -6
    
    function placeRoom(x, y) {
      if(leftMost == null || x <= leftMost[0]) { leftMost = [x,y]}
      if(rightMost == null || x >= rightMost[0]) { rightMost = [x,y]}
      if(topMost == null || y <= topMost[1]) { topMost = [x,y]}
      if(bottomMost == null || y >= bottomMost[1]) { bottomMost = [x,y]}
      let chosenEnemies = []
      for(let i = 0; i < 30; i++) {
        chosenEnemies.push(g.rnd.between(0, 999))
      }
      const SELECTION=['common','common','uncommon','uncommon','rare']
      let storeSelection = []
      for(let i = 0; i < 5; i++) {
        storeSelection.push(g.getRandomSticker(self.floor, SELECTION[i], true))
      }
      let levelIndex = g.rnd.between(0, allLevels.length-1)
      let level = allLevels[levelIndex]
      allLevels.splice(levelIndex, 1)
      let newSpace = {
        level:'level_' + level,
        //'level':'level_h',
        status:{deadEnemies:[], treasuresLeft:2},
        storeSelection:storeSelection,
        treasureSpawn:g.rnd.between(0, 9999),
        enemyRegions:chosenEnemies,
        x:x,
        y:y,
        kind:'normal',
        hasPolice:false,
        left:false,
        right:false,
        up:false,
        down:false
      }
      let leftTile = null; if(x > 0) { leftTile = self.grid[y][x-1]}
      let rightTile = null; if(x < GW-1) { rightTile = self.grid[y][x+1]}
      let upTile = null; if(y > 0) { upTile = self.grid[y-1][x]}
      let downTile = null; if(y < GH-1) { downTile = self.grid[y+1][x]}
      if(leftTile) { newSpace.left=true; leftTile.right=true }
      if(rightTile) { newSpace.right=true; rightTile.left=true }
      if(upTile) { newSpace.up=true; upTile.down=true }
      if(downTile) { newSpace.down=true; downTile.up=true }
      self.grid[y][x] = newSpace
      self.rooms.push(newSpace)
      self.placedRooms.push([x, y])
    }
    
    placeRoom(3,3)
    if(floor == 0) {
      self.rooms[0].level = 'level_lobby'
      self.x = 3
      self.y = 3
    } else {
      
      q = 0
      
      while(self.placedRooms.length < ROOMS) {
        let randomRoom
        /*
        if(self.placedRooms.length < 3){
          randomRoom = self.placedRooms[0]
        } else {
          randomRoom = g.rnd.pick(self.placedRooms)
        }
        */
        randomRoom = [leftMost, rightMost, bottomMost, topMost][q % 4]
        let spots = []
        
        x = randomRoom[0]
        y = randomRoom[1]
        
        if(x > 0 && self.grid[y][x-1] == null) { spots.push([x-1, y]) }
        
        if(y > 0 && self.grid[y-1][x] == null) { spots.push([x, y-1]) }
        
        if((x < GW - 1) && self.grid[y][x+1] == null) { spots.push([x+1, y]) }
        
        if((y < GH - 1) && self.grid[y+1][x] == null) { spots.push([x, y+1]) }
        
        let nextSpot = g.rnd.pick(spots)
        if(nextSpot) {
          placeRoom(nextSpot[0], nextSpot[1])
        }
        q += 1
      }
      
      let spawn = self.grid[leftMost[1]][leftMost[0]]
      spawn.kind = "spawn"
      self.spawn = spawn
      self.grid[rightMost[1]][rightMost[0]].kind = "boss"
      self.grid[rightMost[1]][rightMost[0]].level = "level_boss"
      self.boss = self.grid[rightMost[1]][rightMost[0]]
      //self.x = rightMost[0] // boss spawn
      //self.y = rightMost[1]    
      self.x = leftMost[0]
      self.y = leftMost[1]    
      let store = g.rnd.pick(self.rooms.filter(function(rm) { return rm.kind == "normal" }))
      store.level="level_store"
      store.kind="store"
      store.status.storePurchases = []
      
      let registry = g.rnd.pick(self.rooms.filter(function(rm) { return rm.kind == "normal" }))
      registry.level="level_registry"
      registry.kind="registry"    
      //self.x = registry.x
      //self.y = registry.y
      
      //self.x = store.x ; self.y = store.y
      let rare = g.rnd.pick(self.rooms.filter(function(rm) { return rm.kind == "normal" }))
      rare.kind="rare"
      /*let rare2 = g.rnd.pick(self.rooms.filter(function(rm) { return rm.kind == "normal" }))
      rare2.kind="rare"    
      */
    }
    
    self.saveDeath = function(enemyId) {
      let room = self.getRoom()
      room.status.deadEnemies.push(enemyId)
    }
    
    self.savePurchase = function() {
      let room = self.getRoom()
      room.status.treasuresLeft -= 1
    }
    self.saveStorePurchase = function(i) {
      let room = self.getRoom()
      room.status.storePurchases.push(i)
    }    
    
    self.instantiateMap = function() {
      let group = new Phaser.Group(g)
      let minX = leftMost[0]
      let minY = topMost[1]
      for(let i = 0; i < self.rooms.length; i++) {
        let room = self.rooms[i]
        let spr = new Phaser.Sprite(g, (room.x - minX) * 9, (room.y - minY) * 9, 'mapicons', 0)
        if(room.kind == 'spawn') { spr.frame = 4 }
        if(room.kind == 'store') { spr.frame = 1 }
        if(room.kind == 'boss') { spr.frame = 2 }
        if(room.kind == 'rare') { spr.frame = 3 }
        if(room.kind == 'registry') { spr.frame = 5 }
        group.addChild(spr)
        
        if(self.floor > 0 && room.hasPolice) {
          let popo = new Phaser.Sprite(g, (room.x - minX) * 9, (room.y - minY) * 9, 'policemap')
          group.addChild(popo)
        }
      }
      let here = new Phaser.Sprite(g, (self.x - minX) * 9 -1, (self.y - minY) * 9 - 1, 'youarehere')
      here.animations.add('go', [0,1,2],6,true)
      here.play('go')
      group.addChild(here)
      
      if(self.floor > 0 && self.policeDistance < 0) {
        let x = -12
        let y = (self.spawn.y - minY) * 9 - 2
        let p = new Phaser.Sprite(g, x, y, 'policenumber')
        group.addChild(p)
        let policeCounter = new Phaser.BitmapText(g, x+6, y+1, "visitor", -self.policeDistance, 10)
        policeCounter.anchor.set(0.5, 0)
        policeCounter.tint = 0xffffff
        group.addChild(policeCounter)
      }
      
      let arrow = new Phaser.Sprite(g, 0, 0, 'map-arrow')
      arrow.visible = false
      arrow.anchor.set(-0.25, 0.5)
      group.addChild(arrow)
      
      group.hideRoomArrow = function() {
        arrow.visible = false
      }
      
      group.showRoomArrow = function(direction) {
        arrow.visible = true
        arrow.x = (self.getRoom().x - minX) * 9 + 4
        arrow.y = (self.getRoom().y - minY) * 9 + 4
        if(direction == 'left') {
          arrow.angle = 180
        }
        if(direction == 'right') {
          arrow.angle = 0
        }
        if(direction == 'up') {
          arrow.angle = -90
        }
        if(direction =='down') {
          arrow.angle = 90
        }
      }
      
      /*here.update = function() {
        //here.y = Math.floor((self.y - minY) * 9 + Math.sin(g.now / 200) * 1.5)
        here.alpha = Math.sin(g.now / 200) * 0.3 + 0.7
      }*/
      
      return group
    }
    
    self.getRoom = function() {
      return self.grid[self.y][self.x]
    }
    
    self.move = function(x, y) {
      if(self.grid[y][x] == null) {
        console.log("CANNOT MOVE THERE!!!!")
      }
      self.x = x
      self.y = y
    }
    self.tickPolice = function() {
      self.policeDistance += 1
      if(self.policeDistance == 0) {
        self.spawn.hasPolice = true
      } else if (self.policeDistance > 0) {
        self.rooms.filter(function(r) { return r.hasPolice }).forEach(function(room) {
          if(room.up) { self.grid[room.y-1][room.x].hasPolice = true }
          if(room.down) { self.grid[room.y+1][room.x].hasPolice = true }
          if(room.left) { self.grid[room.y][room.x-1].hasPolice = true }
          if(room.right) { self.grid[room.y][room.x+1].hasPolice = true }
        })
      }
    }
    
    return self
  }
  return ctor
})()
