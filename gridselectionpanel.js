const GridSelectionPanel = (function() {
  function ctor(state, menu, width, height, cellWidth, cellHeight) {
    let self = new Phaser.Group(g)
    self.menu = menu
    
    self.maxHeight = height
    self.maxWidth = width
    
    self.staticCellWidth = cellWidth || -1
    self.staticCellHeight = cellHeight || -1
    
    self.grid = []
    
    self.xPadding = 4
    self.yPadding = 4
    self.xMargin = 2
    self.yMargin = 2    
    
    self.selector = new Phaser.Point(0,0)
    self.selectionSprite = new Phaser.Graphics(g, 0, 0)
    
    self.scrolling = true
    
    self.active = true
    
    self.getCenterX = function(x) {
      return x * (self.cellWidth + self.xPadding * 2 + self.xMargin * 2) + self.cellWidth / 2 + self.xMargin + self.xPadding
    }
    self.getCenterY = function(y) {
      return y * (self.cellHeight + self.yPadding * 2 + self.yMargin * 2) + self.cellHeight / 2 + self.yMargin + self.yPadding
    }    
    
    self.getSelectedValue = function() {
      let val =self.grid[self.selector.y][self.selector.x]
      return val.value
    }
    
    self.updateSelection = function() {
      self.selectionSprite.x = self.getCenterX(self.selector.x) - self.cellWidth/2 - self.xPadding-1
      self.selectionSprite.y = self.getCenterY(self.selector.y) - self.cellHeight/2 - self.yPadding-1
      if(self.scrolling) {
        if(self.selectionSprite.y + self.y < 0) {
          let finalSelectionY = self.maxHeight - self.selectionSprite.height
          self.y = finalSelectionY - self.selectionSprite.y - self.yMargin
          console.log("went off top, adjusting to", self.y)
        }
        if(self.selectionSprite.y + self.y + self.selectionSprite.height > self.maxHeight) {
          let finalSelectionY = self.yMargin
          self.y = finalSelectionY - self.selectionSprite.y
          console.log("went off bottom, adjusting to", self.y)
        }      
        self.y = g.math.clamp(self.y, -self.h + self.maxHeight, 0)
        console.log(self.h, self.maxHeight, self.height)
      }
    }
    
    self.updateGridElements = function() {
      let cellWidth = 0
      let cellHeight = 0
      self.grid.forEach(function(row) {
        row.forEach(function(cell) {
          cellWidth = Math.max(cell.element.width, cellWidth)
          cellHeight = Math.max(cell.element.height, cellHeight)
        })
      })  
      
      if(self.staticCellWidth >= 0) {
        cellWidth = self.staticCellWidth
      } else {
        cellWidth = Math.min(cellWidth, self.maxWidth)        
      }
      if(self.staticCellHeight >= 0) {
        cellHeight = self.staticCellHeight
      } else {
        cellHeight = Math.min(cellHeight, self.maxHeight)
      }
      
      self.cellWidth = cellWidth
      self.cellHeight = cellHeight
    
      for(let y = 0; y < self.grid.length; y++) {
        for(let x = 0; x < self.grid[y].length; x++) {
          let cell = self.grid[y][x]
          cell.element.x = self.getCenterX(x) - cell.element.width / 2
          cell.element.y = self.getCenterY(y) - cell.element.height / 2
        }
      }
      self.h = self.grid.length * (cellHeight + self.yPadding * 2 + self.yMargin * 2)
      self.w = self.grid[0].length * (cellWidth + self.xPadding * 2 + self.xMargin * 2)
      console.log(cellWidth, self.w, cellHeight, self.h)
      
      self.selectionSprite.clear()
      self.selectionSprite.lineStyle(1, 0xffffff, 1)
      self.selectionSprite.drawRect(0,0,cellWidth + self.xPadding*2,cellHeight + self.yPadding*2)
      self.addChild(self.selectionSprite)
      self.updateSelection()
    }
    
    self.setGrid = function(grid) {
      self.removeAll()
      self.grid = grid
      self.grid.forEach(function(row) {
        row.forEach(function(cell) {
          self.addChild(cell.element)
        })
      })
      self.updateGridElements()
      self.selector.set(0,0)
    }
    
    self.update = function() {
      if(self.active) {
        self.selectionSprite.tint = 0xd01f02
        if(g.cursors.left.downDuration(1)) {
          self.selector.x = self.selector.x - 1
          if(self.selector.x < 0) { self.selector.x += self.grid[self.selector.y].length }
          self.updateSelection()
        }
        if(g.cursors.right.downDuration(1)) {
          self.selector.x = (self.selector.x + 1) % (self.grid[self.selector.y].length)
          self.updateSelection()
        }
        if(g.cursors.up.downDuration(1)) {
          self.selector.y = self.selector.y - 1
          if(self.selector.y < 0) { self.selector.y += self.grid.length }
          self.updateSelection()
        }
        if(g.cursors.down.downDuration(1)) {
          self.selector.y = (self.selector.y + 1) % (self.grid.length)
          self.updateSelection()
        }          
      } else {
        self.selectionSprite.tint = 0xa2a091
      }
          
    }
    
    return self
  }
  
  return ctor
})()
