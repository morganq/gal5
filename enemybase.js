EnemyBase = (function() {
  STATES = {
    DEAD:"dead"
  }

  function ctor(state, enemyId, x, y, key, frame) {
    let self = new Phaser.Sprite(g, x, y, key, frame)
    self.careAboutPlatforms = false
    self.anchor = new Phaser.Point(0.5, 0.5)
    g.physics.enable(self)
    g.slopes.enable(self)

    // Desired velocity
    self.desiredVelocity = new Phaser.Point(0,0)

    // Getting hit
    self.hittable = true
    self.hitRecoverMs = 20
    self.hitTime = 0
    self.hitVelocity = new Phaser.Point()

    // Health
    self.health = 50
    self.damage = 15
    
    self.grantHealth = 2
    self.grantCredits = 2

    // effects
    self.effects = []
    
    let levelHack = true
    
    self.shadow = Shadow(state, self, self.width - 4)
    
    self.sm = StateMachine(false)
    //state.UILayer.add(self.sm.debugText)
    
    self.preHitPackage = {}
    self.preHitState = null
    
    self.gettingHitAnimation = "_gettinghit"
    
    self.addEnemyState = function(name) {
      self.sm.add(name, self[name + 'Enter'], self[name + 'Update'], self[name + "Exit"])
    }
    
    self.addEffect = function(spr) {
      state.gameLayer.addChild(spr)
      self.effects.push(spr)
    }
    
    self.onSaveBeforeHit = function(){}
    self.onRestoreAfterHit = function(){}
    
    self._gettinghitEnter = function() {
      if(self.sm.currentState.name != '_gettinghit') {
        self.preHitState = self.sm.currentState.name
        self.preHitPackage = self.onSaveBeforeHit()
      }
      self.play(self.gettingHitAnimation)
      self.hitTime = g.now
    }
    
    self._gettinghitUpdate = function() {
      g.physics.arcade.collide(self, state.tilemapLayer)
      self.desiredVelocity.x = self.hitVelocity.x
      self.desiredVelocity.y = self.hitVelocity.y
      self.hitVelocity.x *= 0.95
      self.hitVelocity.y *= 0.95
      self.hitVelocity.y += g.elapsed * 0.2
      if(g.now > self.hitTimeEnd) {
        self.sm.transition(self.preHitState)
        // Restore has to be here and not in ...Exit, because we need to restore
        // the package AFTER the next state's ...Enter method
        self.onRestoreAfterHit(self.preHitPackage)
      }
    }
    
    self._gettinghitExit = function() {
      
    }
    
    self.addEnemyState('_gettinghit')

    self.update = function() {
      if(levelHack) {
        let level = g.dungeon.floor
        if(level > 1) {
          self.health = Math.floor(self.health * Math.pow(level, 1.2))
          console.log(self.health)
          let cl = 255 - ((level-1) * 30)
          self.tint = Phaser.Color.toRGBA(cl, cl, cl, 255)
        }      
        levelHack = false
      }
      
      // show effect icons (like backstab, melee, etc.)
      let numEffects = self.effects.filter(function(e) { return e.exists }).length
      var i = 0
      self.effects.forEach(function(e) {
        e.update()
        e.x = self.x - (numEffects-1) * 4 + i * 8
        e.y = self.top - 8
        if(e.exists) {
          i += 1
        }        
      })
      
      if(self.health > 0) {
        if(self.enemyUpdate) { self.enemyUpdate() }
        self.sm.update()
      } else {
        g.physics.arcade.collide(self, state.tilemapLayer)
        self.hittable = false
        self.desiredVelocity = new Phaser.Point(0,0)
      }

      // Convert desired enemy velocity (from their update) into velocity
      self.body.velocity.x = self.desiredVelocity.x
      self.body.velocity.y = self.desiredVelocity.y

      if(g.now > self.hitTime + 100) {
        self.filters = null
      } else{
        if(!self.filters) {
          var filter = new InvertFilter();
          self.filters = [filter]
        }
      }
    }
    
    self.spewRewards = function() {
      let rewards = []
      let bonus = g.pct(g.getStat('bonus'))
      let credits = Math.floor(g.creditsMultiplier * self.grantCredits * bonus)
      let fifties = Math.floor(credits * bonus / 50)
      credits -= fifties * 50
      let tens = Math.floor(credits / 10)
      credits -= tens * 10
      let fives = Math.floor(credits / 5)
      credits -= fives * 5
      let ones = credits
      let total = fives + ones + self.grantHealth
      let ai = 6.2818 / (total+1)
      let a = 0
      
      function spawn(image, frames, onTouch) {
        let pickup = Pickup(state, self.x, self.y, g.rnd.frac()*6.2818, image, frames, onTouch)
        state.gameLayer.add(pickup)
        state.pickups.push(pickup)
        a += ai
      }
      
      let i = 0
      
      for(i = 0; i < fifties; i++) {
        spawn('coin-pickup', [12,13,14,15], function(p) {g.credits += 50})
      }
      for(i = 0; i < tens; i++) {
        spawn('coin-pickup', [8,9,10,11], function(p) {g.credits += 10})
      }      
      for(i = 0; i < fives; i++) {
        spawn('coin-pickup', [4,5,6,7], function(p) {g.credits += 5})
      }
      for(i = 0; i < ones; i++) {
        spawn('coin-pickup', [0,1,2,3], function(p) {g.credits += 1})
      }
      for(i = 0; i < self.grantHealth; i++) {
        spawn('health-pickup', [0], function(p) {p.addHealth(1)})
      }
      
      
    }

    self.getHit = function(dmg, x, y) {
      if(self.health <= 0) { return }
      self.health -= dmg
      state.soundEnemyHit.play()
      g.multiplierTimer = 0
      if(self.health <= 0) {
        //self.hitVelocity.x = x * 2
        //self.hitVelocity.y = y * 2
        self.hitRecoverMs *= 3
        self.spewRewards()
        g.creditsMultiplier += 0.25
        g.dungeon.saveDeath(enemyId)
        self.destroy()
        if(self.sm.debugText) { self.sm.debugText.destroy() }
      } else {
        self.hitVelocity.x = x
        self.hitVelocity.y = y
        self.hitTimeEnd = g.now + g.math.clamp(Math.sqrt(x*x+y*y) * 10, 25, 500)
        self.sm.transition("_gettinghit")
      }
    }

    self.render = function() {
      //g.debug.body(self)
      if(self.sm.debugText) {
        self.sm.debugText.x = self.x
        self.sm.debugText.y = self.y - 16
      }      
      if(self.enemyRender) { self.enemyRender() }
    }

    return self
  }

  return ctor
})()
