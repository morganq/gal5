const StatInfo = (function() {
  function ctor(state, isDelta, maxLines, title, titleColor) {
    let self = new Phaser.Group(g)
    
    self.bg = new Phaser.Graphics(g, 0, 0)
    self.addChild(self.bg)
    self.bg.x = 0
    self.bg.y = 0
    
    self.updateBG = function() {
      self.bg.clear()
      self.bg.beginFill(0xffffff, 0.75)
      self.bg.drawRect(0, 0, self.width + 2, self.height + 2)
      self.bg.endFill()
    }
    
    self.updateBG()
    
    self.startY = 0
    
    if(title) {
      self.titleText = new Phaser.BitmapText(g, 0, -10, "visitor", title, 10)
      self.addChild(self.titleText)    
      self.titleText.y = 0
      self.titleText.x = 3
      if(titleColor) {
        self.titleText.tint = titleColor
      } else {
        self.titleText.tint = 0x020628
      }
      self.startY = 10
    }
    
    self.lines = []
    
    self.updateTitle = function(newTitle, newTitleColor) {
      self.titleText.text = newTitle
      if(newTitleColor) {
        self.titleText.tint = newTitleColor
      }
      self.updateBG()
    }
    
    self.updateStatsDelta = function(newStats, oldStats) {
      let stats = {}
      for(k in newStats) {
        if(k in oldStats) {
          stats[k] = newStats[k] - oldStats[k]
        } else {
          stats[k] = newStats[k]
        }
      }
      self.updateStats(stats)
    }
    
    self.updateStats = function(stats) {
      let newLines = []
      for(let k in stats) {
        let v = stats[k]
        if(v==0) { continue }
        newLines.push([k, v])
      }
      for(let i = self.lines.length; i < newLines.length; i++) {
        let newStatIcon = new Phaser.Sprite(g, 0, 0, 'staticons', 0)
        self.addChild(newStatIcon)
        let newText = new Phaser.BitmapText(g, 0, 0, "visitor", "", 10)
        self.addChild(newText)
        if(maxLines === undefined) {
          newStatIcon.y = i * 10 + self.startY
          newStatIcon.x = 0          
          newText.y = i * 10 + self.startY
          newText.x = 12  
        } else {
          newStatIcon.y = (i % maxLines) * 10 + self.startY
          newStatIcon.x = (50 * Math.floor(i / maxLines))
          newText.y = (i % maxLines) * 10 + self.startY
          newText.x = 12 + (50 * Math.floor(i / maxLines))
        }
        self.lines.push([newStatIcon, newText])
      }
      newLines.sort(function(a, b) {
        return a[0] < b[0]
      })
      for(let i = 0; i < newLines.length; i++) {
        self.lines[i][0].frame = STATS.indexOf(newLines[i][0])
        text = newLines[i][1].toFixed(1)
        if(isDelta) {
          if(newLines[i][1] > 0) {
            text = "+" + text
            self.lines[i][1].tint = 0x4da146
          } else {
            self.lines[i][1].tint = 0xd01f02
          }
        } else {
          self.lines[i][1].tint = 0x020628
        }
        self.lines[i][1].text = text + "%"
        self.lines[i][0].visible = true
        self.lines[i][1].visible = true
      }
      
      for(let i = newLines.length; i < self.lines.length; i++) {
        self.lines[i][0].visible = false
        self.lines[i][1].visible = false
      }
      
      self.updateBG()
    }
    
    return self
  }
  
  return ctor
})()
