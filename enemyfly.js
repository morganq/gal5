EnemyFly = (function() {
  ACTIVATE_TILE_RADIUS = 6
  FOLLOW_RANGE = 130
  ANGLE_BETWEEN_MIN = 0.5
  ANGLE_BETWEEN_MIN_SINE_AMPLITUDE = 0.1
  ANGLE_CHANGE_TO_SPEED_RATIO = 0.00005
  NORMAL_MOVE_SPEED = 30
  BOOST_MOVE_SPEED = 60
  MOVE_SPEED_SINE_RATE = 0.0005
  MOVE_SPEED_SINE_AMPLITUDE = 7
  BOOST_TIME_MS = 1000
  PATHFINDING_RECALCULATE_TIME_MS = 500
  SETTLE_SPEED = 15
  SHIVER_TIME_MS = 600

  function ctor(state, enemyId, x, y) {
    let self = new EnemyBase(state, enemyId, x, y, "enemyfly", 0)
    self.animations.add('_gettinghitsettled', [0], 8, true)
    self.animations.add('_gettinghitflying', [2], 8, true)
    self.animations.add("flying", [2,3,4,5,6,7,8,9], 16, true)
    self.animations.add("settled", [0,0,0,0,0,0,0,1,0,0,1,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0], 6, true)
    self.play("flying")
    self.health = 30
    self.body.setSize(10, 10, 0, 0)
    self.anchor.set(0.5, 0.5)
    g.slopes.enable(self)

    self.activationSprite = null

    self.moveAngle = 3.14159/2
    self.moveAngleVel = 0
    self.boostTime = 0

    self.pathfinder = new EasyStar.js()
    self.pathfinder.setGrid(state.pathData)
    self.pathfinder.setAcceptableTiles([T_PATH_EMPTY, T_PATH_HINT])
    self.pathfinder.setTileCost(T_PATH_HINT, 4)

    self.path = []
    self.pathfinding = false
    self.lastPathFindTime = 0

    self.settlePos = new Phaser.Point()
    self.settleAngle = 0

    self.state = STATES.INSTASETTLE
    self.randomValue = g.rnd.frac() * 6.2818

    self.shiverStartTime = 0
    self.shiverStartPos = new Phaser.Point(0,0)
    self.direction = 0
    
    self.create = function() {
      self.sm.add('roaming', self.roamingEnter, self.roamingUpdate, self.roamingExit)
      self.sm.add('chasing', self.chasingEnter, self.chasingUpdate, self.chasingExit)
      self.sm.add('settling', self.settlingEnter, self.settlingUpdate, self.settlingExit)
      self.sm.add('armed', self.armedEnter, self.armedUpdate, self.armedExit)
      self.sm.add('shivering', self.shiveringEnter, self.shiveringUpdate, self.shiveringExit)
      self.sm.transition('settling')
    }

    self.findNearbyTile = function() {
      let xy = state.tilemapLayer.getTileXY(self.x, self.y, new Phaser.Point())
      function ctest(x,y) {
        let tile = state.tilemap.getTile(xy.x+x, xy.y+y, 'main')
        if(tile && is_colliding_tile_index(tile.index)) {
          return true
        }
        return false
      }
      if(ctest(0, -1)) { return [new Phaser.Point((xy.x + 0.5) * GS, (xy.y) * GS), 3.14159] }
      if(ctest(0, 1)) { return [new Phaser.Point((xy.x + 0.5) * GS, (xy.y + 1) * GS), 0] }
      if(ctest(-1, 0)) { return [new Phaser.Point((xy.x) * GS, (xy.y + 0.5) * GS), 3.14159/2] }
      if(ctest(1, 0)) { return [new Phaser.Point((xy.x+1) * GS, (xy.y + 0.5) * GS), -3.14159/2] }
      return null
    }

    self.calcActivationRect = function() {
      let xy = state.tilemapLayer.getTileXY(self.x, self.y, new Phaser.Point())

      function gt(x,y) {
        t = state.tilemap.getTile(xy.x + x, xy.y + y, "main")
        if(t) {
          return is_colliding_tile_index(t.index)
        }
        return false
      }
      sx1 = gt(-1, 0) ? 0 : -ACTIVATE_TILE_RADIUS
      sx2 = gt(1, 0) ? 1 : ACTIVATE_TILE_RADIUS + 1
      sy1 = gt(0, -1) ? 0 : -ACTIVATE_TILE_RADIUS
      sy2 = gt(0, 1) ? 1 : ACTIVATE_TILE_RADIUS + 1

      if(!self.activationSprite) {
        self.activationSprite = new Phaser.Sprite(g, (xy.x + sx1) * GS, (xy.y + sy1) * GS)
        g.physics.enable(self.activationSprite)
        state.gameLayer.add(self.activationSprite)
      }
      self.activationSprite.body.setSize(GS * (sx2 - sx1), GS * (sy2 - sy1))
    }
    
    self.calcFlyingMovement = function(targetAngle) {
      let sineValue = self.randomValue + MOVE_SPEED_SINE_RATE * g.now
      
      // Do the boost logic
      let boostT = g.math.clamp(1 - (g.now - self.boostTime) / BOOST_TIME_MS,0,1)
      let moveSpeed = NORMAL_MOVE_SPEED + (BOOST_MOVE_SPEED * boostT) + Math.sin(sineValue) * MOVE_SPEED_SINE_AMPLITUDE      
      
      // Do wavey movement 
      let angBet = g.math.wrapAngle(targetAngle - self.moveAngle, true)
      if(Math.abs(angBet) > (ANGLE_BETWEEN_MIN + Math.cos(sineValue) * ANGLE_BETWEEN_MIN_SINE_AMPLITUDE)) {
        self.moveAngleVel = Math.sign(angBet) * ANGLE_CHANGE_TO_SPEED_RATIO * moveSpeed
      }
      
      // Turn vel into motion
      self.moveAngle = g.math.wrapAngle(self.moveAngle + self.moveAngleVel * g.elapsed, true)
      g.physics.arcade.velocityFromAngle(self.moveAngle * 180 / 3.14159, moveSpeed, self.desiredVelocity);
      
      // Flip left/right
      if(self.desiredVelocity.x < 0) { self.direction = -1}
      if(self.desiredVelocity.x > 0) { self.direction = 1}
      self.scale.x = self.direction         
    }
    
    self.flyingEnter = function() {
      self.play("flying")
      self.gettingHitAnimation = "_gettinghitflying"
      self.rotation = 0
      self.angle = 0
    }
    
    self.armedEnter = function() {
      self.calcActivationRect()
      self.desiredVelocity.set(0,0)
      self.hittable = false
      self.play("settled")
      self.gettingHitAnimation = "_gettinghitsettled"
    }
    
    self.armedUpdate = function() {
      if(!state.player.justStartedLevel) {
        g.physics.arcade.overlap(state.player, self.activationSprite, function() {
          self.sm.transition('shivering')
        })
      }
    }
    
    self.armedExit = function() {
      self.hittable = true
    }
    
    self.shiveringEnter = function() {
      self.shiverStartTime = g.now
      self.shiverStartPos = new Phaser.Point(self.x, self.y)      
      self.play("settled")
      self.gettingHitAnimation = "_gettinghitsettled"
    }
    
    self.shiveringUpdate = function() {
      if(g.now > self.shiverStartTime + SHIVER_TIME_MS) {
        self.sm.transition('chasing')
      }
      self.x = self.shiverStartPos.x + g.rnd.between(-2,2)
      self.y = self.shiverStartPos.y + g.rnd.between(-2,2)
    }
    
    self.shiveringExit = function() {
      self.boostTime = g.now
      self.moveAngle = self.settleAngle - 3.14159/2
    }
    
    self.roamingEnter = function() {
      self.calcActivationRect()
      self.flyingEnter()
    }
    
    self.roamingUpdate = function() {
      g.physics.arcade.collide(self, state.tilemapLayer)
      let horizAngle = (g.math.wrapAngle(self.moveAngle - 3.14159/2, true) < 0) ? 0 : 3.14159
      self.calcFlyingMovement(horizAngle)
      
      // When roaming, if the player shows up follow them
      if(!state.player.justStartedLevel) {
        g.physics.arcade.overlap(state.player, self.activationSprite, function() {
          self.sm.transition('chasing')
        })
      }
      
      // If we get near a wall, settle on it
      let near = self.findNearbyTile()
      if(near) {
        self.sm.transition('settling')
      }      
      
      self.activationSprite.x = self.x - self.activationSprite.body.width / 2
      self.activationSprite.y = self.y - self.activationSprite.body.height / 2
    }
    
    self.roamingExit = function() {}
    
    self.settlingEnter = function() {
      let near = self.findNearbyTile()
      if(near) {
        self.settlePos = near[0]
        self.settleAngle = near[1]
      }
      self.flyingEnter()
    }
    
    self.settlingUpdate = function() {
      let dx = self.settlePos.x - self.x
      let dy = self.settlePos.y - self.y
      if(dx*dx+dy*dy < 3*3) {
        let off = new Phaser.Point((self.settlePos.x - self.x) / 8, (self.settlePos.y - self.y) / 8)
        self.x = self.settlePos.x - off.x * 2
        self.y = self.settlePos.y - off.y * 2
        self.rotation = self.settleAngle
        self.sm.transition('armed') 
      } else {
        let d = Math.sqrt(dx*dx+dy*dy)
        self.desiredVelocity.x = dx / d * SETTLE_SPEED
        self.desiredVelocity.y = dy / d * SETTLE_SPEED
      }
    }
    
    self.settlingExit = function() {
      
    }
    
    self.chasingEnter = function() {
      self.path=[]
      self.pathfinding=false
      self.flyingEnter()
    }
    
    self.chasingUpdate = function() {
      g.physics.arcade.collide(self, state.tilemapLayer)
      let towardPlayerAngle = 0
      // In follow range
      if(g.math.distanceSq(self.x, self.y, state.player.x, state.player.y) < FOLLOW_RANGE * FOLLOW_RANGE) {
        // Have a good path
        if(self.path && self.path.length > 3) {
          let nextPathPos = new Phaser.Point(self.path[0].x * GS, self.path[0].y * GS)
          let aheadPathPos = new Phaser.Point(self.path[1].x * GS, self.path[1].y * GS)
          towardPlayerAngle = g.math.angleBetweenPoints(self.position, aheadPathPos)

          if(g.math.distanceSq(self.x, self.y, nextPathPos.x, nextPathPos.y) < 10 * 10) {
            self.path.shift()
          }

        } else { // No good path = just go towards player
          towardPlayerAngle = g.math.angleBetweenPoints(self.position, state.player.position)
        }

        // Can we see the player?
        if(state.tilemapLayer.getRayCastTiles(
          new Phaser.Line(self.x, self.y, state.player.x, state.player.y),
          2, true).length > 0) {
          // If not, we need a path (unless we're already pathfinding)
          if(!self.pathfinding && g.now > self.lastPathFindTime + PATHFINDING_RECALCULATE_TIME_MS) {
            self.pathfinding = true
            self.pathfinder.findPath(
              Math.floor(self.x / GS), Math.floor(self.y / GS),
              Math.floor(state.player.x / GS),Math.floor(state.player.y / GS),
              function(path) {
                if(!self.pathfinding) { return }
                path = path || [];
                self.path = path
                self.pathfinding = false
              }
            )
            self.pathfinder.calculate()
            self.lastPathFindTime = g.now
            
          }
        } else {
          self.path = []
          self.pathfinding = false
        }

      } else { // If we're not in follow range
        self.sm.transition('roaming')
      }
      self.calcFlyingMovement(towardPlayerAngle)
    }

    self.enemyUpdate = function() {
      self.angle = Math.round(self.rotation * 180 / 3.14159)    
    }

    self._enemyRender = function() {

      //g.debug.body(self)
      
      if(self.activationSprite) {
        g.debug.body(self.activationSprite)
      }
      
      if(self.path) {
        self.path.forEach(function(p) { g.debug.geom(new Phaser.Rectangle(p.x * GS, p.y*GS, GS, GS), 'rgba(0,0,0,0.2)') })
      }
      /*
      if(self.settlePos) {
        //g.debug.geom(new Phaser.Rectangle(self.settlePos.x - 3, self.settlePos.y - 3, 6, 6), 'rgba(0,0,255,0.5)')
      }
      let x = Math.cos(self.moveAngle)
      let y = Math.sin(self.moveAngle)
      g.debug.geom(new Phaser.Line(self.x, self.y, self.x + x * 10, self.y + y * 10), 'rgba(0,255,255,0.5)')
      */
    }

    return self
  }

  return ctor
})()
