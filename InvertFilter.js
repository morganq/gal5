/**
 * @author Mat Groves http://matgroves.com/ @Doormat23
 */

/**
 * This inverts your Display Objects colors.
 * 
 * @class InvertFilter
 * @extends AbstractFilter
 * @constructor
 */
InvertFilter = function()
{
    Phaser.Filter.call( this );

    this.passes = [this];

    // set the uniforms
    this.uniforms = {
        invert: {type: '1f', value: 1}
    };

    this.fragmentSrc = [
        'precision mediump float;',
        'varying vec2 vTextureCoord;',
        'varying vec4 vColor;',
        'uniform float invert;',
        'uniform sampler2D uSampler;',

        'void main(void) {',
        '   gl_FragColor = texture2D(uSampler, vTextureCoord);',
        '   gl_FragColor.rgb = vec3(1,0,0) * gl_FragColor.a;',
        //'   gl_FragColor.rgb = gl_FragColor.rgb  * gl_FragColor.a;',
      //  '   gl_FragColor = gl_FragColor * vColor;',
        '}'
    ];
};

InvertFilter.prototype = Object.create( Phaser.Filter.prototype );
InvertFilter.prototype.constructor = InvertFilter;

/**
 * The strength of the invert. 1 will fully invert the colors, 0 will make the object its normal color
 * @property invert
 * @type Number
*/
Object.defineProperty(InvertFilter.prototype, 'invert', {
    get: function() {
        return this.uniforms.invert.value;
    },
    set: function(value) {
        this.uniforms.invert.value = value;
    }
});
