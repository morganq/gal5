var GameState = function() {
  let self = {}
  let LAYERS = ["objects1", "objects2"]
  //let LAYERS = []

  self.createObjectGroups = function() {
    let regions = self.tilemap.objects['objectregions']
    let roomEnemyRegions = g.dungeon.getRoom().enemyRegions
    let chosen = regions.map(function(r, i) {
      return Object.assign({}, r, {"chosen":"objects" + ((roomEnemyRegions[i] % LAYERS.length)+1)})
    })
    
    //console.log(chosen)
    
    LAYERS.map(function(oName) {
      self.tilemap.forEach(function(t) {
        let tx = t.x * GS
        let ty = t.y * GS
        chosen.forEach(function(r, i) {
          if(t.index >= 0 &&
            tx + GS >= r.x && tx < r.x + r.width &&
            ty + GS >= r.y && ty < r.y + r.height) {
            //console.log (t.index, "in region", i)
            if(r.chosen != oName) {
              //console.log(t.index, "eliminated in region", i, "because oName is", oName, "but chosen is", r.chosen)
              self.tilemap.putTile(-1, t.x, t.y, oName)              
            }
          }
        })
      }, self, 0, 0, 500, 500, oName)      
    })
  }

  self.init = function(direction) {
    self.direction = direction
  }

  self.create = function() {
    g.plugins.add(Phaser.Plugin.ArcadeSlopes);
    g.physics.startSystem(Phaser.Physics.ARCADE);
    g.world.setBounds(-100, -100, 1920, 1920);
    
    self.windowMask = new Phaser.Graphics(g, 0, 0)
    self.windowMask.clear()
    self.windowMask.width = 800
    self.windowMask.height = 800
    self.windowMask.drawRect(0,0,1,1)


    self.fakeTilemapBmd = new Phaser.BitmapData(g, null, 800, 800)

    self.tilemap = g.add.tilemap(g.dungeon.getRoom().level)
    self.tilemap.x = 0
    self.tilemap.y = 0
    self.tilemap.addTilesetImage("wallpaper", "wallpapertiles", 8, 8)
    self.tilemap.addTilesetImage("bgtiles", "bgtiles", 8, 8)
    self.tilemap.addTilesetImage("main", "tiles", 8, 8)
    self.tilemap.addTilesetImage("flyhints", "tiles", 8, 8)    
    self.tilemap.addTilesetImage("walkerhints", "walkerhints", 8, 8)
    
    self.wallpaperLayer = self.tilemap.createLayer("wallpaper")
    self.wallpaperLayer.resizeWorld()    
    
    self.bgTilemapLayer = self.tilemap.createLayer("bgtiles")
    self.bgTilemapLayer.resizeWorld()
    self.bgTilemapLayer2 = self.tilemap.createLayer("bgtiles2")
    self.bgTilemapLayer2.resizeWorld()    

    self.tilemapLayer = self.tilemap.createLayer("main")
    self.tilemapLayer.resizeWorld()
    self.hintLayer = self.tilemap.createLayer("flyhints")
    self.hintLayer.resizeWorld()
    self.hintLayer.visible = false
    self.tilemap.setCollisionBetween(0,256, true, "main")
    self.tilemap.setCollisionBetween(55,55, true, "flyhints")
    
    self.tilemap.setCollisionBetween(0,9999, true, "walkerhints")
    self.walkerHintLayer = self.tilemap.createLayer("walkerhints")
    self.walkerHintLayer.resizeWorld()
    
    self.tilemap.tilesets.forEach(function(ts) {
      TILESET_INDICES[ts.name] = ts.firstgid
    })
    
    drawLayer = function(layer, key, layerIndexOffset) {
      let im = g.cache.getImage(key, true)
      let iw = Math.floor(im.base.width / GS)
      let ih = Math.floor(im.base.height / GS)
      for(let y = 0; y < layer.layer.data.length; y++) {
        for(let x = 0; x < layer.layer.data[0].length; x++) {
          let tile = layer.layer.data[y][x]
          let i = tile.index - layerIndexOffset
          if(i >= -1) {
            let tx = (i % iw) * GS
            let ty = Math.floor(i / iw) * GS
            //console.log(x,y,tx,ty)
            self.fakeTilemapBmd.copyRect(key,
              new Phaser.Rectangle(tx, ty, GS, GS),
              x * GS + layer.tileOffset.x, y * GS + layer.tileOffset.y, 1, null, true)
          }
        }
      }  
    }
    drawLayer(self.wallpaperLayer, 'wallpapertiles', TILESET_INDICES['wallpaper'])
    drawLayer(self.bgTilemapLayer, 'bgtiles', TILESET_INDICES['bgtiles'])
    drawLayer(self.bgTilemapLayer2, 'bgtiles', TILESET_INDICES['bgtiles'])
    drawLayer(self.tilemapLayer, 'tiles', TILESET_INDICES['main'])
    
    self.walkerHintLayer.visible = false
    self.bgTilemapLayer.visible = false
    self.bgTilemapLayer2.visible = false
    self.tilemapLayer.visible = false
    self.wallpaperLayer.visible = false
    
    
    self.fakeTilemap = new Phaser.Sprite(g, 0, 0, self.fakeTilemapBmd)
    //self.fakeTilemap.visible = false

    self.pauseableGroup = g.add.group()
    self.mapLayer = new Phaser.Group(g)
    self.shadowLayer = new Phaser.Group(g)
    self.gameLayer = new Phaser.Group(g)
    self.enemyLayer = new Phaser.Group(g)
    self.effectLayer = new Phaser.Group(g)
    self.bulletLayer = new Phaser.Group(g)
    self.pauseableGroup.add(self.mapLayer)
    self.pauseableGroup.add(self.shadowLayer)
    self.pauseableGroup.add(self.gameLayer)
    self.pauseableGroup.add(self.enemyLayer)
    self.pauseableGroup.add(self.effectLayer)
    self.pauseableGroup.add(self.bulletLayer)
    self.UILayer = g.add.group()
    self.menuLayer = g.add.group()
    self.menuLayer.fixedToCamera = true
    self.menu = PauseMenu(self)    
    self.mapLayer.add(self.fakeTilemap)
    self.player = new Player(self, 140, 300)
    self.player.alpha = 0
    
    self.playerFake = new Phaser.Sprite(g, 140, 300, 'hero')
    self.playerFake.anchor.set(0.5,0.5)
    self.player.weapons.forEach(function(weap) {
      self.playerFake.addChild(weap)  
    })
    self.playerFake.addChild(self.player.helpKeys)
    
    
    
    self.gameLayer.add(self.windowMask)
    
    
    let parallaxNames = ['parallax1', 'parallax2', 'parallax3', 'parallax4']
    self.parallax = []
    parallaxNames.forEach(function(para) {
      let spr  = new Phaser.Sprite(g, 0, 0, para)
      spr.fixedToCamera = true
      self.gameLayer.add(spr)
      spr.mask = self.windowMask
      self.parallax.push(spr)
    })
    
    self.healthMeter = Meter(10, 10, 0.6, 5, 0xd01f02, self.player, "health", "maxHealth")
    self.healthMeter.fixedToCamera = true
    self.UILayer.add(self.healthMeter)
    
    self.shieldMeter = Meter(74, 10, 0.6, 5, 0x616377, self.player, "shield", "maxShield")
    self.shieldMeter.fixedToCamera = true
    self.UILayer.add(self.shieldMeter)
    self.shieldMeter.visible = false
    
    self.creditsText = new Phaser.BitmapText(
      g, 40, 18, "visitor", "0", 20
    )
    self.creditsText.fixedToCamera = true;
    self.creditsText.tint = 0x000f9a
    self.UILayer.add(self.creditsText)    
    
    self.multiplierText = new Phaser.BitmapText(
      g, 10, 22, "visitor", "x1", 10
    )
    self.multiplierText.tint = 0x89269e
    self.multiplierText.fixedToCamera = true;
    
    self.multiplierTextOver = new Phaser.BitmapText(
      g, 10, 22, "visitor", "x1", 10
    )
    self.multiplierTextOver.tint = 0xee83b8
    self.multiplierTextOver.fixedToCamera = true;  
    self.multiplierMask = new Phaser.Graphics(g, 0, 0)  
    self.multiplierMask.beginFill(0xffffff)
    self.multiplierMask.drawRect(0, 0, 100, 5)
    self.multiplierMask.endFill()
    self.multiplierMask.x = -100
    self.multiplierMask.y = 24
    self.multiplierMask.fixedToCamera = true;  
    self.multiplierTextOver.mask = self.multiplierMask
    self.UILayer.add(self.multiplierText)
    self.UILayer.add(self.multiplierTextOver)
    self.UILayer.add(self.multiplierMask)   
    
    self.weaponsGroup = new Phaser.Group(g)
    self.weaponsGroup.fixedToCamera = true
    self.UILayer.add(self.weaponsGroup)
    self.weaponsGroup.cameraOffset.x = 10
    self.weaponsGroup.cameraOffset.y = 36
    
    self.fpsText = new Phaser.BitmapText(
      g, GAME_WIDTH-20, GAME_HEIGHT-20, "visitor", "0", 10
    )
    self.fpsText.tint = 0x00ff00
    self.fpsText.fixedToCamera = true
    self.UILayer.add(self.fpsText)
    
    let myslopes = {}
    for(let i = 0; i < 256; i++) {
      myslopes[i] = 'FULL'
    }
    
    myslopes[3] = 'QUARTER_BOTTOM_RIGHT_LOW'
    myslopes[4] = 'QUARTER_BOTTOM_RIGHT_HIGH'
    myslopes[5] = 'QUARTER_BOTTOM_LEFT_HIGH'
    myslopes[6] = 'QUARTER_BOTTOM_LEFT_LOW'
    myslopes[10] = 'QUARTER_BOTTOM_RIGHT_LOW'
    myslopes[11] = 'QUARTER_BOTTOM_RIGHT_HIGH'
    myslopes[12] = 'QUARTER_BOTTOM_LEFT_HIGH'
    myslopes[13] = 'QUARTER_BOTTOM_LEFT_LOW'    
    myslopes[14] = 'QUARTER_BOTTOM_RIGHT_HIGH'
    myslopes[15] = 'QUARTER_BOTTOM_LEFT_HIGH'    

    g.slopes.convertTilemapLayer(self.tilemapLayer, myslopes, 1);
    //self.tilemapLayer.debug = true
    g.slopes.preferY = true;

    self.pathData = []
    self.tilemap.layers[self.tilemap.getLayerIndex("main")].data.forEach(function(row,j) {
      let rd = []
      row.forEach(function(cell,i) {
        
        if(self.tilemap.layers[self.tilemap.getLayerIndex('flyhints')].data[j][i].index > -1) {
          rd.push(T_PATH_HINT)
        } else {
          if(is_colliding_tile_index_plus_stairs(cell.index)) {
            rd.push(T_PATH_BLOCKED)
          } else {
            rd.push(T_PATH_EMPTY)
          }
        }
      })
      self.pathData.push(rd)
    })

    self.createObjectGroups()

    ts2Start = self.tilemap.tilesets[0].total
    
    
    self.tilemap.forEach(function(t) {
      if(!t){ return }
      let ti = t.index - TILESET_INDICES['main']
      if(T_SPIKES_INDICES.includes(ti)) {
        t.setCollisionCallback(function(a,t) {
          if(a == self.player) {
            if(self.player.canBeHit()) {
              self.player.getHit(10, t.worldX + GS / 2)
            }
            return true
          }
        })
      }
      if(ti == T_PLATFORM) {
        t.collideLeft = false
        t.collideRight = false
        t.tint = 0xff0000
        t.setCollisionCallback(function(a,t) {
          if(a == self.player) {
            if((self.player.bottom < t.worldY + 4 && self.player.body.velocity.y > 0) ||
               (self.player.bottom < t.worldY + 8 && self.player.body.velocity.y > 50)) {
              return true
            }
          } else {
            if(a.careAboutPlatforms) {
              if(a.careAboutPlatforms == "jumpthrough") {
                if(a.bottom < t.worldY + 8 && a.body.velocity.y > 0) {
                  return true
                }
              } else {
                return true
              }
            }
          }
        })
      }
    }, self, 0, 0, self.tilemap.width, self.tilemap.height, "main")
    
    
    let treasureLocs = []
    
    let doors = []
    
    self.tilemap.forEach(function(t) {
      let ti = t.index - TILESET_INDICES['permanent']
      if(ti == T_DOOR_DOWN && g.dungeon.getRoom().down) {
        let door = Door(self, t.worldX-9, t.worldY-14, g.dungeon.x, g.dungeon.y+1, 'down')
        self.gameLayer.add(door)
        doors.push(door)
        if(self.direction == 'up') {
          self.player.x = t.worldX + 4
          self.player.bottom = t.worldY + 8
          self.player.lastFrameX = self.player.x
          self.player.lastFrameY = self.player.y
          door.frame=1
        }
      }
      if(ti == T_DOOR_LEFT) {
        console.log("LEFT DOOR")
        if(g.dungeon.getRoom().left) {
          let door = Door(self, t.worldX-9, t.worldY-14, g.dungeon.x-1, g.dungeon.y, 'left')
          self.gameLayer.add(door)
          doors.push(door)
          if(self.direction == 'right') {
            door.frame=1
          }
        }
        if(g.dungeon.getRoom().kind=="spawn") {
          let healer = Healer(self, t.worldX-9, t.worldY-14)
          self.gameLayer.add(healer)
        }
        if(self.direction == 'right') {
          self.player.x = t.worldX + 4
          self.player.bottom = t.worldY + 8
          self.player.lastFrameX = self.player.x
          self.player.lastFrameY = self.player.y     
        }
      }
      if(ti == T_DOOR_UP && g.dungeon.getRoom().up) {
        let door = Door(self, t.worldX-9, t.worldY-14, g.dungeon.x, g.dungeon.y-1, 'up')
        self.gameLayer.add(door)
        doors.push(door)
        if(self.direction == 'down') {
          self.player.x = t.worldX + 4
          self.player.bottom = t.worldY + 8
          self.player.lastFrameX = self.player.x
          self.player.lastFrameY = self.player.y
          door.frame=1       
        }
      }
      if(ti == T_DOOR_RIGHT && g.dungeon.getRoom().right) {
        let door = Door(self, t.worldX-9, t.worldY-14, g.dungeon.x+1, g.dungeon.y, 'right')
        self.gameLayer.add(door)
        doors.push(door)
        if(self.direction == 'left') {
          self.player.x = t.worldX + 4
          self.player.bottom = t.worldY + 8
          self.player.lastFrameX = self.player.x
          self.player.lastFrameY = self.player.y
          door.frame=1      
        }
      }
      if(ti == T_DOOR_RIGHT && g.dungeon.floor == 0) {
        let door = Door(self, t.worldX-9, t.worldY-14, g.dungeon.x+1, g.dungeon.y, 'right')
        self.gameLayer.add(door)
        
        door.activate = function() {
          g.dungeon = Dungeon(1)
          self.changeRooms(g.dungeon.spawn.x, g.dungeon.spawn.y, 'right')          
        }
        
      }        
      if(ti == T_TREASURE) {
        treasureLocs.push(new Phaser.Point(t.worldX, t.worldY))
      }
      if(ti == T_STORE) {
        self.gameLayer.add(new Store(self, t.worldX, t.worldY, g.dungeon.floor))
      }
      if(ti == T_REGISTRY) {
        self.gameLayer.add(new Registry(self, t.worldX, t.worldY))
      }      
    }, self, 0, 0, 500, 500, "permanent")
    
    if(g.dungeon.floor > 0 && g.dungeon.getRoom().kind != "boss") {
      self.player.justStartedLevel = true
    }
    
    if(g.dungeon.getRoom().kind == "boss") {
      doors.forEach(function(d){d.destroy()})
    }
    
    self.tilemap.forEach(function(t) {
      let tindex = t.index - TILESET_INDICES['bgtiles']
      if(T_WINDOW_TILES.includes(tindex)) {
        self.windowMask.beginFill(0xffffff)
        self.windowMask.drawRect(t.worldX, t.worldY-2, GS, GS)
      }
    }, self, 0, 0, 500, 500, "bgtiles2")
    
    if(treasureLocs.length > 0) {
      let randomTreasureIndex = g.dungeon.getRoom().treasureSpawn % treasureLocs.length
      let randomTreasureLoc = treasureLocs[randomTreasureIndex]
      treasureLocs.splice(randomTreasureIndex, 1)
      let rarity = 'common'
      if(g.dungeon.getRoom().kind == 'rare') {
        rarity = 'rare'
      }
      let t = ItemRandomSticker(self, randomTreasureLoc.x, randomTreasureLoc.y, g.dungeon.floor, rarity)
      t.stickersLeft = g.dungeon.getRoom().status.treasuresLeft
      self.gameLayer.add(t)
    }
    
    if(g.dungeon.floor==0) {
      lobbySpawn(self)
    }
    
    self.gameLayer.add(self.player)
    self.player.health = g.playerHealth
    self.player.shield = g.playerShield
    
    self.gameLayer.add(self.playerFake)
    
    deadEnemies = g.dungeon.getRoom().status.deadEnemies
    LAYERS.map(function(oName) {
      self.tilemap.forEach(function(t) {
        let replaced = true
        let id = t.x * 500 + t.y
        let ti = t.index - TILESET_INDICES['objects']
        if(deadEnemies.includes(id)) {
          return
        }
        if(ti == 0) {
          self.enemyLayer.add(new EnemyFly(self, id, t.x * GS, t.y * GS))
        } else if (ti == 1) {
          let walker = new EnemyWalker(self, id, t.x * GS + GS/2, t.y * GS + GS/2)
          self.enemyLayer.add(walker)
        } else if (ti == 2) {
          let charger = new EnemyCharger(self, id, t.x * GS + GS/2, t.y * GS + GS/2)
          self.enemyLayer.add(charger)
        } else if (ti == 3) {
          let spinner = new EnemySpinner(self, id, t.x * GS + GS/2, t.y * GS + GS/2)
          self.enemyLayer.add(spinner)
        } else if (ti == 4) {
          let charger = new EnemyRollyTurret(self, id, t.x * GS + 4, t.y * GS + 4)
          self.enemyLayer.add(charger)
        } else {
          replaced = false
        }
        if(replaced) {
          self.tilemap.putTile(-1, t.x, t.y, oName)
        }
      }, self, 0, 0, 500, 500, oName)      
    })
    
    self.enemyLayer.forEach(function(e) {
      if(e.create) { e.create() }
    })
    
    if(g.dungeon.getRoom().kind == "boss") {
      let id = 1
      let boss = new CageBoss(self, id, 50 * GS, 45 * GS)
      self.enemyLayer.add(boss)
      
      let hpscale = (GAME_WIDTH-20) / boss.health
      
      self.bossHealth = Meter(10, 2, hpscale, 5, 0xdb8600, boss, "health", "maxHealth")
      self.bossHealth.fixedToCamera = true
      self.UILayer.add(self.bossHealth)      
      
    }

    //self.inMenu = true
    //self.menu.open()

    self.menuLayer.add(self.menu)
    self.menuButton = g.input.keyboard.addKey(Phaser.Keyboard.Q)
    self.UIButton = g.input.keyboard.addKey(Phaser.Keyboard.A)

    self.pausedTime = 0
    self.pauseUpdate = function() { }
    self.unpausedUpdate = self.pauseableGroup.update
    
    //g.camera.follow(self.player)
    g.camera.focusOnXY(Math.round(self.player.x), Math.round(self.player.y))
    
    self.mapGrp = g.dungeon.instantiateMap()
    self.mapGrp.fixedToCamera = true
    self.mapGrp.cameraOffset.x = GAME_WIDTH - self.mapGrp.width - 10
    self.mapGrp.cameraOffset.y = 15
    self.UILayer.add(self.mapGrp)
    
    self.updateWeapons()
    
    self.soundEnemyHit = g.add.audio('enemy-hit')
    self.soundEnemyArmorBounce = g.add.audio('enemy-armor-bounce')
    self.soundLaser = g.add.audio('laser')
    self.soundHurt = g.add.audio('hurt')
    self.soundJump = g.add.audio('jump')
    self.soundDodge = g.add.audio('dodge')
    self.soundBonus = g.add.audio('bonusmove')
    
    self.policeDamageTimer = g.now + 10000
    self.pickups = []
    self.frameIndex = 0
    self.updateCamera()
    g.camera.fx.beginFill(0x000000);
    g.camera.fx.drawRect(0, 0, g.camera.width, g.camera.height);
    g.camera.fx.endFill();
    g.camera.fx.alpha = 1
    
    
    self.transitioning = true
  }
  
  self.updateWeapons = function() {
    self.weaponsGroup.removeAll()
    
    let i = 8
    g.weapons.forEach(function(w) {
      if(w == null) { return }
      
      let spr = new Phaser.Sprite(g, 0, 0, w.inventoryImage)
      spr.anchor.set(0.5,0.5)
      self.weaponsGroup.add(spr)
      spr.x = i
      spr.y = 8
      i += 19
    })
    
    self.weaponSelector = new Phaser.Graphics(g, 0, 0)
    self.weaponSelector.lineStyle(1, 0xdb8600, 1)
    self.weaponSelector.drawRect(0, 0, 18, 18)
    self.weaponsGroup.add(self.weaponSelector)
    self.weaponSelector.x = -1 + g.curWeaponIndex * 16
    self.weaponSelector.y = -1
    
    self.player.resetWeapons()
    self.player.weapons.forEach(function(weap) {
      self.playerFake.addChild(weap)  
    })
  }
  
  self.makeDustParticle = function(key, frames, x, y, xv, yv, gravity, angle) {
    let dust = new Phaser.Sprite(g, x, y, key)
    dust.anchor.set(0.5, 0.5)
    dust.animations.add("default", frames, 16, false)
    dust.play("default", null, false, true)
    g.physics.enable(dust)
    self.effectLayer.add(dust)
    dust.body.velocity.x = xv
    dust.body.velocity.y = yv
    dust.angle = angle
    if(gravity) {
      dust.update = function() {
        dust.body.velocity.y = dust.body.velocity.y + g.elapsed / 10
      }    
    }
  }
  
  self.changeRooms = function(tx, ty, direction) {
    self.pickups.forEach(function(p) {
      p.collect()
    })
    self.transitioning = true
    g.camera.fade(0x000000, 250)
    g.camera.onFadeComplete.add(function() {
      g.playerHealth = self.player.health
      g.playerShield = self.player.shield
      g.dungeon.move(tx, ty)
      g.dungeon.tickPolice()
      g.changeRooms(direction)
      g.camera.onFadeComplete.removeAll()
    })
  }
  
  self.award = function(image, name, color, x, y, altTitle) {
    if(self.reward) {
      self.reward.destroy()
    }
    let reward = new Phaser.Group(g, 0, 0)
    
    let rewardBg = new Phaser.Graphics(g, 0, 0)
    rewardBg.x = GAME_WIDTH
    rewardBg.y = 0
    reward.addChild(rewardBg)
    
    //reward.anchor.set(0.5, 0.5)
    let rewardGfx = new Phaser.Sprite(g, 0, 0, image)
    rewardGfx.anchor.set(0.5, 0.5)
    reward.addChild(rewardGfx)
    self.gameLayer.add(reward)
    reward.fixedToCamera = true
    rewardGfx.x = Math.floor(GAME_WIDTH * 2)
    rewardGfx.y = Math.floor(GAME_HEIGHT / 2 - 50)
    
    
    let title = altTitle || "New Sticker! [q] to apply"
    expTxt = new Phaser.BitmapText(g, 0, 0, "visitor", title, 10)
    expTxt.anchor.set(0.5, 0.5)
    reward.addChild(expTxt)
    expTxt.x = GAME_WIDTH * 2
    expTxt.y = rewardGfx.top - 8
    expTxt.tint = 0x020628    
    
    rewardTxt = new Phaser.BitmapText(g, 0, 0, "visitor", name, 20)
    rewardTxt.anchor.set(0.5, 0.5)
    reward.addChild(rewardTxt)
    rewardTxt.x = GAME_WIDTH * 2
    rewardTxt.y = rewardGfx.bottom + 12
    rewardTxt.tint = color    
    
    rewardBg.beginFill(0xffffff, 0.75)
    rewardBg.drawRect(0, 0, GAME_WIDTH, Math.floor(rewardGfx.height + 45))
    rewardBg.endFill()
    rewardBg.y = Math.floor(rewardGfx.top - 17)
    
    reward.startTime = g.now
    reward.update = function() {
      let t = g.now - reward.startTime
      rewardBg.x = Math.max(GAME_WIDTH - t * 3, 0)
      expTxt.x = rewardBg.x + GAME_WIDTH / 2
      rewardGfx.x = rewardBg.x + GAME_WIDTH / 2
      rewardTxt.x = rewardBg.x + GAME_WIDTH / 2
      reward.alpha = Math.min(3 - t / 3000, 1)
      if(reward.alpha <= 0) {
        reward.destroy()
      }
    }
    self.reward = reward
  }
  
  self.setCurrentWeapon = function(num) {
    self.menu.loadCurrentWeapon()
  }

  self.shakeAmt = 0
  self.shakeX = 0
  self.shakeY = 0
  self.bulletShake = function(x,y) {
    /*self.shakeAmt = 2
    self.shakeX = x
    self.shakeY = y
    */
  }
  
  self.setAnimationStatus = function(paused) {
    self.pauseableGroup.forEach(function(grp) {
      grp.forEachExists(function(spr) {
        if(spr.animations && spr.animations.currentAnim) {
          spr.animations.paused = paused
        }
      })
    })
  }

  self.update = function() {
    if(self.frameIndex == 2) {
      g.camera.flash(0x000000, 250, true)
      g.camera.onFlashComplete.add(function() {
        self.transitioning = false
        g.camera.onFlashComplete.removeAll()
      })
    }
    self.frameIndex += 1
    self.updateCamera()
    self.weaponSelector.x = -1 + g.curWeaponIndex * 19    
    self.creditsText.text = g.credits.toString()
    self.multiplierText.text = "x" + g.creditsMultiplier.toFixed(2)
    self.multiplierTextOver.text = self.multiplierText.text
    self.creditsText.cameraOffset.x = self.multiplierText.cameraOffset.x + self.multiplierText.textWidth + 5
    self.fpsText.text = g.time.fps.toString()
    if(self.inMenu || self.player.killed || self.transitioning) {
      self.pauseableGroup.update = self.pauseUpdate
      g.physics.arcade.isPaused = true
      self.setAnimationStatus(true)
    } else {
      if(g.physics.arcade.isPaused) { // this will happen on the first frame after unpause
        self.setAnimationStatus(false)
      }     
      if(g.menuButton.downDuration(1)) {
        g.menuButton.reset()
        self.menu.open()
      }      
      self.pauseableGroup.update = self.unpausedUpdate
      g.physics.arcade.isPaused = false
      g.elapsed = Math.min(g.time.elapsed, 200)
      g.now += g.elapsed
      if(!self.player.justStartedLevel) {
        g.multiplierTimer += g.time.elapsed / g.pct(g.getStat('multipliertime'))
      }
      g.updateStats()
      let t = Math.max(1 - ((g.multiplierTimer) / BASE_MULTIPLIER_TIMER), 0)
      if(g.creditsMultiplier > 1) {
        self.multiplierMask.cameraOffset.x = Math.round(
          self.multiplierText.cameraOffset.x -
          (100 - self.multiplierText.textWidth) - 
          self.multiplierText.textWidth * (1-t))
      } else {

      }
      if(g.multiplierTimer > BASE_MULTIPLIER_TIMER) {
        g.creditsMultiplier = 1
        self.multiplierMask.cameraOffset.x = self.multiplierText.cameraOffset.x - 100
      }
      
      if(self.player.debugCollisionChecks.length > 50) {
        self.player.debugCollisionChecks.splice(0, self.player.debugCollisionChecks.length-50)
      }
      
      let testPlayerMidframe = function(color) {
        //console.log("before: x=", self.player.body.x, "y=",self.player.body.y)
        if(color) {
          self.player.debugCollisionChecks.push(
            {
              rect:new Phaser.Rectangle(self.player.body.left, self.player.body.top, self.player.body.width, self.player.body.height),
              color:color
            }
          )
        }
        let didHit = false
        g.physics.arcade.collide(self.player, self.tilemapLayer, function(p,t) {
          didHit = true
          return true
        })
        return didHit
        //console.log("after: x=", self.player.body.x, "y=",self.player.body.y)
      }
      
      let xf = self.player.body.x - self.player.lastFrameX
      let yf = self.player.body.y - self.player.lastFrameY
      let intervalsX = Math.ceil(Math.abs(xf) / 3)
      let intervalsY = Math.ceil(Math.abs(yf) / 3)
      let maxIntervals = Math.max(intervalsX, intervalsY)
      
      if(maxIntervals <= 1)  {
        testPlayerMidframe()
      } else {
        let res = false//testPlayerMidframe()
        if(!res) {
          let sx = xf
          let sy = yf
          let xv = sx / maxIntervals
          let yv = sy / maxIntervals
          //console.log("xf=",xf,"yf=",yf,"intervals=",maxIntervals,"xv=",xv,"yv=",yv)
          self.player.body.x = self.player.body.x - sx
          self.player.body.y = self.player.body.y - sy
          
          //testPlayerMidframe('rgba(255,255,0,0.5)'
          let color = 'rgba(' + g.rnd.between(0,255) + ',' + g.rnd.between(0,255) + ',' + g.rnd.between(0,255) + ',0.75)'
          for(var i = 0; i < maxIntervals; i++) {
            self.player.body.x += xv
            self.player.body.y += yv
            //console.log("i=",i)
            let res = testPlayerMidframe(color)
          } 
        }
      }
      
      self.player.lastFrameX = self.player.body.x
      self.player.lastFrameY = self.player.body.y      
      
      if(g.dungeon.getRoom().hasPolice && g.now > self.policeDamageTimer) {
        self.policeDamageTimer += 10000
        self.player.getHit(10, 0)
      }      
    }
    
    

    
    if(g.input.mousePointer.isDown) {
      let mx = self.tilemapLayer.getTileX(g.input.activePointer.worldX)
      let my = self.tilemapLayer.getTileY(g.input.activePointer.worldY)
      console.log(self.tilemap.getTile(mx,my))
    }
    
    if(self.player.maxShield > 0) {
      self.shieldMeter.cameraOffset.x = self.healthMeter.currentWidth + self.healthMeter.cameraOffset.x + 4
      self.shieldMeter.visible = true
    } else {
      self.shieldMeter.visible = false
    }
  }
  
  self.updateCamera = function() {
    self.shakeAmt = Math.max(0, self.shakeAmt - g.elapsed / 50)
    let shakeX = g.rnd.normal() * self.shakeAmt + self.shakeX * self.shakeAmt
    let shakeY = g.rnd.normal() * self.shakeAmt + self.shakeY * self.shakeAmt
    
    let focusX = self.player.world.x - g.camera.view.halfWidth
    let focusY = self.player.world.y - g.camera.view.halfHeight + shakeY
    let camX = (g.camera.view.x) * 0.9 + focusX * 0.1 + shakeX
    //camX = focusX
    g.camera.setPosition(Math.floor(camX), Math.floor(focusY))
    self.playerFake.x = Math.floor(self.player.x)// - 8 * self.player.scale.x
    self.playerFake.y = Math.floor(self.player.y)// - 8
    self.playerFake.alpha = self.player.fakeAlpha
    self.playerFake.frame = self.player.frame 
    self.playerFake.scale.x = self.player.scale.x
    self.playerFake.rotation = self.player.rotation
    
    self.parallax.forEach(function(p, i) {
      let sx = i * 0.15
      let sy = i * 0.05 - 0.8
      let yo = -30 + 40 * i
      p.cameraOffset.x = g.camera.view.x * -sx - 300
      p.cameraOffset.y = g.camera.view.y * sy - i * 18 - yo
    })    
  }

  self.render = function() {
    //g.debug.body(self.player)
    
    self.bulletLayer.forEach(function(z) { z.render() })
    self.enemyLayer.forEach(function(z) { if(z.render) { z.render()} })
    self.player.render()
    
    /*
    if(self.player.debugCollisionChecks) {
      self.player.debugCollisionChecks.forEach(function(r) {
        g.debug.geom(r.rect, r.color)
      })
    }
    */
    
    
    /*g.debug.geom(new Phaser.Rectangle(
      self.player.debugLastCollisionCheck.x-2,
      self.player.debugLastCollisionCheck.y-8, 4, 16, 'rgba(0,255,0,0.5)'))
    */
    
    //if(self.lastBulletHitRect) {
    //  g.debug.geom(self.lastBulletHitRect, 'rgba(0,255,0,1)')
    //}
  }

  return self
}();
