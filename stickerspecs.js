const _stickerSpecs = [
  ////////////
  // LEVEL 1
  ////////////
  {stats:{'damage':5}, sprite:'butterknife', rarity:'common', level:1, name:'butter knife'},  
  {stats:{'damage':10}, sprite:'steakknife', rarity:'common', level:1, name:'steak knife'},
  {stats:{'reload':10}, sprite:'generic-3', rarity:'common', level:1, name:'DIY mod'},
  {stats:{'speed':10}, sprite:'generic-2', rarity:'common', level:1, name:'marathon'},
  {stats:{'jump':5}, sprite:'frog', rarity:'common', level:1, name:'frog'},
  {stats:{'jump':7}, sprite:'bunny', rarity:'common', level:1, name:'bunny'},
  {stats:{'roll':15}, sprite:'coffeebean', rarity:'common', level:1, name:'coffee bean'},
  {stats:{'aim':10}, sprite:'generic-2', rarity:'common', level:1, name:'steady aim'},
  
  // cloud!
  {stats:{'float':25}, sprite:'cloud', rarity:'common', level:1, name:'cloud'},
  
  {stats:{'damage':13}, sprite:'cleaver', rarity:'uncommon', level:1, name:'cleaver'},
  {stats:{'reload':15}, sprite:'generic-2', rarity:'uncommon', level:1, name:'quick hands'},
  {stats:{'bonus':15}, sprite:'generic-1', rarity:'uncommon', level:1, name:'lucky find'},
  {stats:{'multipliertime':15}, sprite:'generic-1', rarity:'uncommon', level:1, name:'extension'},
  {stats:{'critchance':4}, sprite:'jackofhearts', rarity:'uncommon', level:1, name:'jack of hearts'},
  {stats:{'critdamage':25}, sprite:'xray2', rarity:'uncommon', level:1, name:'x-ray'},
  {stats:{'health':10}, sprite:'painkiller', rarity:'uncommon', level:1, name:'painkiller'},
  {stats:{'roll':35}, sprite:'caffeine', rarity:'uncommon', level:1, name:'caffeine'},
  
  {stats:{'float':20, 'jump':4}, sprite:'chicken', rarity:'uncommon', level:1, name:'chicken'},
  
  // bullet made of blood!
  {stats:{'vampire':2}, sprite:'generic-3', rarity:'uncommon', level:1, name:'lifeblood'},
  
  // 
  {stats:{'bullets':20}, sprite:'generic-1', rarity:'uncommon', level:1, name:'makeshift upgrade'},
  
  // fingers holding a needle and thread
  {stats:{'piercing':20}, sprite:'generic-1', rarity:'uncommon', level:1, name:'needle'},
  
  // archery?
  {stats:{'aim':25}, sprite:'triangulate', rarity:'uncommon', level:1, name:'triangulate'},
  
  // could be a brand logo... for a weapons mnfctr? 
  {stats:{'bullets':15, 'reload':10, 'critchance':3}, sprite:'generic-1', rarity:'rare', level:1, name:'weaponsmith'},
  
  
  {stats:{'bullets':34, 'reload':-25}, sprite:'generic-2', rarity:'rare', level:1, name:'sawed off'},
  
  // cute boots
  {stats:{'speed':25}, sprite:'booties', rarity:'rare', level:1, name:'booties'},
  
  // ?
  {stats:{'multipliertime':35, 'reload':25, 'bonus':-25}, sprite:'generic-4', rarity:'rare', level:1, name:'combo'},
  
  // ?
  {stats:{'piercing':35, 'aim':20}, sprite:'generic-4', rarity:'rare', level:1, name:'holy light'},
  
  // ?
  {stats:{'damage':25, 'health':-25}, sprite:'generic-3', rarity:'rare', level:1, name:'frenzy'},
  
  // hot potato.
  {stats:{'fire':10}, sprite:'generic-1', rarity:'rare', level:1, name:'hot potato'},
  
  // tesla coils?
  {stats:{'lightning':10}, sprite:'generic-1', rarity:'rare', level:1, name:'flicker'},
  {stats:{'shield':5}, sprite:'psychicarmor', rarity:'rare', level:1, name:'psychic armor'},
  
  // ?
  {stats:{'damage':18}, sprite:'generic-4', rarity:'rare', level:1, name:'big gun'},
  
  // cute leech
  {stats:{'vampire':4,'health':-25}, sprite:'leech', rarity:'rare', level:1, name:'leech'},
  
  
  {stats:{'critchance':10, critdamage:50, 'reload':-10}, sprite:'generic-1', rarity:'rare', level:1, name:'stabilizer'},
  
  // stylized normal-shape with athletes
  {stats:{'speed':25, roll:25, jump:15, 'reload':-10}, sprite:'generic-4', rarity:'rare', level:1, name:'athlete'},
  
  // surprised face looking towards camera/bullet
  {stats:{'aim':20, critchance:7, speed:-10}, sprite:'headshot', rarity:'rare', level:1, name:'headshot'},
  
  // horn of plenty
  {stats:{'multipliertime':20, bonus:10, damage:-10}, sprite:'cornucopia', rarity:'rare', level:1, name:'bounty'},
  
  {stats:{'critdamage':100}, sprite:'generic-4', rarity:'rare', level:1, name:'tiger surgeon'},
  
  {stats:{'backstab':30, critchance:5}, sprite:'generic-1', rarity:'rare', level:1, name:'rogue'},
  
  {stats:{'melee':30, 'speed':10}, sprite:'generic-1', rarity:'rare', level:1, name:'brawler'},
  
  {stats:{'sniper':30, 'aim':15}, sprite:'generic-1', rarity:'rare', level:1, name:'from downtown'},
  
  ////////////
  // LEVEL 2
  ////////////
  {stats:{'bullets':50}, sprite:'rdlogo', rarity:'rare', level:2, name:'rd logo'},
  
  ////////////
  // LEVEL 3
  ////////////
  {stats:{'multipliertime':30, bonus:30}, sprite:'generic-1', rarity:'rare', level:3, name:'cornucopia'},
]

let stickerSpecs = []

_stickerSpecs.forEach(function(spec) {
  stickerSpecs.push(spec)
})

const fillStickerSpecs = [
  {stats:{'jump':20}, color:0xa9de79, level:1, name:'light'},
  {stats:{'roll':30}, color:0x196d0e, level:1, name:'agile'},
  {stats:{'speed':30}, color:0x4da146, level:1, name:'quick'},
  {stats:{'damage':30}, color:0xbb499a, level:1, name:'high-impact'},
  {stats:{'bullets':34}, color:0x89269e, level:1, name:'modified'},
  {stats:{'bonus':20}, color:0x005ede, level:1, name:'bounty hunter\'s'},
  {stats:{'critchance':15}, color:0x8e012c, level:1, name:'precise'},
  {stats:{'vampire':5}, color:0xd01f02, level:1, name:'thirsty'},
]
