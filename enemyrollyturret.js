const EnemyRollyTurret = (function() {
  const DOWNFORCE = 400
  const WALKSPEED = 30
  const FIRE_RELOAD_TIME_MS = 500
  const BULLET_SPEED = 80
  const MAX_NUM_SHOTS = 7
  const WALK_TIME_MIN_MS = 1000
  const WALK_TIME_MAX_MS = 4000
  const MODETIME_MS = 900
  
  const STATES = {
    WALKING:"walking",
    SETTINGUP:"settingup",
    SIEGE:"siege",
    CLOSINGDOWN:"closingdown"
  }
  
  function ctor(state, enemyId, x, y) {
    let self = EnemyBase(state, enemyId, x, y, "enemyrollyturret", 0)
    self.body.anchor = new Phaser.Point(0.5, 0.5)
    
    self.animations.add("float", [0,0,0,1,1,1], 9)
    self.animations.add("siegeleft", [1,2,3,4,5,6], 6, false)
    self.animations.add("fireleft", [7,8,6], 9, false)
    self.animations.add("siegecenter", [1,2,3,4,9,10], 6, false)
    self.animations.add("firecenter", [11,12,10], 9, false)
    self.animations.add("siegeright", [1,2,3,4,13,14], 6, false)
    self.animations.add("fireright", [15,16,14], 9, false)  
    self.animations.add("unseige", [3,2,1,0], 4, false)
    
    self.careAboutPlatforms = true
    
    self.direction = 1
    self.walkStartTime = g.now
    self.nextShot = 0

    self.state = STATES.WALKING
    
    self.startAngle = Math.PI / 2
    
    self.debugPts = [null, null, null, null]
    self.nextDustTime = 0
    
    self.create = function() {
      self.addEnemyState('walking')
      self.addEnemyState('settingup')
      self.addEnemyState('firing')
      self.addEnemyState('closingdown')
      self.sm.transition('walking')
    }
    
    self.setWalkEndTime = function() {
      self.walkEndTime = g.now + g.rnd.between(WALK_TIME_MIN_MS, WALK_TIME_MAX_MS)
    } 
    
    self.setWalkEndTime()  
    
    self.hasTile = function(x,y) {
      let tiles = state.tilemapLayer.getTiles(x, y, 1, 1)
      if(tiles.length == 0) { return false }
      return is_colliding_tile_index_all(tiles[0].index)
    }     
    
    if(self.hasTile(self.x, self.bottom+1)) {
      self.startAngle = Math.PI / 2
    } else if (self.hasTile(self.right+1, self.y)) {
      self.startAngle = 0
      self.shadow.xo = 3
    } else if (self.hasTile(self.left-1, self.y)) {
      self.startAngle = Math.PI
      self.shadow.xo = -3
    } else if (self.hasTile(self.x, self.top-1)) {
      self.startAngle = -Math.PI / 2
    }
    self.angle = Math.round((self.startAngle - Math.PI/2) * 180 / Math.PI)    
    
    self.walkingEnter = function() {
      self.play("float")
      self.setWalkEndTime()
    }
    
    self.walkingUpdate = function() {
      self.desiredVelocity.x = Math.cos(self.startAngle - Math.PI/2) * WALKSPEED * self.direction
      self.desiredVelocity.y = Math.sin(self.startAngle - Math.PI/2) * WALKSPEED * self.direction
      
      if(g.now > self.nextDustTime) {
        self.nextDustTime = g.now + 10 + g.rnd.between(0, 120) + (self.frame == 0 ? 140 : 20)
        let a = -self.startAngle// + (g.rnd.frac() - 0.5) * 0.2
        self.makeDust(
          Math.cos(self.startAngle) * 9,
          Math.sin(self.startAngle) * 9,
          Math.cos(a) * g.rnd.between(30,60),
          Math.sin(a) * g.rnd.between(30,60))
      }
      
      if(g.now >= self.walkEndTime) {
        self.sm.transition('settingup')
      }
      
      let downright = state.tilemapLayer.getTileXY(self.right + 1, self.bottom + 1, new Phaser.Point())
      let downleft = state.tilemapLayer.getTileXY(self.left - 1, self.bottom + 1, new Phaser.Point())
      let blx = Math.floor(self.x + Math.cos(self.startAngle + Math.PI/4) * 14)
      let bly = Math.floor(self.y + Math.sin(self.startAngle + Math.PI/4) * 14)
      let brx = Math.floor(self.x + Math.cos(self.startAngle - Math.PI/4) * 14)
      let bry = Math.floor(self.y + Math.sin(self.startAngle - Math.PI/4) * 14)
      let mlx = Math.floor(self.x + Math.cos(self.startAngle + Math.PI/2) * 11)
      let mly = Math.floor(self.y + Math.sin(self.startAngle + Math.PI/2) * 11)
      let mrx = Math.floor(self.x + Math.cos(self.startAngle - Math.PI/2) * 11)
      let mry = Math.floor(self.y + Math.sin(self.startAngle - Math.PI/2) * 11)
      self.debugPts[0] = new Phaser.Point(blx, bly)
      self.debugPts[1] = new Phaser.Point(mlx, mly)
      self.debugPts[2] = new Phaser.Point(brx, bry)
      self.debugPts[3] = new Phaser.Point(mrx, mry)

      if(self.direction == -1 &&
        (!self.hasTile(blx, bly) ||
        self.hasTile(mlx, mly))) {
          self.direction = 1
      }
      else if (self.direction == 1 &&
        (!self.hasTile(brx, bry) ||
        self.hasTile(mrx, mry))) {
          self.direction = -1
      }        
    }
    
    self.walkingExit = function() {
      self.desiredVelocity.x = 0
      self.desiredVelocity.y = 0      
    }
    
    self.settingupEnter = function() {
      self.transitionTime = g.now
      self.barrel = g.rnd.between(0,2)
      if(self.barrel == 0) {
        self.play("siegeleft")
      } else if (self.barrel == 1) {
        self.play("siegecenter")
      } else {
        self.play("siegeright")
      }        
    }
    
    self.settingupUpdate = function() {
      if(g.now > self.transitionTime + MODETIME_MS) {
        self.sm.transition('firing')
      }
    }
    
    self.settingupExit = function(){}
    
    self.firingEnter = function() {
      self.fireAngle = self.startAngle + Math.PI + (self.barrel-1) * Math.PI / 4
      self.numShots = g.rnd.between(2, MAX_NUM_SHOTS)
      self.nextShot = g.now + FIRE_RELOAD_TIME_MS
    }
    
    self.firingUpdate = function() {
      if(g.now > self.nextShot) {
        self.nextShot = g.now + FIRE_RELOAD_TIME_MS
        self.numShots -= 1
        self.fire()
        if(self.barrel == 0) {
          self.play("fireleft")
        } else if (self.barrel == 1) {
          self.play("firecenter")
        } else {
          self.play("fireright")
        }
      }
      if(self.numShots <= 0) {
        self.sm.transition('closingdown')
      }      
    }
    
    self.firingExit = function() {
      
    }
    
    self.closingdownEnter = function() {
      self.transitionTime = g.now
      self.play("unseige")
    }
    
    self.closingdownUpdate = function() {
      if(g.now > self.transitionTime + MODETIME_MS) {
        self.sm.transition('walking')
      }
    }
    
    self.closingdownExit = function() {
      
    }
    
    self.onSaveBeforeHit = function(){
      return {
        transitionTime:self.transitionTime,
        numShots:self.numShots,
        fireAngle:self.fireAngle,
        barrel:self.barrel,
        desiredVelocity:self.desiredVelocity.clone()
      }
    }
    self.onRestoreAfterHit = function(package) {
      //self.transitionTime = package.transitionTime
      self.numShots = package.numShots
      self.fireAngle = package.fireAngle
      self.barrel = package.barrel
      self.desiredVelocity = package.desiredVelocity
    }

    
    self.enemyUpdate = function() {
      g.physics.arcade.collide(self, state.tilemapLayer)
      self.desiredVelocity = self.body.velocity
      self.desiredVelocity.x += Math.cos(self.startAngle) * DOWNFORCE * g.elapsed / 1000.0
      self.desiredVelocity.y += Math.sin(self.startAngle) * DOWNFORCE * g.elapsed / 1000.0
    }
    
    self.makeDust = function(xo, yo, xv, yv) {
      let dust = new Phaser.Sprite(g, self.x + xo, self.y + yo, "crystaldust")
      dust.animations.add("decay", [0,1,2,3,4,5,6,6], 16, false)
      dust.play("decay", null, false, true)
      g.physics.enable(dust)
      state.effectLayer.add(dust)
      dust.body.velocity.x = xv
      dust.body.velocity.y = yv
      dust.update = function() {
        dust.body.velocity.y = dust.body.velocity.y + g.elapsed / 10
      }
    }
    
    self.fire = function() {
      let bullet = new Phaser.Sprite(g, self.x-3.5, self.y-3.5, "turretbullet")
      bullet.animations.add("spin", [0,1,2,3], 10, true)
      bullet.play("spin")
      state.enemyLayer.add(bullet)
      g.physics.enable(bullet)
      g.slopes.enable(bullet)
      bullet.body.anchor = new Phaser.Point(0.5,0.5)
      bullet.body.velocity.x = Math.cos(self.fireAngle) * BULLET_SPEED
      bullet.body.velocity.y = Math.sin(self.fireAngle) * BULLET_SPEED
      let xo = Math.cos(self.fireAngle) * 10 + Math.cos(self.startAngle) * 5
      let yo = Math.sin(self.fireAngle) * 10 + Math.sin(self.startAngle) * 5
      bullet.x += xo
      bullet.y += yo      
      
      /*for(let i = 0; i < 3; i++) {
        let a = self.fireAngle + ((i - 1) / 10)
        self.makeDust(
          xo, yo,
          Math.cos(a) * g.rnd.between(30,60),
          Math.sin(a) * g.rnd.between(30,60)
        )
      }*/
            
      bullet.damage = 20
      bullet.update = function() {
        g.physics.arcade.overlap(bullet, state.tilemapLayer, function(b, t) {
          if(is_colliding_tile_index_plus_stairs(t.index)) {
            bullet.destroy()
          }
        })
      }
    }
    
    /*
    self.enemyRender = function() {
      const colors = [
        'rgba(255,0,0,1)',
        'rgba(255,255,0,1)',
        'rgba(0,255,0,1)',
        'rgba(0,255,255,1)',
        'rgba(0,0,255,1)',
        'rgba(255,0,255,1)',
        'rgba(255,255,255,1)',
      ]      
      for(let i = 0; i < self.debugPts.length; i++){
        if(self.debugPts[i] != null) {
          g.debug.geom(new Phaser.Rectangle(self.debugPts[i].x, self.debugPts[i].y, 1, 1), colors[i])
        }
      }
    }
    */
    
    return self
  }
  
  return ctor
  
})()
