const Pickup = (function() {
  const EXPLODE_SPEED = 20
  const EXPLODE_SPEED_END = 0
  const EXPLODE_TIME_MS = 300
  const SPEED_RATE = .05
  const COLLECT_DIST = 10
  function ctor(state, x, y, angle, image, frames, onTouch) {
    let self = new Phaser.Sprite(g, x, y, image)
    self.animations.add("main", frames, 6, true)
    self.animations.play("main")
    g.physics.enable(self)
    self.x -= self.width / 2
    self.y -= self.height / 2
    self.body.anchor = new Phaser.Point(0.5,0.5)
    
    self.startTime = g.now
    
    self.speed = 0
    self.onTouch = onTouch
    
    self.collect = function() {
      self.onTouch(state.player)
      self.destroy()      
    }
    
    self.update = function() {
      if(g.now < self.startTime + EXPLODE_TIME_MS) {
        let x = Math.cos(angle)
        let y = Math.sin(angle)
        let t = (g.now-(self.startTime + EXPLODE_TIME_MS)) / EXPLODE_TIME_MS
        speed = EXPLODE_SPEED * (1-t) + EXPLODE_SPEED_END * t
        self.body.velocity.x = x * speed
        self.body.velocity.y = y * speed
      } else {
        self.speed = (g.now - self.startTime - EXPLODE_TIME_MS) * SPEED_RATE
        let dx = state.player.x - self.x
        let dy = state.player.y - self.y
        let d = Math.sqrt(dx*dx+dy*dy)
        if(d<COLLECT_DIST) {
          self.collect()
        } else {
          ang = Math.atan2(dy, dx)
          let angBet = g.math.wrapAngle(self.ang - ang, true)
          let frameRotate = g.time.elapsed * (Math.pow(self.speed, 1.1) / 40000 + 0.01)
          if(angBet < -frameRotate) {
            self.ang += frameRotate
          } else if (angBet > frameRotate) {
            self.ang -= frameRotate
          } else {
            self.ang = ang
          }
          self.body.velocity.x = Math.cos(self.ang) * self.speed
          self.body.velocity.y = Math.sin(self.ang) * self.speed
        }
      }
    }
    
    return self
  }
  
  return ctor
})()
