const EnemySpinner = (function() {
  const DOWNFORCE = 110
  const PREPJUMPTIME_MS = 100
  const SIT_DELAY_MS = 350
  const SIT_DELAY_RANDOM_MS = 750
  const JUMP_SPEED = 100
  const DECEL_RATE = 0.02
  const SIT_DECEL_RATE = 0.1
  const STATES = {
    SITTING:"sitting",
    PREPJUMP:"prepjump",
    JUMPING:"jumping",
    BOUNCING:"bouncing"
  }
  
  function ctor(state, enemyId, x, y) {
    let self = EnemyBase(state, enemyId, x+8, y, "enemyspinner", 0)
    self.body.setSize(12, 12, 2, 2)
    self.body.anchor = new Phaser.Point(0.5, 0.5)
    g.slopes.enable(self)
    self.animations.add("expand", [1,2,3,4,5,6], 16, false)
    self.animations.add("spinning", [7,8,9,10], 20, true)
    self.animations.add("contract", [6,5,4,3,2,1,0], 20, false)
    
    self.eye = new Phaser.Sprite(g, 0, 0, "enemyspinnereye")
    self.eye.x = 0
    self.eye.y = 0
    self.addChild(self.eye)
    
    self.state = STATES.SITTING
    self.direction = 1
    self.health = 30
    self.damage = 5
    
    self.grantCredits = 3
    
    self.timeForJump = g.now + SIT_DELAY_MS
    self.careAboutPlatforms = "jumpthrough"
    self.preCollideYVelocity = 0
    self.decel = 0
    
    self.create = function() {
      self.addEnemyState('sitting')
      self.addEnemyState('prepjump')
      self.addEnemyState('jumping')
      self.addEnemyState('bouncing')
      self.sm.transition('sitting')
    }
    
    self.setEyeAngle = function(angle) {
      self.eye.x = g.math.clamp(Math.cos(angle) * 2 - 1, -2, 0)
      self.eye.y = g.math.clamp(Math.sin(angle) * 2 - 1, -2, 0)
    }
    
    self.setEyeAngle(0)
    
    self.flyingUpdate = function() {
      let angle = Math.atan2(self.desiredVelocity.y, Math.abs(self.desiredVelocity.x))
      self.setEyeAngle(angle)  
    }
    
    self.sittingEnter = function() {
      self.desiredVelocity.x = 0
      self.timeForJump = g.now + SIT_DELAY_MS + g.rnd.frac() * SIT_DELAY_RANDOM_MS
      self.decel = SIT_DECEL_RATE
    }
    self.sittingUpdate = function() {
      if(g.now > self.timeForJump - PREPJUMPTIME_MS) {
        self.sm.transition("prepjump")
      }      
    }
    self.sittingExit = function() {}
    self.prepjumpEnter = function() {
      self.decel = DECEL_RATE
      self.setEyeAngle(-Math.PI / 4)
    }
    self.prepjumpUpdate = function() {
      if(g.now > self.timeForJump) {
        self.sm.transition('jumping')
      }      
    }
    self.prepjumpExit = function() {}
    self.jumpingEnter = function() {
      self.decel = DECEL_RATE
      let angle = -3.14159/4 + g.rnd.normal() * 0.1
      let jx = Math.cos(angle)
      let jy = Math.sin(angle)
      self.desiredVelocity.y = jy * JUMP_SPEED
      self.desiredVelocity.x = jx * self.direction * JUMP_SPEED
      self.body.bounce.set(0.5)
      self.animations.play("expand")      
    }
    self.jumpingUpdate = function() {
      self.flyingUpdate()
      if(!self.animations.getAnimation("expand").isPlaying) {
        self.animations.play("spinning")
        self.damage = 25
      }
      if(self.preCollideYVelocity > 0 && self.body.touching.down) {
        self.sm.transition('bouncing')
        self.animations.play("contract")
        self.damage = 5
        self.body.bounce.set(0.3)
      }      
    }
    self.jumpingExit = function() {}
    self.bouncingEnter = function() {
      self.decel = DECEL_RATE
    }
    self.bouncingUpdate = function() {
      self.flyingUpdate()
      if(self.body.touching.down && self.body.velocity.getMagnitudeSq() < 50 * 50)  {
        self.sm.transition('sitting')
      }      
    }
    self.bouncingExit = function() {
      self.body.bounce.set(0)
    }
    
    self.onSaveBeforeHit = function() {
      return {
        frame:self.frame
      }
    }
    
    self.onRestoreAfterHit = function(package) {
      self.frame = package.frame
    }
    
    self.enemyUpdate = function() {
      self.preCollideYVelocity = self.body.velocity.y
      g.physics.arcade.collide(self, state.tilemapLayer)
      //g.physics.arcade.collide(self, state.hintLayer)
      self.desiredVelocity.x = self.body.velocity.x
      self.desiredVelocity.y = self.body.velocity.y
      self.desiredVelocity.y += DOWNFORCE * g.elapsed / 1000.0      
      
      if(self.desiredVelocity.x > 0) {
        self.desiredVelocity.x = Math.max(0, self.desiredVelocity.x - self.decel * g.elapsed)
      } else {
        self.desiredVelocity.x = Math.min(0, self.desiredVelocity.x + self.decel * g.elapsed)
      }
      
      if(self.desiredVelocity.x < 0) { self.direction = -1 }
      if(self.desiredVelocity.x > 0) { self.direction = 1 }
      self.scale.x = self.direction      
    }
    
    let enemyGetHit = self.getHit
    self.getHit = function(dmg, x, y, bullet) {
      if(bullet) {
        if(self.frame == 0) {
          let dx = 0
          let dy = 0
          if(Math.abs(bullet.body.velocity.x) > Math.abs(bullet.body.velocity.y)) {
            dx = Math.sign(bullet.body.velocity.x) * -6
            dy = bullet.y - self.y
          } else {
            dx = bullet.x - self.x
            dy = Math.sign(bullet.body.velocity.y) * -6
          }
          let d = Math.sqrt(dx*dx+dy*dy)
          let ang = Math.atan2(dy/d, dx/d)
          bullet.rotation = ang
          let oldVel = bullet.body.velocity.getMagnitude()
          bullet.body.velocity.x = dx / d * oldVel
          bullet.body.velocity.y = dy / d * oldVel    
          bullet.x += dx / d * 6
          bullet.y += dy / d * 6    
          state.soundEnemyArmorBounce.play()
          //bullet.body.velocity.x = 
          return false
        }
      }
      enemyGetHit(dmg, x, y)
    }
    
    return self
  }
  
  return ctor
})()
