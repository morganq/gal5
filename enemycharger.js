const CrystalSpike = (function() {
  function ctor(state, x, y) {
    let self = new Phaser.Sprite(g, x, y, "crystalspike", 0)
    g.physics.enable(self)
    g.slopes.enable(self)
    self.body.anchor = new Phaser.Point(0.5,0.5)
    self.damage = 15

    self.update = function() {
      if(g.physics.arcade.overlap(self, state.tilemapLayer)) {
        self.destroy()
      }
    }

    return self
  }

  return ctor
})()

const EnemyCharger = (function() {
  const DOWNFORCE = 400
  const WALKSPEED = 18
  const TARGET_RANGE = 100
  const TARGET_VERTICAL_RANGE = 20
  const CHARGING_MAX_SPEED = 130
  const CHARGING_ACCEL = 55
  const RECOVERY_TIME_MS = 1000
  const SPIKE_FIRE_SPEED = 90
  
  const FRAME_SPIKE_OFFSET = [
    new Phaser.Point(-5, -8),
    new Phaser.Point(-5, -7),
    new Phaser.Point(-5, -8),
    new Phaser.Point(-5, -7),
    new Phaser.Point(-5, -6),
    new Phaser.Point(-5, -6),
  ]

  const STATES = {
    WALKING:"walking",
    CHARGING:"charging",
    RECOVERING:"recovering",
  }

  function ctor(state, enemyId, x, y) {
    let self = EnemyBase(state, enemyId, x + 8, y - 8, "enemycharger", 0)
    self.careAboutPlatforms = true
    self.body.setSize(19,19, 2, 4)
    self.body.anchor = new Phaser.Point(0.5,0.5)
    g.slopes.enable(self)
    self.animations.add('_gettinghit', [0], 8, true)
    self.animations.add('stand', [0], 8, true)  
    self.animations.add('walk', [0,1,2,3], 6, true)
    self.animations.add('charge', [4,5], 32, true)
    self.animations.play('walk')
    
    self.spikeGfx = new Phaser.Sprite(g, x, y, "enemycharger-spikes", 0)
    self.addChild(self.spikeGfx)
    self.spikeGfx.x = -5
    self.spikeGfx.y = -8
    self.health = 60

    self.direction = 1;

    self.state = STATES.WALKING

    self.chargeStartTime = 0
    self.recoverStartTime = 0

    self.spikesDropped = 0
    
    self.grantHealth = 3
    self.grantCredits = 4
    self.lastDustFrame = 4
    
    self.shadow.xo = 1
    
    self.create = function() {
      self.addEnemyState('walking')
      self.addEnemyState('charging')
      self.addEnemyState('recovering')
      self.sm.transition('walking')
    }
    
    self.testForBarrier = function() {
      let downright = state.tilemapLayer.getTileXY(self.right + 1, self.bottom + 1, new Phaser.Point())
      let downleft = state.tilemapLayer.getTileXY(self.left - 1, self.bottom + 1, new Phaser.Point())
      let left = Math.min(self.left, self.right)
      let right = Math.max(self.left, self.right)
      function hasTile(x,y) {
        let tiles = state.tilemapLayer.getTiles(x, y, 1, 1)
        if(tiles.length == 0) { return false }
        if(is_colliding_tile_index_plus_platform(tiles[0].index)) { return true }
        return false
      }
      if(self.direction == -1 &&
        (!hasTile(left + 8, self.bottom+4) ||
        hasTile(left-1, self.bottom-4) || 
        hasTile(left-1, self.top))) {
          return true
      }
      else if (self.direction == 1 &&
        (!hasTile(right - 8, self.bottom+4) ||
        hasTile(right+1, self.bottom-4) || 
        hasTile(right+1, self.top))) {
          return true
      }      
      return false
    }
    
    self.walkingEnter = function() {
      self.play("walk")
    }
    
    self.walkingUpdate = function() {
      self.desiredVelocity.x = WALKSPEED * self.direction
      
      if( !state.player.justStartedLevel &&
          Math.sign(state.player.x - self.x) == Math.sign(self.direction) &&
          Math.abs(state.player.y - self.y) < TARGET_VERTICAL_RANGE &&
          Math.abs(state.player.x - self.x) < TARGET_RANGE) {
        self.sm.transition('charging')
        
      }
      if(self.frame != self.lastDustFrame) {
        if(self.frame == 0 || self.frame == 2) {
          state.makeDustParticle("effect-poof1", [0,1,2,3,4,5], self.x + (self.frame * -4 + 5) * self.scale.x, self.y + 7, 0, 0, false, 0)  
        }
        self.lastDustFrame = self.frame
      }      
      
      if(self.testForBarrier()) {
        self.direction = - self.direction
      }
    }
    
    self.walkingExit = function() {
      
    }
    
    self.chargingEnter = function() {
      self.play("charge")
      self.chargeStartTime = g.now
      self.desiredVelocity.x = 0
    }
    
    self.chargingUpdate = function() {
      let speed = ((g.now - self.chargeStartTime) / 1000) ** 2 * CHARGING_ACCEL
      speed = Math.min(speed, CHARGING_MAX_SPEED)
      self.desiredVelocity.x = speed * self.direction
      self.damage = 15 + Math.abs(self.desiredVelocity.x) / 5
      if(self.testForBarrier()) {
        self.sm.transition('recovering')
      }
    }
    
    self.chargingExit = function() {
      self.damage = 15
    }
    
    self.recoveringEnter = function() {
      self.recoverStartTime = g.now
      self.animations.play("stand")
    }
    
    self.recoveringUpdate = function() {
      self.desiredVelocity.x = 0
      if(g.now > self.recoverStartTime + RECOVERY_TIME_MS) {
        self.direction = -self.direction
        self.sm.transition('walking')
      }      
    }
    
    self.recoveringExit = function() {
      
    }
    
    self.onSaveBeforeHit = function() {
      return {chargeStartTime:self.chargeStartTime, velocity:self.desiredVelocity}
    }
    
    self.onRestoreAfterHit = function(package) {
      self.chargeStartTime = package.chargeStartTime
      self.desiredVelocity = package.velocity
    }

    self.enemyUpdate = function() {
      g.physics.arcade.collide(self, state.tilemapLayer)
      self.desiredVelocity = self.body.velocity
      self.desiredVelocity.y += DOWNFORCE * g.elapsed / 1000.0

      self.scale.x = self.direction

      self.spikeGfx.x = FRAME_SPIKE_OFFSET[self.frame].x
      self.spikeGfx.y = FRAME_SPIKE_OFFSET[self.frame].y
    }

    let enemyGetHit = self.getHit
    self.getHit = function(damage, x, y) {
      enemyGetHit(damage, x, y)
      if(self.spikesDropped < 3) {
        let xv = 0
        let yv = 0
        let yo = 0
        let angle = 0
        if(self.spikesDropped == 0) {
          xv = -self.direction * SPIKE_FIRE_SPEED
          yv = 0
          yo = 3
          angle = (self.direction > 0) ? -90 : 90
        }
        if(self.spikesDropped == 1) {
          xv = -self.direction * SPIKE_FIRE_SPEED * 0.717
          yv = -SPIKE_FIRE_SPEED * 0.717
          angle = angle = (self.direction > 0) ? -45 : 45
        }
        if(self.spikesDropped == 2)  {
          xv = 0
          yv = -SPIKE_FIRE_SPEED
          angle = 0
        }
        //state.makeDustParticle("muzzleflash", [0,1,2,3,4,5], self.x, self.y + yo, xv/3, yv/3, true, angle)
        let spike = new CrystalSpike(state, self.x, self.y+yo)
        spike.anchor.set(0.5,0.5)
        state.enemyLayer.add(spike)
        spike.body.velocity.x = xv
        spike.body.velocity.y = yv
        spike.angle = angle        
        self.spikesDropped += 1
        self.spikeGfx.frame = Math.min(self.spikesDropped,3)
        spike.spawnTime = g.now
        spike.update = function() {
          if(g.rnd.frac() < 0.2) {
            state.makeDustParticle("effect-spiketrail", [0,1,2,3], spike.x, spike.y, 0, 0, false, spike.angle+90)
          }
          
          g.physics.arcade.overlap(spike, state.tilemapLayer, function(s, tile) {
            if(is_colliding_tile_index_plus_stairs(tile.index)) {
              spike.destroy()
            }
          })
          
          if(g.now > spike.spawnTime + 3000) { spike.destroy() }
        }
      }
    }

    return self
  }
  return ctor

})()
