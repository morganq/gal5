const Usable = (function() {
  const ACTIVE_RANGE = 20
  let ctor = function(state, x, y, key){
    let self = new Phaser.Sprite(g, x, y, key)
    self.active = false
    
    self.text = 'Use'
    
    self.useDisplay = new Phaser.BitmapText(g, 0, 0, "visitor", "", 10)
    self.useDisplay.anchor.set(0.5, 0.5)
    state.UILayer.addChild(self.useDisplay)
    self.useDisplay.tint = 0xbb499a
    self.useDisplay.visible = false
    
    self.present = function() {
      self.useDisplay.text = "[A] " + self.text
      self.useDisplay.visible = true
      self.useDisplay.x = self.left + self.width / 2
      self.useDisplay.y = self.top - 8      
    }
    
    self.revoke = function() {
      self.useDisplay.visible = false
    }    
    
    self.use = function() {
      console.log("Override use() function")
    }
    
    self.usableUpdate = function() {}
    
    self.update = function() {
      let dx = state.player.x - (self.left + self.width / 2)
      let dy = state.player.y - (self.top + self.height / 2)
      let d = dx*dx+dy*dy
      if(d <= ACTIVE_RANGE ** 2) {
        if(self.active) {
          if(g.useButton.downDuration(1)) {
            self.use()
          }
        } else {
          self.present()
          self.active = true
        }
      } else {
        if(self.active) {
          self.revoke()
          self.active = false
        }
      }  
      self.usableUpdate()    
    }
    
    return self
  }
  return ctor
})()
