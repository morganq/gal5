function StateMachine(debug) {
  let self = {}
  
  self.debug = !!debug
  
  if(self.debug) {
    self.debugText = new Phaser.BitmapText(g, 0, 0, 'visitor', 'null', 10)
    self.debugText.tint = 0x990000
  }
  
  self.states = {}
  self.currentState = null
  
  self.add = function(name, enter, update, exit) {
    self.states[name] = {
      name:name,
      enter:enter,
      update:update,
      exit:exit
    }
  }
  
  self.transition = function(name) {
    if(!self.states[name]) {
      console.log("No state named", name)
      return
    }
    if(self.currentState && self.currentState.exit) {
      self.currentState.exit()
    }
    if(self.states[name].enter) { 
      self.states[name].enter()
    }    
    self.currentState = self.states[name]
    if(self.debug) { self.debugText.text = name }
  }
  
  self.update = function() {
    if(self.currentState && self.currentState.update) {
      self.currentState.update()
    }
  }
  
  return self
}
