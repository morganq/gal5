const ItemRandomSticker = (function() {
  const ACTIVE_RANGE = 12
  const DEFAULT_STICKERS = 2
  
  function ctor(state, x, y, levelNum, rarity) {
    let self = new Phaser.Sprite(g, x + 4, y, "item-randomsticker")
    if(rarity == "rare") { self.frame = 2}
    self.anchor.set(0.5, 0.5)
    self.active = false
    self.stickersLeft = DEFAULT_STICKERS
    
    self.rarity = rarity
    self.levelNum = levelNum
    
    self.priceTag = new Phaser.BitmapText(g, 0, 0, "visitor", "$", 10)
    self.priceTag.anchor.set(0.5, 0.5)
    self.addChild(self.priceTag)
    self.priceTag.x = 0
    self.priceTag.y = -16
    self.priceTag.tint = 0xbb499a
    self.priceTag.visible = false
    
    self.presentChoice = function() {
      self.price = Math.floor((levelNum**1.25) * (10 + ((rarity == "rare") ? 10 : 0)))
      self.price *= 4 ** (DEFAULT_STICKERS - self.stickersLeft)
      self.priceTag.text = "[A] buy $" + self.price      
      self.priceTag.visible = true
    }
    
    self.revokeChoice = function() {
      self.priceTag.visible = false
    }
    
    self.update = function() {
      if(self.stickersLeft <= 0) {
        self.frame = 1
        return
      }
      let dx = state.player.x - self.x
      let dy = state.player.y - self.y
      let d = dx*dx+dy*dy
      if(d <= ACTIVE_RANGE ** 2) {
        if(self.active) {
          if(g.useButton.downDuration(1)) {
            if(g.credits >= self.price) {
              g.credits -= self.price
            
              self.stickersLeft -= 1
              if(self.stickersLeft <= 0) {
                self.revokeChoice()
                self.frame = 1
              } else {
                self.presentChoice()
              }
              let sticker = g.grantSticker(g.getRandomSticker(self.levelNum, self.rarity))
              let color = COLOR_FOR_RARITY[sticker.rarity]
              state.award(sticker.sprite, sticker.name, color, x, y)
              g.dungeon.savePurchase()
            }
          }
        } else {
          self.presentChoice()
          self.active = true
        }
      } else {
        if(self.active) {
          self.revokeChoice()
          self.active = false
        }
      }
      if(self.active) {
        self.priceTag.y = -16 + Math.sin(g.now / 300) * 2
      }
    }
    
    return self
  }
  
  return ctor
})()
