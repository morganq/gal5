let allianceFrames = {
  'mosaic':0,
  'homeworld':1,
  'yz':2,
  'roth':3,
  'seers':4,
  'fragment':5
}
let achievementFrames = {
  'locked':0,
  'augment':1,
  'gun':2,
  'dodge':3,
  'upgrade':4
}

function AllianceSubmenu(state, menuValues, dialog, buttons) {
  let self = MenuBase(state, 0xffffff, 0, 0, 282, 109)
  
  let bg = new Phaser.Sprite(g, 0, 0, 'alliancemenu')
  self.addChild(bg)
  
  let alliance = new Phaser.Sprite(g, 26, 7, 'civflags', allianceFrames[menuValues.alliance])
  self.addChild(alliance)
  
  let title = new Phaser.BitmapText(g, 49, 2, "visitor", menuValues.name, 20)
  title.tint = 0x03022a
  self.addChild(title)
  let slogan = new Phaser.BitmapText(g, 49, 17, "visitor", menuValues.slogan, 10)
  slogan.tint = 0x616377
  self.addChild(slogan)
  
  let words = dialog.split(" ")
  let d = ""
  let c = 0
  words.forEach(function(w) {
    if(c + w.length > 36) {
      d = d + "\n"
      c = 0
    }
    if(c == 0) {
      d = d + w
    } else {
      d = d + " " + w
    }
    c += w.length
  })
  
  let text = new Phaser.BitmapText(g, 25, 28, "visitor", d, 10)
  text.tint = 0x03022a
  self.addChild(text)
  
  if(buttons.length > 0) {
    self.optionsPanel = GridSelectionPanel(state, self, 999, 999)
    self.optionsPanel.setGrid([buttons.map(function(b) {
      let grp = new Phaser.Group(g)
      let bg = new Phaser.Graphics(g, 0, 0)
      bg.beginFill(0xffffff)
      bg.drawRect(0, 0, 1, 10)
      bg.endFill()
      grp.addChild(bg)      
      let txt = new Phaser.BitmapText(g, 0,0,"visitor", b.text, 10)
      txt.tint = 0x03022a
      grp.addChild(txt)
      return {
        'element':grp,
        'value':b.callback
      }
    })])
    
    self.optionsPanel.x = 25
    self.optionsPanel.y = 80
    self.optionsPanel.scrolling = false
    self.addChild(self.optionsPanel)
    
    self.openUpdate = function() {
      if(g.selectButton.downDuration(1) && self.optionsPanel) {
        let cb = self.optionsPanel.getSelectedValue()
        cb(self)
      }
    }
  }
  
  
  
  return self
}

function AllianceMenu(state, menuValues) {
  let self = MenuBase(state, 0xffffff, 3, 3, 282, 109)
  
  let bg = new Phaser.Sprite(g, 0, 0, 'alliancemenu')
  self.addChild(bg)
  
  let alliance = new Phaser.Sprite(g, 26, 7, 'civflags', allianceFrames[menuValues.alliance])
  self.addChild(alliance)
  
  let title = new Phaser.BitmapText(g, 49, 2, "visitor", menuValues.name, 20)
  title.tint = 0x03022a
  self.addChild(title)
  let slogan = new Phaser.BitmapText(g, 49, 17, "visitor", menuValues.slogan, 10)
  slogan.tint = 0x616377
  self.addChild(slogan)
  
  self.dialogText = new Phaser.BitmapText(g, 25, 32, "visitor", "hi", 10)
  self.dialogText.tint = 0x4da146
  self.addChild(self.dialogText)
  
  let sw = 121
  let sh = 17
  self.selectionPanel = GridSelectionPanel(state, self, 260, 82, sw, sh)
  self.selectionPanel.xPadding=2
  self.selectionPanel.yPadding=2
  self.selectionPanel.x = 21
  self.selectionPanel.y = 54
  self.selectionPanel.scrolling = false
  
  function makeMenuIcon(name, frame, switchFn) {
    let grp = new Phaser.Group(g)
    let bg = new Phaser.Graphics(g, 0, 0)
    bg.beginFill(0xffffff)
    bg.drawRect(0, 0, sw, sh)
    bg.endFill()
    grp.addChild(bg)
    let icon = new Phaser.Sprite(g, 0, 0, 'achievementicons')
    grp.addChild(icon)
    let text = new Phaser.BitmapText(g, 20, -1, "visitor", menuValues[name], 10)
    text.tint = 0x03022a
    if(g.alliances[menuValues.alliance][name].unlocked) {
      icon.frame = frame
      if(switchFn) {
        let switchText = new Phaser.BitmapText(g, 20, 8, "visitor", 'equip', 10)
        switchText.tint = 0xbb499a
        grp.addChild(switchText)
      }      
    }
    else {
      icon.frame = 0
      text.text = menuValues[name + "Achievement"] + "\nto unlock"
      text.tint = 0xa2a091
    }
    grp.addChild(text)
    
    return grp
  }
  
  self.bonus = makeMenuIcon('bonus',4)
  self.gun = makeMenuIcon('gun',2, true)
  self.dodge = makeMenuIcon('dodge',3,true)
  self.augment = makeMenuIcon('augment',1,true)
  
  let grid = [
    [
      {'element':self.bonus, 'value':'bonus'},
      {'element':self.dodge, 'value':'dodge'}
    ], 
    [
      {'element':self.gun, 'value':'gun'},
      {'element':self.augment, 'value':'augment'}      
    ]
  ]  
  self.selectionPanel.setGrid(grid)
  self.addChild(self.selectionPanel)
  
  
  self.onOpen = function() {
    self.dialogText.text = ""
    self.nextDialogTextTimer = g.time.now
    
    if(g.alliances[menuValues.alliance].gun.enabled != self.gun.enabled && self.gun.toggle) {
      self.gun.toggle()
    }
    if(g.alliances[menuValues.alliance].dodge.enabled != self.dodge.enabled && self.dodge.toggle) {
      self.dodge.toggle()
    }
    if(g.alliances[menuValues.alliance].augment.enabled != self.augment.enabled && self.augment.toggle) {
      self.augment.toggle()
    }        
    
  }
  self.openUpdate = function() {
    if(g.time.now > self.nextDialogTextTimer) {
      self.nextDialogTextTimer = g.time.now + 35
      self.dialogText.text = menuValues.dialog.substr(0, self.dialogText.text.length + 1)
    }
    
    let disableOthers = function(name) {
      Object.values(g.alliances).forEach(function(civ) {
        if(civ[name].unlocked) {
          civ[name].enabled = false
        }
      })
    }
    
    if(g.selectButton.downDuration(1)) {
      let value = self.selectionPanel.getSelectedValue()
      let unlocked = g.alliances[menuValues.alliance][value].unlocked
      if(unlocked) {
        if(value == 'bonus') {
          let buttons = [{text:'back', callback:function(sub){sub.close()}}]
          if(menuValues.alliance == "roth") {
            buttons.push({
              text:'yes!',
              callback:function(sub) {
                let sticker = g.grantSticker(g.getRandomSticker(1, 'rare'))
                let color = COLOR_FOR_RARITY[sticker.rarity]
                state.award(sticker.sprite, sticker.name, color, 0, 0)
                sub.close()
                self.close()
              }
            })
          }
          self.subMenu = AllianceSubmenu(state, menuValues, menuValues.bonusText, buttons)
          self.openChildMenu(self.subMenu)
        }
        if(value == 'gun') {
          disableOthers('gun')
          g.weapons[0] = Weapon[menuValues.gun]()
          state.updateWeapons()
          g.alliances[menuValues.alliance].gun.enabled = true
        }
        if(value == 'dodge') {
          disableOthers('dodge')
          g.alliances[menuValues.alliance].dodge.enabled = true
        }
        if(value == 'augment') {
          disableOthers('augment')
          g.alliances[menuValues.alliance].augment.enabled = true
        }                
      }
    }
  }
  
  
  return self
}

function Ally(state, x, y, key, menuValues) {
  let self = Usable(state, x, y, key)
  self.text = "Talk"
  self.menuValues = menuValues
  self.menu = AllianceMenu(state, menuValues)
  self.use = function() {
    self.menu.open()
    self.play("talk")
  }
  let oldUpdate = self.update
  self.update = function() {
    oldUpdate()
    if(!state.inMenu) {
      self.play("base")
    }
  }
  state.menuLayer.add(self.menu)
  return self
}

function lobbySpawn(self) {
  let fragment = Ally(self,28 * GS, 47 * GS, 'fragment-lobby', ALLIANCES.fragment)
  fragment.animations.add('base', [0,1,2,3,4,5,6,7], 8, true)
  fragment.play('base')
  self.gameLayer.add(fragment)
  
  Shadow(self, fragment, 10, 0, -1)
  
  let mosaic = Ally(self,22 * GS, 48 * GS - 4, 'mosaic-lobby', ALLIANCES.mosaic)
  mosaic.animations.add('base', [0,0,0,0,0,0,0,0,0,1,2,3,3,3,3,3,3,3,4,4,4,3,3,3,3,3,3,3,3,3,3,3,3,3,3,4,4,4,3,3,3,3,3,3,3,2,1], 6, true)
  mosaic.play('base')
  self.gameLayer.add(mosaic)    
  
  mosaic.smokeTimer = g.now
  
  let sf = [
    new Phaser.Point(5,7),
    new Phaser.Point(2,8),
    new Phaser.Point(3,11),
    new Phaser.Point(2,12),
    new Phaser.Point(2,12),
  ]
  
  mosaic.usableUpdate = function() {
    if(g.now > mosaic.smokeTimer) {
      let pt = sf[mosaic.frame]
      self.makeDustParticle('crystaldust', [0,1,2,2,2,3,3,3,4,4,4,5,5,5,6,6,6], mosaic.x + pt.x, mosaic.y + pt.y, 0, -10, 0, 0)
      mosaic.smokeTimer = g.now + g.rnd.between(300, 600)
      
    }
    if(mosaic.animations.currentAnim._frameIndex > 13 && mosaic.animations.currentAnim._frameIndex < 15 ) {
      self.makeDustParticle('crystaldust', [0,1,2,2,2,3,3,3,4,4,4,5,5,5,6,6,6],
      mosaic.x + 7, mosaic.y + 7,
      g.rnd.between(-20,-5), g.rnd.between(-7,-14), 0, 0)
    }
  }
  Shadow(self, mosaic, 10, 1, -1)
  
  let seer = Ally(self,35 * GS, 46 * GS, 'seer-lobby', ALLIANCES.seers)
  Shadow(self, seer, 12, 1, -1)
  seer.usableUpdate = function() {
    seer.y = 46 * GS + 2 + Math.sin(g.now / 200) * 1.5
  }
  self.gameLayer.add(seer)  
  
  let homeworld = Ally(self,40 * GS, 47 * GS - 2, 'homeworld-lobby', ALLIANCES.homeworld)
  homeworld.animations.add('base', [0,1,2,3,4,5,6], 4, true)
  homeworld.play('base')
  Shadow(self, homeworld, 20, 0, -1)
  self.gameLayer.add(homeworld) 
  
  let roth = Ally(self,48 * GS, 46 * GS, 'roth-lobby', ALLIANCES.roth)
  roth.animations.add('base', [0,0,0,0,0,0,2,2,2,2,2,0,0,0,0,0], 3, true)
  roth.animations.add('talk', [3], 1, false)
  roth.anchor.set(0.5, 0)
  roth.play('base')
  roth.usableUpdate = function() {
    if(self.player.x - (roth.x + roth.width/2) > 0) {
      roth.scale.x = -1
    } else {
      roth.scale.x = 1
    }
  }
  Shadow(self, roth, 14, 0, -1)
  
  let rothUse = roth.use
  roth.use = function() {
    rothUse()
    self.player.x = roth.x - 7
    self.player.y = roth.y + 24
    self.player.scale.x = 1
    roth.scale.x = 1
  }
  
  self.gameLayer.add(roth)    
  
  g.camera.bounds = new Phaser.Rectangle(0, 0,800, 424)
}
