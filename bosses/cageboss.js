const CageBoss = (function() {
  
  const STATES = {
    STAGE1:'stage1',
    FALLING:'falling',
    HELPLESS:'helpless'
  }
  
  const MINX = 38 * GS
  const MINY = 34 * GS
  const MAXX = 61 * GS
  const MAXY = 56 * GS
  const CENTERX = 45 * GS
  const CENTERY = 45 * GS
  const CENTEROFFSET = 32
  const RADIUS = 1.2 * (CENTERX - MINX)
  const NEAR_DIST = 30
  
  const ACCEL = 0.15
  const MAXYV = 70
  const MAXXV = 70
  
  const RELOAD_TIME = 1500
  const LASER_ROTATE_SPEED_MIN = 0.0009
  const LASER_STRAY_SPEED = 0.0002
  const LASER_ROTATE_SPEED_MAX = 0.0015
  const LASER_AIM_TIME = 2100
  const LASER_FIRE_TIME = 1500
  const LASER_DELAY = 950
  
  const SPAWNDELAY = 1300
  const NUMSPAWN = 3
  
  
  function ctor(state, enemyId, x, y) {
    self = new EnemyBase(state, enemyId, x, y, "cageboss")
    self.body.setSize(60, 44, 7, 7)
    self.body.anchor = new Phaser.Point(0.5,0.5)
    self.x = CENTERX + CENTEROFFSET
    
    g.slopes.enable(self)
    
    self.inside = new Phaser.Sprite(g, 0, 0, "cageboss-inside")
    self.inside.anchor = new Phaser.Point(0.5,0.5)
    self.addChild(self.inside)
    self.inside.x = 0
    self.inside.y = -4
    
    self.chains = []
    self.chains.push(new Phaser.Sprite(g, 0, 0, "cageboss-chain"))
    self.addChild(self.chains[0])
    self.chains[0].x = -4
    self.chains[0].y = -(self.chains[0].height + 28)
    
    self.chains.push(new Phaser.Sprite(g, 0, 0, "cageboss-chain"))
    self.addChild(self.chains[1])
    self.chains[1].angle = 90
    self.chains[1].scale.y = -1
    self.chains[1].x = 36
    self.chains[1].y = -2
    
    self.chains.push(new Phaser.Sprite(g, 0, 0, "cageboss-chain"))
    self.addChild(self.chains[2])
    self.chains[2].x = -4
    self.chains[2].y = 27
    
    self.chains.push(new Phaser.Sprite(g, 0, 0, "cageboss-chain"))
    self.addChild(self.chains[3])
    self.chains[3].angle = 90
    self.chains[3].scale.y = -1
    self.chains[3].x = -(self.chains[0].height + 36)
    self.chains[3].y = -1 
    
    self.turret = new Phaser.Sprite(g, 0, 0, "cageboss-turret-base")
    self.turret.anchor = new Phaser.Point(0.5, 0.5)
    self.addChild(self.turret)
    
    self.turret.gun = new Phaser.Sprite(g, 0, 0, "cageboss-turret")
    self.turret.gun.anchor = new Phaser.Point(0.5, 0.5)
    self.turret.addChild(self.turret.gun)
    self.turret.gun.x = 0
    self.turret.gun.y = 0
    
    self.laserbeam = new Phaser.Graphics(g, 0, 0)
    self.addChild(self.laserbeam)
    self.laserbeam.x = 0
    self.laserbeam.y = 0
    
    self.turretAngle = 0
    
    self.health = 500
    self.maxHealth = 500
    
    self.grantHealth = 20
    self.grantCredits = 100
    
    self.fireAngleMode = 0
    self.fireAngle = 0
    self.nextFireTime = 0
    
    self.state = STATES.STAGE1
    self.laserAimStartTime = g.now
    self.lastLaserTrackPlayerDir = 0
    
    self.nextTargetTime = 0
    
    self.weakLinks = []
    
    self.nextSpawnTime = 0
    self.spawnsLeft = NUMSPAWN
    
    self.debugLine1 = null
    self.debugLine2 = null
    
    self.setupLinks = function() {
      self.weakLinks = []
      self.weakLinks.push(new EnemyBase(state, enemyId + 99999 + self.weakLinks.length, 0, 0, "cageboss-weaklink"))
      
      self.weakLinks.push(new EnemyBase(state, enemyId + 99999 + self.weakLinks.length, 0, 0, "cageboss-weaklink"))
      self.weakLinks[1].angle = 90
      self.weakLinks[1].scale.y = -1
      
      self.weakLinks.push(new EnemyBase(state, enemyId + 99999 + self.weakLinks.length, 0, 0, "cageboss-weaklink"))
      
      self.weakLinks.push(new EnemyBase(state, enemyId + 99999 + self.weakLinks.length, 0, 0, "cageboss-weaklink"))
      self.weakLinks[3].angle = 90
      self.weakLinks[3].scale.y = -1      
      
      for(let i = 0; i < 4; i++) {
        state.enemyLayer.add(self.weakLinks[i])
        self.weakLinks[i].damage = 0
        self.weakLinks[i].health = 30
        self.weakLinks[i].grantHealth = 5
        self.weakLinks[i].grantCredits = 0
        self.weakLinks[i].visible = false
        self.weakLinks[i].hittable = false
      }
      self.weakLinks[2].visible = true
      self.weakLinks[2].hittable = true
    }
    
    self.newTarget = function() {
      
      // if we're missing the top chain then we can only move left-right
      // if we're missing a side chain, we can only move up-down
      let minr = 0
      let maxr = 2
      if(self.weakLinks.length == 4) {
        if(!self.weakLinks[0].exists) {
          maxr = 0
        }
        if(!self.weakLinks[1].exists || !self.weakLinks[3].exists) {
          minr = 1
        }
        if(minr > maxr) { return }
      }
      if(g.rnd.between(minr,maxr) == 0) {
        let x = (self.x < CENTERX) ? (CENTERX + CENTEROFFSET) : (CENTERX - CENTEROFFSET)
        self.targetPt = new Phaser.Point(x, self.y)
      } else {
        self.targetPt = new Phaser.Point(self.x, g.rnd.between(MINY, MAXY))
      }
    }
    
    self.newTarget()
    
    self.laserUpdate = function() {
      if(g.now < self.nextAimTime) { return }
      let offAngle = 0
      let a = 0
      let speed = LASER_ROTATE_SPEED_MIN
      if(g.now < self.laserAimStartTime + LASER_AIM_TIME) {
        let dx = state.player.x - self.x
        let dy = state.player.y - self.y
        a = Math.atan2(dy, dx)      
        
        offAngle = g.math.wrapAngle(self.turretAngle - a, true)
        if(offAngle != 0) {
          self.lastLaserTrackPlayerDir = offAngle
        }
        
      } else if(g.now < self.laserAimStartTime + LASER_AIM_TIME + LASER_DELAY) {
        self.laserbeam.clear();
        
        (function() {
          let lx = Math.cos(self.turretAngle)
          let ly = Math.sin(self.turretAngle)
          const turretOffset = 22
          const startX = self.turret.x + lx * turretOffset
          const startY = self.turret.y + ly * turretOffset
          self.laserbeam.beginFill(0xd01f02, 1)
          
          for(let i = 0; i < 25; i++) {
            let t = (i * 20 + g.now / 10) % 500
            
            self.laserbeam.drawCircle(
              t * lx + startX,
              t * ly + startY,
              3)
          }
        })();
        self.laserbeam.endFill()  
                
      } else if(g.now < self.laserAimStartTime + LASER_AIM_TIME + LASER_DELAY + LASER_FIRE_TIME) {
        offAngle = -self.lastLaserTrackPlayerDir
        speed = LASER_STRAY_SPEED
        self.laserbeam.clear()
        
        let lx = Math.cos(self.turretAngle)
        let ly = Math.sin(self.turretAngle)
        
        const turretOffset = 22
        const startX = self.turret.x + lx * turretOffset
        const startY = self.turret.y + ly * turretOffset
        
        self.laserbeam.beginFill(0xd01f02, 1)
        self.laserbeam.drawCircle(
          startX,
          startY,
          27)
        self.laserbeam.endFill()
        
        self.laserbeam.beginFill(0xffffff, 1)
        self.laserbeam.drawCircle(
          startX,
          startY,
          Math.sin(g.now / 30) * 6 + 15)
        self.laserbeam.endFill()                
        
        self.laserbeam.lineStyle(27, 0xd01f02, 1)
        self.laserbeam.moveTo(startX, startY)
        self.laserbeam.lineTo(
          lx * 500 + startX,
          ly * 500 + startY)
        
        self.laserbeam.lineStyle(0, 0x000000, 0)        
        
        for(let i = 0; i < 25; i++) {
          let t = (i * 20 + g.now / 10) % 500
          
          /*self.laserbeam.beginFill(0xd01f02, 1)
          self.laserbeam.drawCircle(t * lx + self.turret.x + lx * 10, t * ly + self.turret.y + ly * 10, 13)
          self.laserbeam.endFill()          
          */
          
          self.laserbeam.beginFill(0xffffff, 1)
          self.laserbeam.drawCircle(
            t * lx + startX,
            t * ly + startY,
            Math.sin(t / 12) * 3 + 18)
          self.laserbeam.endFill()
        }
        
        self.laserbeam.lineStyle(Math.sin(g.now / 30) * 6 + 15, 0xffffff, 1)        
        self.laserbeam.moveTo(startX,startY)
        self.laserbeam.lineTo(
          lx * 500 + startX,
          ly * 500 + startY)        
        
        let off1x = ly * 10
        let off1y = -lx * 10
        
        let line1 = new Phaser.Line(
          self.x + startX + off1x,
          self.y + startY + off1y,
          self.x + startX + lx * 500 + off1x,
          self.y + startY + ly * 500 + off1y)
          
        let line2 = new Phaser.Line(
          self.x + startX - off1x,
          self.y + startY - off1y,
          self.x + startX + lx * 500 - off1x,
          self.y + startY + ly * 500 - off1y)
          
        self.debugLine1 = line1
        self.debugLine2 = line2
        
        let rect = new Phaser.Rectangle()
        rect.copyFrom(state.player.body)
        let intersect1 = Phaser.Line.intersectsRectangle(
          line1,
          rect);
        let intersect2 = Phaser.Line.intersectsRectangle(
          line2,
          rect);        
            
          
        if(intersect1 || intersect2) {
          if(state.player.canBeHit()) { state.player.getHit(35, 0) }
        }
        
        
      } else {
        self.laserbeam.clear()
        offAngle = 0
        self.nextAimTime = self.laserAimStartTime + LASER_AIM_TIME + LASER_FIRE_TIME + RELOAD_TIME
        self.laserAimStartTime = self.nextAimTime
      }
        
      let angleRotateAmt = g.elapsed * speed
      
      if(offAngle < -angleRotateAmt) {
        self.turretAngle = g.math.wrapAngle(self.turretAngle + angleRotateAmt, true)
      } else if (offAngle > angleRotateAmt){
        self.turretAngle = g.math.wrapAngle(self.turretAngle - angleRotateAmt, true)
      }
      
      self.turret.x = g.math.clamp(Math.cos(self.turretAngle) * 34, -24, 25)
      self.turret.y = g.math.clamp(Math.sin(self.turretAngle) * 23, -19, 11)
      self.turret.gun.angle = self.turretAngle * 180 / 3.14159
    }
    
    self.enemyUpdate = function() {
      if(self.weakLinks.length == 0) {
        self.setupLinks()
      }
      
      let nextState = null
      
      if(self.state == STATES.STAGE1) {
        self.laserUpdate()
        
        let tx = self.targetPt.x - self.x
        let ty = self.targetPt.y - self.y      
        
        if(self.nextTargetTime > 0 && g.now > self.nextTargetTime) {
          self.newTarget()
          self.nextTargetTime = 0
        }
        
        self.frame = 0
        if(tx > 2) {
          self.desiredVelocity.x += ACCEL * g.elapsed
          self.frame = 2
        } else if (tx < -2) {
          self.desiredVelocity.x -= ACCEL * g.elapsed
          self.frame = 2
        }
        if(ty > 2) {
          self.desiredVelocity.y += ACCEL * g.elapsed
          self.frame = 1
        } else if (ty < -2) {
          self.desiredVelocity.y -= ACCEL * g.elapsed
          self.frame = 1
        }
        
        if(Math.abs(tx) < 4 && Math.abs(ty) <4 && g.now > self.nextTargetTime) {
          self.nextTargetTime = g.now + 2000
        }
        
        self.desiredVelocity.x = g.math.clamp(self.desiredVelocity.x, -MAXXV, MAXXV)
        self.desiredVelocity.y = g.math.clamp(self.desiredVelocity.y, -MAXYV, MAXYV)
        self.desiredVelocity.x *= 0.95
        self.desiredVelocity.y *= 0.95
        
        self.weakLinks[0].x = self.x 
        self.weakLinks[0].y = self.y - 58
        
        self.weakLinks[1].x = self.x + 67
        self.weakLinks[1].y = self.y + 2
        
        self.weakLinks[2].x = self.x 
        self.weakLinks[2].y = self.y + 58
        
        self.weakLinks[3].x = self.x - 66
        self.weakLinks[3].y = self.y + 3
        
        for(let i =0; i < 4; i++) {
          self.chains[i].exists = self.weakLinks[i].exists
        }
        
        if(self.weakLinks[2].exists) {
          self.weakLinks[2].visible = true; self.weakLinks[2].hittable=true
        }
        if(!self.weakLinks[2].exists && self.weakLinks[1].exists) {
          self.weakLinks[1].visible = true; self.weakLinks[1].hittable=true
        }
        if(!self.weakLinks[1].exists && self.weakLinks[3].exists) {
          self.weakLinks[3].visible = true; self.weakLinks[3].hittable=true
        }        
        if(!self.weakLinks[3].exists && self.weakLinks[0].exists) {
          self.weakLinks[0].visible = true; self.weakLinks[0].hittable=true
        }        
        
        if(!self.weakLinks[0].exists && (!self.weakLinks[1].exists || !self.weakLinks[3].exists)) {
          nextState = STATES.FALLING
          for(let i =0; i < 4; i++) {
            self.weakLinks[i].getHit(9999, 0, 0)
            self.chains[i].exists = false
          }          
        }
      }
      
      if(self.state == STATES.FALLING) {
        self.laserbeam.clear()
        self.desiredVelocity.y = self.body.velocity.y
        self.desiredVelocity.y += 0.1 * g.elapsed
        self.desiredVelocity.x = 0
        self.angle -= g.elapsed / 400
        g.physics.arcade.collide(self, state.tilemapLayer, function(s,l) {
          self.desiredVelocity.y = - self.desiredVelocity.y * 0.5
          if(Math.abs(self.desiredVelocity.y) < 25) {
            nextState = STATES.HELPLESS
            self.desiredVelocity.y = 0
            self.nextSpawnTime = g.now + SPAWNDELAY * 2
            self.spawnsLeft = NUMSPAWN
          }          
        })
      }
      
      if(self.state == STATES.HELPLESS) {
        if(g.now > self.nextSpawnTime) {
          if(self.spawnsLeft <= 0) {
            nextState = STATES.STAGE1
            self.angle = 0
            self.setupLinks()
            self.laserAimStartTime = g.now + RELOAD_TIME
            self.targetPt = new Phaser.Point(self.x, CENTERY)
            for(let i = 0; i < 4; i++) {
              self.chains[i].exists = true
            }
          } else {
            let spinner = EnemySpinner(state, -1, self.x, self.y)
            spinner.state = 'jumping'
            spinner.body.velocity.x = g.rnd.between(-150, 150)
            spinner.body.velocity.y = g.rnd.between(-50, -100)
            state.enemyLayer.add(spinner)
            self.nextSpawnTime = g.now + SPAWNDELAY
            self.spawnsLeft -= 1
          }
        }
      }
      
      let dx = state.player.x - self.x
      let dy = state.player.y - self.y
      let a = Math.atan2(dy, dx)
      self.inside.frame = Math.floor(g.math.normalizeAngle(a + 6.2818/16) / 6.2818 * 8)      
      
      if(nextState && nextState != self.state) {
        console.log(nextState)
        self.state = nextState
      }
    }
    
    let enemyGetHit = self.getHit
    self.getHit = function(dmg, x, y, bullet) {
      if(bullet) {
        if(self.state != STATES.HELPLESS) {
          let dx = 0
          let dy = 0
          if(Math.abs(bullet.body.velocity.x) > Math.abs(bullet.body.velocity.y)) {
            dx = Math.sign(bullet.body.velocity.x) * -6
            dy = bullet.y - self.y
          } else {
            dx = bullet.x - self.x
            dy = Math.sign(bullet.body.velocity.y) * -6
          }
          let d = Math.sqrt(dx*dx+dy*dy)
          let ang = Math.atan2(dy/d, dx/d)
          bullet.rotation = ang
          let oldVel = bullet.body.velocity.getMagnitude()
          bullet.body.velocity.x = dx / d * oldVel
          bullet.body.velocity.y = dy / d * oldVel    
          bullet.x += dx / d * 6
          bullet.y += dy / d * 6    
          state.soundEnemyArmorBounce.play()
          //bullet.body.velocity.x = 
          return false
        }
      }
      if(dmg >= self.health && !self.killed) {
        self.killed = true
        enemyGetHit(dmg, x, y)
        let d = new Door(state, 55*GS, 58*GS + 2, 0, 0, 0, 'bossdoor')
        let weap = g.makeRandomWeapon()
        let mod = weap.modifier ? (weap.modifier + " ") : ""
        state.award(weap.inventoryImage, mod + weap.name, 0x616377, 0, 0)
        
        for(i = 0; i < g.weapons.length; i++) {
          if(g.weapons[i] == null) {
            g.weapons[i] = weap
            state.updateWeapons()
            break
          }
        }
        
        d.activate = function() {
          let oldFloor = g.dungeon.floor
          g.dungeon = Dungeon(oldFloor + 1)
          state.changeRooms(g.dungeon.spawn.x, g.dungeon.spawn.y, 'right')
        }
        state.gameLayer.add(d)        
      } else {
        enemyGetHit(dmg, x/10, y/10)
        
      }
    }    
    
    self.render = function() {
      //g.debug.geom(new Phaser.Rectangle(self.targetPt.x, self.targetPt.y, 4, 4), 'rgba(255,0,0,1)')
      /*if(self.debugLine1 && self.debugLine2) {
        g.debug.geom(self.debugLine1, 'rgba(0,255,0,1)')
        g.debug.geom(self.debugLine2, 'rgba(0,255,0,1)')
      }
      */
    }
    
    return self
  }
  
  return ctor
})()
