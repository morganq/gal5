const StickerSelectionMenu = (function() {
  const WIDTH = 50
  const INNER_WIDTH = 40
  const VPAD = 4
  const HEIGHT = GAME_HEIGHT
  function ctor(state, parentMenu) {
    let menu = MenuBase(state, 0xccd6dc, 0, 0, WIDTH, HEIGHT)
    menu.openDirection = 'right'
    
    menu.stickerPanel = GridSelectionPanel(state, menu, WIDTH - 2, HEIGHT)
    menu.addChild(menu.stickerPanel)
    
    let grid = []
    g.stickers.forEach(function(s, i) {
      let spr = new Phaser.Sprite(g, 0, 0, s.sprite)
      grid.push([{'element':spr, 'value':{index:i, spec:s}}])
    })
    menu.stickerPanel.setGrid(grid)    
    
    menu.stickerStat = StatInfo(state, false, undefined, "unnamed sticker")
    menu.addChild(menu.stickerStat)
    menu.stickerStat.x = WIDTH + 2
    menu.stickerStat.y = 0
    menu.stickerStat.visible = false

    menu.selectedIndex = 0

    menu.isOpen = false  

    menu.updateStats = function() {
      let stats = {}
      let name = null
      if(g.stickers.length >= menu.selectedIndex) {
        stats = g.stickers[menu.selectedIndex].stats
        name = g.stickers[menu.selectedIndex].name
      }
      menu.stickerStat.updateStats(stats)
      if(name) {
        let rarity = g.stickers[menu.selectedIndex].rarity
        if(rarity == 'common') {
          menu.stickerStat.updateTitle(name, 0x616377)
        } else if (rarity == 'uncommon') {
          menu.stickerStat.updateTitle(name, 0x89269e)
        } else {
          menu.stickerStat.updateTitle(name, 0xdb8600)
        }
      }
      menu.stickerStat.y = menu.selector.y + menu.selector.height / 2 - 5
      if(menu.stickerStat.bottom > HEIGHT) {
        menu.stickerStat.y = HEIGHT - menu.stickerStat.height
      }
    }
    
    menu.openUpdate = function() {
      if(g.selectButton.downDuration(1)) {
        menu.parentMenu.pickSticker(menu.stickerPanel.getSelectedValue().index)
        menu.close()
      }
    }

    return menu
  }

  return ctor
})()

const PauseMenu = (function() {
  const WIDTH = GAME_WIDTH
  const HEIGHT = GAME_HEIGHT
  const STICKER_MOVE_SPEED = 100
  const STICKER_PRECISE_MOVE_SPEED = 10
  function ctor(state) {
    let menu = new MenuBase(state, 0xffffff, 0, 0, WIDTH, HEIGHT)

    menu.loadCurrentWeapon = function() {
      if(menu.curWeapBackground) { menu.removeChild(menu.curWeapBackground) }
      if(menu.curWeapSprite) { menu.removeChild(menu.curWeapSprite) }
      if(menu.curWeapAlpha) { menu.removeChild(menu.curWeapAlpha) }
      menu.curWeapBackground = new Phaser.Sprite(g, 0, 0, g.curWeapon.bigImageBackground)
      menu.addChildAt(menu.curWeapBackground, 0)
      menu.curWeapBackground.x = 0
      menu.curWeapBackground.y = 0
      menu.curWeap = new Phaser.BitmapData(g, 'something', WIDTH, HEIGHT)
      menu.curWeap.copy(g.curWeapon.currentImage)
      menu.curWeap.update()
      menu.curWeapSprite = new Phaser.Sprite(g, 0, 0, g.curWeapon.currentImage)
      menu.addChildAt(menu.curWeapSprite, 1)
      menu.curWeapSprite.x = 0
      menu.curWeapSprite.y = 0    
      menu.curWeapAlpha = new Phaser.Sprite(g, 0, 0, g.curWeapon.bigImageAlpha)
      menu.addChildAt(menu.curWeapAlpha, 2)
      menu.updateStats()  
    }

    menu.selectingSticker = false
    menu.selectedStickerId = 0
    menu.selectedSticker = null
    menu.selectedStickerObj = null
    menu.stickerCursorPos = new Phaser.Point(0,0)

    menu.statText = StatInfo(state, false, 5)
    menu.addChild(menu.statText)

    menu.deltaStatTextContainer = StatInfo(state, true, undefined, "Change in stats")
    menu.addChild(menu.deltaStatTextContainer)
    
    menu.weaponName = new Phaser.BitmapText(g, 0, 0, "visitor", "", 10)
    menu.weaponName.anchor.set(1, 1)
    menu.addChild(menu.weaponName)
    menu.weaponName.x = GAME_WIDTH - 8
    menu.weaponName.y = GAME_HEIGHT - 4
    menu.weaponName.tint = 0x020628
    
    menu.stats = {}
    
    menu.help1 = new Phaser.BitmapText(g, 120, 6, "visitor", "[q] unpause", 10)
    menu.help1.tint = 0xbb499a 
    menu.addChild(menu.help1)
    
    menu.help2 = new Phaser.BitmapText(g, 120, 16, "visitor", "", 10)
    menu.help2.tint = 0xbb499a 
    menu.addChild(menu.help2)
    
    menu.help3 = new Phaser.BitmapText(g, 120, 26, "visitor", "", 10)
    menu.help3.tint = 0xbb499a 
    menu.addChild(menu.help3) 
    
    menu.help4 = new Phaser.BitmapText(g, 120, 36, "visitor", "", 10)
    menu.help4.tint = 0xbb499a 
    menu.addChild(menu.help4)      
    menu.selectedStickerIndex = 0
    
    menu.updateName = function() {
      let name = g.curWeapon.modifier ? (g.curWeapon.modifier + " " + g.curWeapon.name) : g.curWeapon.name
      menu.weaponName.text = name
    }
    menu.updateName()
    
    menu.updateStats = function() {
      menu.stats = menu.getStats(g.curWeapon.stickerPixels)
      g.nextStats = menu.stats
      menu.statText.updateStats(menu.stats)
      menu.statText.x = GAME_WIDTH - menu.statText.width - 2
      menu.statText.y = 2
    }    

    menu.pickSticker = function(index) {
      menu.selectedStickerIndex = index
      menu.selectedSticker = g.stickers[index]
      menu.selectedStickerObj = new Phaser.Sprite(g, menu.stickerCursorPos.x, menu.stickerCursorPos.y, menu.selectedSticker.sprite, 0)
      menu.addChild(menu.selectedStickerObj)
      menu.selectedStickerObj.x = WIDTH / 2 - menu.selectedStickerObj.width / 2
      menu.selectedStickerObj.y = HEIGHT / 2 - menu.selectedStickerObj.height / 2
      menu.stickerBD = new Phaser.BitmapData(g, 'temp', menu.selectedStickerObj.width, menu.selectedStickerObj.height)
      menu.stickerBD.draw(menu.selectedStickerObj, 0, 0, null, null, null, true)
      menu.stickerBD.update()
    }

    menu.cancelSticker = function() {
      if(menu.selectedStickerObj) {
        menu.selectedStickerObj.destroy()
        menu.selectedStickerObj = null
      }
      menu.selectedSticker = null
    }

    menu.pasteSticker = function() {
      //g.curWeapon.stickerPixels = menu.pasteStickerHelper(true)
      g.curWeapon.stickerPixels = g.curWeapon.pasteSticker(
        menu.selectedSticker, menu.stickerBD, 
        menu.selectedStickerObj.x,menu.selectedStickerObj.y, true)
      g.curWeapon.stickersPasted.push({
        sticker:menu.selectedSticker.name,
        x:Math.floor(menu.selectedStickerObj.x),
        y:Math.floor(menu.selectedStickerObj.y)
      })      
      menu.cancelSticker()
      g.stickers.splice(menu.selectedStickerIndex, 1)
      menu.updateStats()
    }

    menu.getStats = function(pixels) {
      let statPixels = {}
      let statTotals = {}
      let stickerPixels = {}
      let stickerTotals = {}
      for(let y = 0; y < pixels.length; y++) {
        for(let x = 0; x < pixels[y].length; x++) {
          let s = pixels[y][x]
          if(s && s.stats) {
            if(s.id in stickerPixels) {
              //statPixels[s.stat] += s.value
              for(key in s.stats) {
                stickerPixels[s.id][key] += s.stats[key]
              }
            } else {
              //statPixels[s.stat] = s.value
              stickerPixels[s.id] = Object.assign({}, s.stats)
              stickerTotals[s.id] = s.totalPixels
            }
          }
        }
      }
      for(sid in stickerPixels) {
        for(key in stickerPixels[sid]) {
          stickerPixels[sid][key] /= stickerTotals[sid]
          if(key in statPixels) {
            statPixels[key] = g.math.clamp(statPixels[key] + stickerPixels[sid][key], -90, 1000)
          } else {
            statPixels[key] = g.math.clamp(stickerPixels[sid][key], -90, 1000)
          }
        }
      }
      return statPixels
    }

    menu.getStatsWithSticker = function() {
      pixels = g.curWeapon.pasteSticker(
        menu.selectedSticker, menu.stickerBD, 
        menu.selectedStickerObj.x,menu.selectedStickerObj.y, false)
      return menu.getStats(pixels)
    }

    menu.updateHelp = function() {
      if(menu.selectedStickerObj) {
        menu.help2.text = "[z] back"
        menu.help3.text = "[c] place sticker"
        menu.help4.text = "hold [x] precise move"
      } else if (menu.selectingSticker) {
        menu.help2.text = "[z] back"
        menu.help3.text = "[c] select sticker"
        menu.help4.text = ""        
      } else {
        if(g.stickers.length > 0) {
          menu.help2.text = "[c] open sticker menu"
        } else {
          menu.help2.text = ""
        }
        menu.help3.text = ""
        menu.help4.text = ""
      }
    }

    let oldUpdate = menu.update
    menu.update = function() {
      if(g.menuButton.downDuration(1)) { 
        menu.close()
      }
      oldUpdate()
    }

    menu.openUpdate = function() {
      menu.updateHelp()

      if(menu.selectedStickerObj) {
        if(!menu.selectedStickerObj.zx) { 
          menu.selectedStickerObj.zx = menu.selectedStickerObj.x
          menu.selectedStickerObj.zy = menu.selectedStickerObj.y
        }
        menu.selectedStickerObj.alpha = Math.sin(g.time.now / 100) * 0.2 + 0.5
        let speed = g.extraButton.isDown ? STICKER_PRECISE_MOVE_SPEED : STICKER_MOVE_SPEED
        if(g.cursors.right.isDown) { menu.selectedStickerObj.zx += g.time.elapsed / 1000 * speed }
        if(g.cursors.left.isDown) { menu.selectedStickerObj.zx -= g.time.elapsed / 1000 * speed }
        if(g.cursors.up.isDown) { menu.selectedStickerObj.zy -= g.time.elapsed / 1000 * speed }
        if(g.cursors.down.isDown) { menu.selectedStickerObj.zy += g.time.elapsed / 1000 * speed }
        
        menu.selectedStickerObj.x = Math.floor(menu.selectedStickerObj.zx)
        menu.selectedStickerObj.y = Math.floor(menu.selectedStickerObj.zy)

        if(g.backButton.downDuration(1)) {
          menu.cancelSticker()
          return
        }

        if(g.selectButton.downDuration(1)) {
          menu.pasteSticker()
        } else {
          menu.deltaStatTextContainer.visible = true
          let stats = menu.getStatsWithSticker()
          menu.deltaStatTextContainer.updateStatsDelta(stats, menu.stats)
          if(menu.selectedStickerObj.x < GAME_WIDTH / 2) {
            menu.deltaStatTextContainer.x = menu.selectedStickerObj.right + 3
          } else {
            menu.deltaStatTextContainer.x = menu.selectedStickerObj.left - menu.deltaStatTextContainer.width - 3
          }
          menu.deltaStatTextContainer.y = menu.selectedStickerObj.y
        }
      } else {
        menu.deltaStatTextContainer.visible = false

        if(g.selectButton.downDuration(1) && g.stickers.length > 0) {
          g.selectButton.reset()
          menu.ssm = StickerSelectionMenu(state, menu)
          menu.openChildMenu(menu.ssm)
        }
      }
    }
    
    menu.onClose = function() {
      if(menu.ssm) { menu.ssm.close(); menu.ssm.destroy() }
    }
    
    menu.loadCurrentWeapon()

    return menu
  }

  return ctor
})()
