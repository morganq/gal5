const Healer = (function() {
  const ACTIVE_RANGE = 18
  const TICK_TIME = 100
  
  function ctor(state, x, y, tx, ty, direction) {
    let self = new Phaser.Sprite(g, x, y, "coffeemaker")
    self.animations.add("refill", [4,5,6,0], 2)
    self.active = false
    
    self.ready = true
    self.quantity = 4
    
    self.nextTick = g.now + TICK_TIME
    
    self.update = function() {
      if(self.ready) {
        self.frame = Math.floor(4 - self.quantity)
      }
      if(g.now > self.nextTick && self.ready) {
        let dx = state.player.x - (self.x + self.width/2)
        let dy = state.player.y - (self.y + self.height/2)
        let d = dx*dx+dy*dy
        if(d <= ACTIVE_RANGE ** 2 && state.player.health < state.player.maxHealth) {
          self.quantity -= 0.2
          //state.player.addHealth(5)
          for(let i = 0; i < 1; i++) {
            let pickup = Pickup(state,
              self.x + self.width/2 + g.rnd.between(-4,4),
              self.y + self.height/2 + g.rnd.between(-4,4),
              g.rnd.frac()*6.2818, 'health-pickup', 0, function(p){p.addHealth(2)})
            state.gameLayer.add(pickup)
          }
        }
        self.nextTick = g.now + TICK_TIME
      }
      if(self.quantity <= 0) {
        self.ready = false
      }
      if(!self.ready) {
        self.play("refill")
        if(self.frame == 0) {
          self.ready = true
          self.quantity = 4
        }
      }
    }
    
    return self
  }
  
  return ctor
})()
