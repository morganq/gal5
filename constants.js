GAME_WIDTH=288
GAME_HEIGHT=176
GS = 8

BASE_GRAVITY = 1.53/2
BASE_MULTIPLIER_TIMER = 5000

T_PLATFORM = 6
T_SPIKES_INDICES = [7,8]

T_WALKERHINT_TURN_RIGHT = 0
T_WALKERHINT_TURN_LEFT = 1
T_WALKERHINT_FLIP = 2

TILESET_INDICES = {}

T_PATH_HINT = 9
T_PATH_BLOCKED = 1
T_PATH_EMPTY = -1

T_DOOR_DOWN = 0
T_DOOR_LEFT = 1
T_DOOR_UP = 2
T_DOOR_RIGHT = 3
T_TREASURE = 4
T_STORE=5
T_REGISTRY=6

T_WINDOW_TILES = [
  227
]

COLOR_FOR_RARITY = {
  'common':0x616377,
  'uncommon':0x89269e,
  'rare':0xdb8600
}

function is_colliding_tile_index_plus_stairs(index) {
  let i = index - TILESET_INDICES['main']
  if((i > -1 && i < 6) || i > 8) {
    return true
  }
  return false
}

function is_colliding_tile_index_plus_platform(index) {
  let i = index - TILESET_INDICES['main']
  if((i > -1 && i < 2) || i > 14 || i == 6) {
    return true
  }
  return false
}

function is_colliding_tile_index(index) {
  let i = index - TILESET_INDICES['main']
  if((i > -1 && i < 2) || i > 14) {
    return true
  }
  return false
}

function is_colliding_tile_index_all(index) {
  let i = index - TILESET_INDICES['main']
  if(i > -1) {
    return true
  }
  return false
}

STATS = [
  'damage',
  'speed',
  'jump',
  'roll',
  'aim',
  'float', // if you're holding jump when you're falling, apply some antigrav
  'bonus',
  'critchance',
  'critdamage', // 200% + bonus
  'health',
  'shield', 
  'homing',
  'lightning', // % chance to do chain lightning effect, over 100% = larger radius?
  'fire',
  'acid', // after some seconds, deals x% of original bullet damage (creates acid bullet?)
  'vampire',
  'bossdamage', // Extra % damage to bosses
  'piercing', // Bullet continues to live for a certain distance after hitting an enemy
  'bullets', // Extra bullets for shotguns and such
  'reload',
  'multipliertime', // extra time for multiplier bonus
  'backstab',
  'melee',
  'sniper',
  'hacking', // something special to mechanical (cyborg)
  'heavy', // knockback
]

STAT_DESCRIPTIONS = {
  'damage': 'Your weapon stings a bit more',
  'speed': 'Run like the wind',
  'jump': 'Jump higher',
  'roll': 'Chance for a critical shot just after dodging',
  'aim': 'Your bullets seek out enemies',
  'float': 'Hang in midair if you hold jump',
  'bonus': 'More credits and health from dead enemies',
  'critchance': 'Chance for critical hits',
  'critdamage': 'More powerful criticals',
  'health': 'Get healthier',
  'shield': 'Gain an energy shield',
  'homing': 'NOT IMPLEMENTED',
  'lightning': 'A chance to zap nearby enemies',
  'fire': 'A chance to start a fire',
  'acid': 'NOT IMPLEMENTED',
  'vampire': 'Gain health when you damage enemies',
  'bossdamage': 'Deal bonus damage to bosses',
  'piercing': 'Bullets pierce through enemies',
  'bullets': 'Weapon upgrade',
  'reload': 'Fire more rapidly',
  'multipliertime': 'Combo lasts longer',
  'backstab': 'Backstab for bonus damage',
  'melee': 'Attack in close range for bonus damage',
  'sniper': 'Attack at long range for bonus damage'
}

LIGHTNING_RANGE = 150
MELEE_RANGE = 32
SNIPE_RANGE = 100

ALLIANCES = {
  'mosaic': {
    alliance:'mosaic',
    name:'apex (mosaic)',
    slogan:'the banner of liberation',
    bonus:'all-access pass', 
    bonusAchievement:'beat the game',
    gun:'Revolver',
    gunAchievement:'unlocked',
    dodge:'roll',
    dodgeAchievement:'unlocked',
    augment:'none',
    augmentAchievement:'beat the game',
    bonusText:'The all-access pass lets you slip into this convention unnoticed, so you can carry out the mission', 
    dialog:'' +
'"you should not be seen talking to me... but\n' +
'here\'s what i have for a fellow traveller."',    
  },
  'homeworld': {
    alliance:'homeworld',
    name:'apex (homeworld)',
    slogan: 'one home - one civilization',
    bonus:'delay cops',
    bonusAchievement:'beat the game',
    gun:'Zapper',
    gunAchievement:'beat the game',
    dodge:'roll',
    dodgeAchievement:'beat the game',
    augment:'+100 float',
    augmentAchievement:'beat the game',    
    dialog:'' +
'"idle chatter is against protocol. however,\n' +
'perhaps you can be of some use to apex."',
  },
  'yz': {
    alliance:'yz',
    name:'y/z',
    slogan: 'symbiosis is our strength',
    bonus:'ad-hoc clone',
    bonusAchievement:'beat the game',
    gun:'revolver',
    gunAchievement:'beat the game',
    dodge:'spin',
    dodgeAchievement:'beat the game',
    augment:'none',
    augmentAchievement:'beat the game',
    dialog:'' +
'"here is an example of some dialog that w\n' +
'ill go on multiple lines."',    
  },
  'seers': {
    alliance:'seers',
    name:'seers',
    slogan: 'order and vision',
    bonus:'unknown',
    bonusAchievement:'destroy 45 y/z',
    gun:'revolver',
    gunAchievement:'clear floor', // kill every enemy on a floor
    dodge:'blink',
    dodgeAchievement:'beat the game',
    augment:'none',
    augmentAchievement:'beat the game',
    dialog:'' +
'"there is such idiocy... weakness... the galaxy\n' +
'cries out for discipline. you will assist us."',    
  },
  'roth': {
    alliance:'roth',
    name:'roth luminaries',
    slogan: 'industry, prosperity, freedom',
    bonus:'sticker scheme',
    bonusAchievement:'save 100 credits',
    bonusText:'Receive a random sticker, absolutely free*!\n\n*Debt will be collected at a future date.', 
    gun:'Rocket Launcher',
    gunAchievement:'spend 1000 credits',
    dodge:'mark',
    dodgeAchievement:'save 1000 credits',
    augment:'+67 upgrade',
    augmentAchievement:'spend 10000',
    dialog:'' +
'you and i, friend, we can be rich as kings!\n' +
'join me in this new venture!"',
  },
  'fragment': {
    alliance:'fragment',
    name:'fragment',
    slogan: '...',
    bonus:'unknown',
    bonusAchievement:'beat the game',
    gun:'revolver',
    gunAchievement:'beat the game',
    dodge:'reflect',
    dodgeAchievement:'beat the game',
    augment:'none',
    augmentAchievement:'beat the game',    
    dialog:'' +
'fragment does not speak. however, you find\n' +
'yourself suddenly aware of its interests.',    
  }
}
