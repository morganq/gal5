const Bullet = (function() {
  FADE_TIME_MS = 100
  LASER_DISTANCE = 20
  PIERCING_TIME = 300

  function ctor(state, properties, x, y, xv, yv) {
    let bullet = new Phaser.Sprite(g, x, y, properties.graphic)
    bullet.frame = g.rnd.pick(properties.graphicFrames)
    bullet.properties = properties
    
    bullet.customUpdate = null
    
    if(bullet.properties.halfBullet) { bullet.scale.set(0.4, 1)}
    if(bullet.properties.fires > 0) {
      let fire = new Phaser.Sprite(g, x, y, "flame")
      fire.animations.add("burn",[0,1,2,3], 12, true)
      fire.play("burn")
      fire.rotation = -Math.PI/2
      bullet.addChild(fire)
      fire.x = 4
      fire.y = 4
    }
    if(bullet.properties.lightning > 0) {
      let spark = new Phaser.Sprite(g, x, y, "spark")
      spark.animations.add("spark",[0,1,2], 12, true)
      spark.play("spark")
      bullet.addChild(spark)
      spark.x = 3
      spark.y = -3
    }    
    g.physics.enable(bullet)
    g.slopes.enable(bullet)
    bullet.anchor.set(0.5,0.5)
    bullet.body.velocity.x = xv
    bullet.body.velocity.y = yv
    bullet.rotation = Math.atan2(yv, xv)
    bullet.speed = bullet.body.velocity.getMagnitude()
    bullet.baseSpeed = bullet.speed

    bullet.startPosition = new Phaser.Point(x,y)

    bullet.fadeStart = g.now + bullet.properties.lifetime
    
    // Special
    bullet.nextHomingTargetTime = 0
    bullet.homingTarget = null
    
    bullet.enemiesHit = []

    bullet.setHitbox = function() {
      /*
      if(Math.abs(bullet.body.velocity.x) >= Math.abs(bullet.body.velocity.y)) {
        bullet.body.setSize(bullet.width, 4, 0, bullet.height/2 - 2)      
      } else {
        bullet.body.setSize(4, bullet.height, bullet.width/2 - 2, 0)
      }
      */
      //bullet.body.setSize(bullet.width, 4, 0, bullet.height/2 - 2)
      //bullet.body.setSize(6,6,bullet.width/2-3,bullet.height/2-3)
      //console.log(bullet.width/2-3, bullet.height/2-3)
      let bx = Math.cos(bullet.rotation) * bullet.width * 0.33
      let by = Math.sin(bullet.rotation) * bullet.width * 0.33
      if(bullet.properties.closecombat) {
        bx = 0; by = 0;
      }
      let rad = bullet.properties.radius
      let xo = bullet.width/2 - rad + bx
      let yo = bullet.height/2 - rad + by
      bullet.body.setCircle(rad, xo, yo)
      //bullet.body.setCircle(4, 0, 0)
      //bullet.body.setSize(4, 4, bullet.width/2-2, bullet.height/2-2)
      g.slopes.enable(bullet)
    }

    bullet.setHitbox()

    bullet.justHit = function() {
      let piercing = 0
      if(bullet.properties.piercing) { piercing = bullet.properties.piercing }
      bullet.fadeStart = Math.min(
        bullet.fadeStart,
        g.now + PIERCING_TIME * (piercing / 100)
      )
      
      if(bullet.properties.explosionRadius) {
        let er= bullet.properties.explosionRadius
        // make explosion
        state.enemyLayer.forEach(function(e) {
          if(!e.getHit) { return }
          if(bullet.enemiesHit.includes(e)) { return }
          let dx = e.x - bullet.x
          let dy = e.y - bullet.y
          if(dx*dx+dy*dy < er*er) {
            let d = bullet.properties.damage * (er - Math.sqrt(dx*dx+dy*dy)) / er
            e.getHit(
              d,
              0,
              0)
          }
        })

      }
    }

    bullet.update = function() {
      if(g.now < bullet.fadeStart) {
        bullet.setHitbox()
        if(bullet.properties.gravity) {
          xv = bullet.body.velocity.x
          yv = bullet.body.velocity.y + g.elapsed * 0.1
          d = Math.sqrt(xv*xv+yv*yv)
          let a = Math.atan2(yv/d, xv/d)
          bullet.body.velocity.x = xv
          bullet.body.velocity.y = yv
          bullet.rotation = a
        }
        
        if(bullet.properties.homing && bullet.properties.homing != 0) {
          if(g.now >= bullet.nextHomingTargetTime) {
            bullet.nextHomingTargetTime = g.now + 0.01
            let distSqMax = 999999
            let curTarget = null
            state.enemyLayer.forEach(function(e) {
              if(!e.exists || !e.hittable) { return }
              if(state.tilemapLayer.getRayCastTiles(
                new Phaser.Line(bullet.x, bullet.y, e.x, e.y),
                2, true).length == 0) {
                let dx = e.x - bullet.x
                let dy = e.y - bullet.y
                let distSq = dx*dx+dy*dy
                if(distSq < 200 * 200 && distSq < distSqMax) {
                  bullet.homingTarget = e
                  distSqMax = distSq
                }
              }
            })
          }
          if(bullet.homingTarget) {
            let toTargetAngle = g.math.angleBetweenPoints(bullet.position, bullet.homingTarget.position)
            let angBet = g.math.wrapAngle(toTargetAngle - bullet.rotation, true)
            let newAngle = 0
            let dx = bullet.homingTarget.x - bullet.x
            let dy = bullet.homingTarget.y - bullet.y            
            let dist = Math.sqrt(dx*dx+dy*dy)
            let rotAmt = bullet.properties.homing / 100// * g.math.clamp(2 - dist / 100, 0.25, 2)
            let rotAmount = Math.abs(-0.01 * g.elapsed * rotAmt)
            if(angBet < -rotAmount) {
              newAngle = bullet.rotation - rotAmount * Math.sign(bullet.properties.homing)
            } else if(angBet > rotAmount) {
              newAngle = bullet.rotation + rotAmount * Math.sign(bullet.properties.homing)
            } else {
              newAngle = toTargetAngle
            }
            bullet.speed = bullet.baseSpeed * g.math.clamp(dist / 80, 1 / g.pct(bullet.properties.homing*2), 1)
            bullet.body.velocity.x = Math.cos(newAngle) * bullet.speed
            bullet.body.velocity.y = Math.sin(newAngle) * bullet.speed
            bullet.rotation = newAngle
          }
        }
        
        if(bullet.customUpdate) {
          bullet.customUpdate()
        }
        
        bullet.detectCollisions(0)
        bullet.detectCollisions(0.25)
        bullet.detectCollisions(0.5)
        bullet.detectCollisions(0.75)
      } else {
        bullet.body.velocity.x = 0
        bullet.body.velocity.y = 0
        bullet.alpha = 1 - ((g.now - bullet.fadeStart) / FADE_TIME_MS)
        if(bullet.alpha <= 0) { bullet.destroy() }
      }
    }
    
    bullet.detectCollisions = function(framesOffset) {
      
      
      preX = bullet.body.x
      preY = bullet.body.y
      bullet.body.x += (bullet.body.velocity.x * g.elapsed / 1000) * framesOffset
      bullet.body.y += (bullet.body.velocity.y * g.elapsed / 1000) * framesOffset
      
      let hitTileAlready = false
      let hitwall = function(bullet, tile) {
        if(!hitTileAlready && is_colliding_tile_index_plus_stairs(tile.index)) {
          bullet.justHit()
          hitTileAlready = true
          bullet.spawnFires()
          bullet.fadeStart = g.now
          if(!bullet.properties.closecombat) {
            bullet.makeWallHitEffect(tile)
            bullet.body.velocity.x = 0
            bullet.body.velocity.y = 0
          }
          state.lastBulletHitRect = new Phaser.Rectangle(bullet.body.left, bullet.body.top, bullet.body.width, bullet.body.height)
        }
      }
      if(bullet.badStartingPosition) {
        hitwall(bullet, bullet.badStartingPositionTile)
      }
      g.physics.arcade.overlap(bullet, state.tilemapLayer, hitwall)

      g.physics.arcade.overlap(bullet, state.enemyLayer, function(bullet, enemy) {
        if(bullet.enemiesHit.includes(enemy)) { return }
        if(!enemy.hittable) { return }
        bullet.enemiesHit.push(enemy)
        let d = Math.sqrt(Math.pow(bullet.body.velocity.x,2) + Math.pow(bullet.body.velocity.y,2))
        let damage = bullet.properties.damage
        
        let isBackstab = bullet.isBackstab(enemy)
        if(isBackstab) { damage *= g.pct(bullet.properties.backstab) }
        
        let isMelee = bullet.isMelee(enemy)
        if(isMelee) { damage *= g.pct(bullet.properties.melee) }
        
        let isSnipe = bullet.isSnipe(enemy)
        if(isSnipe) { damage *= g.pct(bullet.properties.sniper)}
        
        let getHitResult = enemy.getHit(
          damage,
          bullet.body.velocity.x/d * bullet.properties.weight,
          bullet.body.velocity.y/d * bullet.properties.weight,
          bullet)
        if(getHitResult !== false) {
          bullet.justHit()
          if(bullet.properties.vampire && bullet.properties.vampire > 0) {
            state.player.vampireLife += bullet.properties.damage * bullet.properties.vampire / 100
          }
          bullet.spawnFires(enemy)
          bullet.doSpark(enemy)
          bullet.makeEnemyHitEffect(enemy)
          if(isBackstab) { bullet.doBackstabGfx(enemy) }
          if(isMelee) { bullet.doMeleeGfx(enemy) }
          if(isSnipe) { bullet.doSnipeGfx(enemy) }
        }
      })      
      
      bullet.body.x = preX
      bullet.body.y = preY      
    }
    
    bullet.makeEnemyHitEffect = function(enemy) {
      let fx = new Phaser.Sprite(g, 0, 0, "effect-hit")
      fx.animations.add("hit",[1,2,3], 30, false)
      fx.anchor.set(0.5, 0.5)
      fx.play("hit", null, false, true)
      state.effectLayer.addChild(fx)
      fx.x = bullet.x + bullet.body.velocity.x / 20
      fx.y = bullet.y + bullet.body.velocity.y / 20
      fx.tint = bullet.properties.color
    }
    
    bullet.makeWallHitEffect = function(tile) {
      let x = 0
      let y = 0
      let angle = 0
      if(bullet.body.velocity.x > Math.abs(bullet.body.velocity.y)) {
        x = tile.worldX
        y = bullet.y
        angle = 0
      } else if (bullet.body.velocity.x < -Math.abs(bullet.body.velocity.y)) {
        x = tile.worldX + GS
        y = bullet.y  
        angle = 180
      } else if (bullet.body.velocity.y > Math.abs(bullet.body.velocity.x)) {
        x = bullet.x
        y = tile.worldY
        angle = 90
      } else if (bullet.body.velocity.y < -Math.abs(bullet.body.velocity.x)) {
        x = bullet.x
        y = tile.worldY + GS
        angle = -90
      }
      let fx = new Phaser.Sprite(g,0, 0, "effect-hitwall")
      fx.animations.add("hit",[1,2,3], 30, false)
      fx.anchor.set(1, 0.5)
      fx.play("hit", null, false, true)
      state.effectLayer.addChild(fx)
      fx.x = x
      fx.y = y
      fx.angle = angle
      fx.tint = bullet.properties.color
    }
    
    bullet.isSnipe = function(enemy, damage) {
      if(bullet.properties.sniper <= 0) { return false}
      let dx = enemy.x - state.player.x
      let dy = enemy.y - state.player.y
      if(dx * dx + dy * dy > SNIPE_RANGE ** 2) {
        return true
      }
      return false
    }    
    
    bullet.isMelee = function(enemy, damage) {
      if(bullet.properties.melee <= 0) { return false}
      let dx = enemy.x - state.player.x
      let dy = enemy.y - state.player.y
      if(dx * dx + dy * dy < MELEE_RANGE ** 2) {
        return true
      }
      return false
    }
    
    bullet.doGenericGfx = function(enemy, key) {
      let gfx = new Phaser.Sprite(g, enemy.x - 5, enemy.top - 10, key)
      enemy.addEffect(gfx)
      gfx.anchor = new Phaser.Point(0.5, 0.5)
      gfx.x = 0
      gfx.y = enemy.top - enemy.y - 1
      gfx.startTime = g.now
      
      gfx.update = function() {
        if(g.now - gfx.startTime > 400) {     
          gfx.alpha -= g.elapsed / 500
        }
        if(gfx.alpha <= 0) {
          gfx.destroy()
          gfx.exists = false
        }
      }       
      return gfx  
    }    
    
    bullet.doSnipeGfx = function(enemy) {
      bullet.doGenericGfx(enemy, 'effect-snipe')
    }
    
    bullet.doMeleeGfx = function(enemy) {
      bullet.doGenericGfx(enemy, 'effect-melee')
    }
    
    bullet.isBackstab = function(enemy, damage) {
      if(bullet.properties.backstab <= 0) { return false }
      // make this work for vertical shots too?
      if(Math.abs(enemy.body.velocity.x) < 1 || Math.abs(bullet.body.velocity.x) < 1){ return false }
      if(Math.sign(enemy.body.velocity.x) == Math.sign(bullet.body.velocity.x)) {
        return true
      }
      return false
    }
    
    bullet.doBackstabGfx = function(enemy) {
      bullet.doGenericGfx(enemy, 'effect-backstab')
    }
    
    bullet.spawnFires = function(attached) {
      if(bullet.properties.fires && bullet.properties.fires > 0) {
        let howManyFires = bullet.properties.fires
        for(i = 0; i < howManyFires; i++) {
          let flame
          if(attached) {
            flame = Flame(state,attached.x + g.rnd.between(-4, 4), attached.y + g.rnd.between(-4, 4), attached)
          } else {
            flame = Flame(state,bullet.x + g.rnd.between(-4, 4), bullet.y + g.rnd.between(-4, 4))
          }
          state.effectLayer.add(flame)
        }
      }      
    }
    
    bullet.doSpark = function(enemy) {
      for(var i = 0; i < bullet.properties.lightning; i++) {
        let near = []
        state.enemyLayer.forEachExists(function(e) {
          if(e && e.getHit && e != enemy) {
            let dx = e.x - bullet.x
            let dy = e.y - bullet.y
            if(dx*dx+dy*dy < LIGHTNING_RANGE ** 2) {
              near.push(e)
            }
          }
        })
        let e = g.rnd.pick(near)
        if(e) {
          let bx = enemy.x - 100
          let by = enemy.y - 100
          let lastX = 100
          let lastY = 100
          let bd = new Phaser.BitmapData(g, 200, 200)
          let spark = new Phaser.Sprite(g, bx, by, bd)
          let spots = 7
          for(let j = 0; j < spots; j++) {
            let t = (j+1) / (spots+1)
            let dx = e.x - enemy.x
            let dy = e.y - enemy.y
            
            bd.smoothed = false
            
            spark.smoothed = false
            let tx = 100 + dx * t + g.rnd.between(-5,5)
            let ty = 100 + dy * t + g.rnd.between(-5,5)
            bd.line(lastX, lastY,
              tx,
              ty,
              '#005ede', 1)
            if(g.rnd.between(1,6) == 3) {
              bd.line(lastX, lastY,
                100 + dx * t + g.rnd.between(-10,10),
                100 + dy * t + g.rnd.between(-10,10),
                '#005ede', 1)              
            }
            lastX = tx
            lastY = ty
          }
          startTime = g.now
          spark.update = function() {
            spark.x = bx + g.rnd.between(-2,2)
            spark.y = by + g.rnd.between(-2,2)
            spark.alpha = (startTime + 200 - g.now) / 200
            if(g.now > startTime + 200) {
              spark.destroy()
            }
          }
          state.effectLayer.add(spark)
          e.getHit(bullet.properties.damage, 0, 0)
        }
      }
    }

    bullet.render = function() {
      /*
      let vx = bullet.x - bullet.startPosition.x
      let vy = bullet.y - bullet.startPosition.y
      let d = Math.sqrt(vx * vx + vy * vy)
      if(d == 0) { return }
      let mul = Math.min(LASER_DISTANCE, d)
      g.debug.geom(new Phaser.Line(bullet.x, bullet.y, bullet.x - vx / d * mul, bullet.y - vy / d * mul), 'rgb(255,0,0)')
      */
      //g.debug.body(bullet)
      
    }

    //bullet.detectCollisions(0)
    if(!bullet.properties.closecombat) {
      let rayCastTiles = state.tilemapLayer.getRayCastTiles(
        new Phaser.Line(bullet.x, bullet.y, state.player.x, state.player.y),
        2, true)
      let anybad = false
      rayCastTiles.forEach(function(t) {
        if(is_colliding_tile_index_plus_stairs(t.index)) {
          anybad = true
        }
      })
      if(anybad) {
        console.log("yep")
        bullet.badStartingPosition = true
        bullet.badStartingPositionTile = rayCastTiles[0]
        bullet.visible = false
      }
    }

    return bullet
  }

  return ctor
})()
