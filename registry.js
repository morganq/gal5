const RegistryMenu = (function() {
  WIDTH = GAME_WIDTH-40
  HEIGHT = 140
  X = (GAME_WIDTH - WIDTH) / 2
  Y = (GAME_HEIGHT - HEIGHT) / 2
  
  function WeaponDisplay(i) {
    let w = g.weapons[i]
    let grp = new Phaser.Group(g)
    let spr = new Phaser.Sprite(g, 0, 0, w.inventoryImage)
    let num = new Phaser.BitmapText(g, 0, 0, "visitor", i+1, 10) 
    num.tint = 0xd01f02
    grp.addChild(spr)
    grp.addChild(num)
    num.x = 13
    num.y = 13
    return grp    
  }
  
  function TagDisplay(i, price) {
    let grp = new Phaser.Group(g)
    let img = "tags"
    if(i in g.savedWeapons) {
      img = "tags-small"
      console.log(g.savedWeapons[i]['name'])
      let weapCtor = Weapon[g.savedWeapons[i]['name']]
      let weapImg = weapCtor().inventoryImage
      let weap = new Phaser.Sprite(g, 0, 0, weapImg)
      grp.addChild(weap)
      if(price) {
        let p = g.getTagPrice(i)
        let priceText = new Phaser.BitmapText(g, 0, 0, "visitor", "$" + p, 10)
        priceText.tint = 0xd01f02
        grp.addChild(priceText)
        priceText.y = 14
      }      
    }
    let spr = new Phaser.Sprite(g, 0, 0, img, i)
    grp.addChild(spr)
    return grp
  }
  
  function RegisterAndClaimMenu(state, label1txt, label2txt, label3txt, isClaim) {
    let self = MenuBase(state, 0xffffff, 0, 0, WIDTH, HEIGHT)
    self.openDirection = null
    
    self.weaponPanel = GridSelectionPanel(state, self, WIDTH-4, 50)
    self.weaponPanel.xMargin = 6
    self.addChild(self.weaponPanel)    
    self.weaponPanel.y = isClaim ? 70 : 20
    
    let label = new Phaser.BitmapText(g, 0, 0, "visitor", label1txt, 10) 
    label.anchor.set(0.5, 0.5)
    label.tint = 0x020628
    self.addChild(label) 
    label.x = WIDTH / 2
    label.y = 15
    
    let grid = [[]]
    let i = 0
    g.weapons.forEach(function(w) {
      if(w != null) {
        grid[0].push({element:WeaponDisplay(i), value:i})    
        i++
      }
    })
    self.weaponPanel.setGrid(grid)
    
    let label2 = new Phaser.BitmapText(g, 0, 0, "visitor", label2txt, 10)
    label2.anchor.set(0.5, 0.5)
    label2.tint = 0x020628
    self.addChild(label2) 
    label2.x = WIDTH / 2
    label2.y = 65
    
    self.tagPanel = GridSelectionPanel(state, self, WIDTH-4, 50)
    self.tagPanel.xMargin = 6
    self.addChild(self.tagPanel)
    self.tagPanel.y = isClaim ? 20 : 70
    
    let grid2 = [[]]
    for(let i = 0; i < g.numTags; i++) {
      grid2[0].push({element:TagDisplay(i, isClaim), value:i})
    }
    self.tagPanel.setGrid(grid2)   
    
    self.weaponPanel.active = true
    self.tagPanel.active = false
    
    self.confirmLabel = new Phaser.BitmapText(g, 0, 0, "visitor", label3txt, 20)
    self.confirmLabel.anchor.set(0.5, 0.5)
    self.confirmLabel.tint = 0xa2a091   
    self.addChild(self.confirmLabel) 
    self.confirmLabel.x = WIDTH / 2
    self.confirmLabel.y = 115
    
    self.selIndex = 0
    
    self.openUpdate = function() {
      if(g.cursors.down.downDuration(1)) {
        self.selIndex = (self.selIndex + 1) % 3
      }
      if(g.cursors.up.downDuration(1)) {
        self.selIndex -= 1
        if(self.selIndex < 0) { self.selIndex += 3}
      }      
      let weapIndex = isClaim ? 1 : 0
      let tagIndex = isClaim ? 0 : 1
      if(self.selIndex == weapIndex) {
        self.weaponPanel.active = true
        self.tagPanel.active = false
        self.confirmLabel.tint = 0xa2a091
      }
      if(self.selIndex == tagIndex) {
        self.weaponPanel.active = false
        self.tagPanel.active = true
        self.confirmLabel.tint = 0xa2a091
      }      
      if(self.selIndex == 2) {
        self.weaponPanel.active = false
        self.tagPanel.active = false
        self.confirmLabel.tint = 0xd01f02
        if(g.selectButton.downDuration(1)) {
          self.activate(self.weaponPanel.getSelectedValue(), self.tagPanel.getSelectedValue())
          self.close()
        }
      }      
    }
    
    return self
  }
  
  
  function ctor(state){
    let self = MenuBase(state, 0xffffff, X, Y, WIDTH, HEIGHT)
    
    self.weaponPanel = GridSelectionPanel(state, self, WIDTH-4, 50)
    self.weaponPanel.yMargin = 10
    
    let txt1 = new Phaser.BitmapText(g, 0, 0, "visitor", "Register a weapon", 10) 
    txt1.tint = 0x020628
    let txt2 = new Phaser.BitmapText(g, 0, 0, "visitor", "Claim a registered weapon", 10) 
    txt2.tint = 0x020628
    
    self.weaponPanel.setGrid([[{element:txt1, value:"register"}],[{element:txt2, value:"claim"}]])
    
    self.addChild(self.weaponPanel)
    self.weaponPanel.x = 46
    self.weaponPanel.y = 30
    
    let label = new Phaser.BitmapText(g, 0, 0, "visitor", "Registry", 20)
    label.tint = 0x020628
    label.anchor.set(0.5, 0.5)
    self.addChild(label)
    label.x = WIDTH / 2
    label.y = 15
    
    self.openUpdate = function() {
      if(g.selectButton.downDuration(1)) {
        let val = self.weaponPanel.getSelectedValue()
        if(val == "register") {
          let rm = RegisterAndClaimMenu(state, "pick a weapon to register", "pick a tag", "register", false)
          self.openChildMenu(rm)
          rm.activate = function(weaponIndex, tagIndex) {
            g.saveWeapon(tagIndex,g.weapons[weaponIndex].getData())
          }
        } else if (val == "claim") {
          let cm = RegisterAndClaimMenu(state, "pick a registered weapon", "pick a weapon to replace", "claim", true)
          self.openChildMenu(cm)
          cm.activate = function(weaponIndex, tagIndex) {
            if(g.loadWeapon(tagIndex,weaponIndex)) {
              state.updateWeapons()
            }
          }
        }
      }
    }
    
    return self
  }
  
  return ctor
})()

const Registry = (function() {
  const ACTIVE_RANGE = 32
  function ctor(state, x, y) {
    self = new Phaser.Sprite(g, x, y+GS, "registry")
    self.anchor.set(0.5, 1)
    
    self.label = new Phaser.BitmapText(g, 0, 0, "visitor", "[a] register / claim", 10)
    self.label.anchor.set(0.5, 0.5)
    self.addChild(self.label)
    self.label.x = 0
    self.label.y = -64
    self.label.tint = 0xbb499a
    self.label.visible = false    
    
    self.menu = RegistryMenu(state)
    state.menuLayer.add(self.menu)
    
    self.update = function() {
      let dx = state.player.x - self.x
      let dy = state.player.y - (self.y - 8)
      let d = dx*dx+dy*dy      
      if(d <= ACTIVE_RANGE ** 2) {      
        self.label.visible = true
        
        if(g.useButton.downDuration(1)) {
            self.menu.open()
        }
        
      } else {
        self.label.visible = false
      }
    }
    
    return self
  }
  
  return ctor
  
})()
