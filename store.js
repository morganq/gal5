const Store = (function() {
  const XOFF = 45
  function ctor(state, x, y) {
    self = new Phaser.Group(g)
    
    let storeSelection = g.dungeon.getRoom().storeSelection
    
    for(let i = 0; i < storeSelection.length; i++) {
      if(g.dungeon.getRoom().status.storePurchases.includes(i)) { continue }
      let grp = new Phaser.Group(g)
      let sticker = storeSelection[i]
      let gfx = g.createStickerGraphic(sticker)
      let spr = new Phaser.Sprite(g, x - 80 + i * XOFF - Math.floor(gfx.width/2), y - 30 - gfx.height, gfx)
      g.physics.enable(spr)
      grp.addChild(spr)
      
      let price = Math.floor((i * 5 + 15) * (g.dungeon.floor ** 1.25))
      let txt = new Phaser.BitmapText(g, spr.x + spr.width - 10, spr.y - 7, "visitor", "$" + price, 10)
      txt.tint = 0xbb499a
      
      let box = new Phaser.Graphics(g, txt.x-1, txt.y)
      box.beginFill(0xffffff,1)
      box.drawRect(0,0,txt.width+2,txt.height+2)
      box.endFill()
      box.update()
      
      grp.addChild(box)
      grp.addChild(txt)   
      
      grp.update = function(){
        grp.y = Math.floor(Math.sin(g.now / 200 + i) * 2.5)
        
        g.physics.arcade.overlap(spr, state.player, function(_,__) {
          if(g.credits >= price) {
            let newStick = g.grantSticker(sticker)
            state.award(newStick.sprite, newStick.name, COLOR_FOR_RARITY[newStick.rarity], spr.x, spr.y)
            g.credits -= price
            g.dungeon.saveStorePurchase(i)
            grp.destroy()
          }
          
        })
        
      }
         
      self.addChild(grp)
    }
    
    return self
  }
  return ctor
})()
