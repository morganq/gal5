const Player = (function() {
  const GUN_POINTS = [
    {x: -4, y:4, flipX:false, flipY: false, angle:90}, //standing
    {x: -5, y:-3, flipX:true, flipY: false, angle:90}, //standing-postfire
    {x: 4, y:-1, flipX:false, flipY: false, angle:0}, //standing-fire
    {x: 0, y:2, flipX:false, flipY: false, angle:0}, // running
    {x: 4, y:1, flipX:false, flipY: false, angle:0},
    {x: 5, y:-1, flipX:false, flipY: false, angle:0},
    {x: 5, y:0, flipX:false, flipY: false, angle:0},
    {x: 4, y:1, flipX:false, flipY: false, angle:0},
    {x: 0, y:2, flipX:false, flipY: false, angle:0},
    {x: -3, y:1, flipX:false, flipY: false, angle:90},
    {x: -2, y:2, flipX:false, flipY: false, angle:90}, // 10
    {x: 5, y:0, flipX:false, flipY: false, angle:0}, // running-fire
    {x: 5, y:0, flipX:false, flipY: false, angle:0},
    {x: 6, y:-2, flipX:false, flipY: false, angle:0},
    {x: 7, y:-2, flipX:false, flipY: false, angle:0},
    {x: 6, y:-1, flipX:false, flipY: false, angle:0},
    {x: 6, y:0, flipX:false, flipY: false, angle:0},
    {x: 6, y:-1, flipX:false, flipY: false, angle:0},
    {x: 6, y:-1, flipX:false, flipY: false, angle:0},    
    {x: 2, y:-4, flipX:false, flipY: false, angle:-90}, // sliding
    {x: 0, y:4, flipX:false, flipY: false, angle:90}, // jump-fire-down
    {x: 0, y:4, flipX:false, flipY: false, angle:90},
    {x: 0, y:4, flipX:false, flipY: false, angle:90},
    {x: 8, y:0, flipX:false, flipY: false, angle:0}, // roll
    {x: 5, y:7, flipX:false, flipY: false, angle:0},
    {x: 3, y:8, flipX:false, flipY: false, angle:90},
    {x: -2, y:8, flipX:false, flipY: false, angle:180},
    {x: 2, y:2, flipX:false, flipY: false, angle:-90},
    {x: 6, y:5, flipX:false, flipY: false, angle:0},
    {x: -2, y:6, flipX:false, flipY: false, angle:90},
    {x: 0, y:0, flipX:false, flipY: false, angle:0}, // airdodge-start
    {x: 0, y:0, flipX:false, flipY: false, angle:0}, 
    {x: 0, y:0, flipX:false, flipY: false, angle:0}, // airdodge-roll
    {x: 0, y:0, flipX:false, flipY: false, angle:0},
    {x: 0, y:0, flipX:false, flipY: false, angle:0},
    {x: 0, y:0, flipX:false, flipY: false, angle:0},
    {x: 3, y:-3, flipX:false, flipY: false, angle:0}, // axe attack
    {x: 4, y:-3, flipX:false, flipY: false, angle:0},
    {x: 3, y:-1, flipX:false, flipY: false, angle:0},
    {x: 4, y:2, flipX:false, flipY: false, angle:0},
    {x: -4, y:4, flipX:false, flipY: false, angle:90}, // shadow
  ]

  const MAX_SPEED = 80
  const SM_MAX_SPEED = 2

  const JUMP_SPEED = 153
  const SM_JUMP_SPEED = 2

  const HORIZ_ACCEL = 2000
  const HORIZ_DECEL = 2000
  const MAX_VERTICAL_SPEED = 300
  const PRECISE_JUMP_BOOST = 40
  const PRECISE_JUMP_TIMING = 120
  const KEYBUFFER_TIME_MS = 100
  const EARLYJUMP_DURATION_MS = 150
  const EARLYJUMP_BOOST_GRAVITY_RATIO = 1.2
  const GRAVITY = BASE_GRAVITY

  const AIRDODGE_VERTICAL_BOOST = 180
  const AIRDODGE_HORIZONTAL_SPEED = 90
  const AIRDODGE_RETURN_CONTROL_TIME_MS = 350

  const SLIDE_END_SPEED = 10
  const SLIDE_DECEL = 200

  const ROLL_SPIN_RATIO = 0.1
  const ROLL_INTO_SLIDE_CANCEL_RATIO = 0.01
  const ROLL_HOP_HEIGHT = 1
  const ROLL_HOP_FORCE = 60
  const ROLL_START_SPEED = 120
  const ROLL_END_SPEED = 50
  const ROLL_DURATION_MS = 550
  const ROLL_STUN_TIME_MS = 200
  const ROLL_ANGLES = 8
  const ROLL_DI_ACCEL = 150
  const ROLL_DI_FASTER = 20
  const ROLL_DI_SLOWER = -20
  
  const RECOVER_TIME_MS = 200
  const RECOVER_INVUL_TIME_MS = 1000
  
  const FIRED_STAND_ANIM_TIME_MS = 1400
  const FIRED_RUN_ANIM_TIME_MS = 800
  const FIRED_SHOOT_ANIM_TIME_MS = 600

  const BULLET_AUTOFIRE_TIME_MS = 320
  const BULLET_QUICKFIRE_TIME_REDUCTION_MS = 200

  const SURFACE_FALLOFF_TIME_MS = 100
  
  const DEFAULT_MAX_HEALTH=100
  
  //const RUN_POOF_FRAMES = [3, 7, 11, 15]
  const RUN_POOF_FRAMES = [3, 5, 7, 9, 11, 13, 15, 17]
  
  //special
  const SHIELD_START_TIME = 2500

  const STATES = {
    RUNNING:"running",
    JUMPING:"jumping",
    EARLYJUMP:"earlyjump",
    ROLLING:"rolling",
    ROLLSTUN:"rollstun",
    AIRDODGING:"airdodging",
    SLIDING:"sliding",
    RECOVERING:"recovering"
  }

  function ctor(state, x, y) {
    let player = new Phaser.Sprite(g, x, y, "hero")
    player.anchor.set(0.5, 0.5)
    player.animations.add('stand', [0], 12, true)
    player.animations.add('stand-postfire', [1], 12, true)
    player.animations.add('stand-fire', [2], 12, true)
    player.animations.add('run', [3,4,5,6,7,8,9,10], 12, true)
    player.animations.add('run-fire', [11,12,13,14,15,16,17,18], 12, true)
    player.animations.add('slide', [19], 12, true)
    player.animations.add('jump1', [8], 12, true)
    player.animations.add('jump2', [9], 12, true)
    player.animations.add('jump3', [10], 12, true)
    player.animations.add('jump1-fire', [16], 12, true)
    player.animations.add('jump2-fire', [17], 12, true)
    player.animations.add('jump3-fire', [18], 12, true)
    player.animations.add('jump1-fire-down', [20], 12, true)
    player.animations.add('jump2-fire-down', [21], 12, true)
    player.animations.add('jump3-fire-down', [22], 12, true)
    player.animations.add('roll', [23,23,24,24,25,26,27,28], 12, false)
    player.animations.add('rollstun', [29], 12, false)
    player.animations.add('airdodge-start', [30,31], 12, false)
    player.animations.add('airdodge-roll', [32,33,34,35], 8, true)
    player.animations.add('axe-attack', [36,37,38,39], 16, false)
    player.animations.add('shadow', [40], 16, false)
    player.animations.play('shadow')

    player.weapons = []

    player.resetWeapons = function() {
      player.weapons.forEach(function(w) {
        w.destroy()
        player.removeChild(w)
      })
      player.weapons = []
      g.weapons.forEach(function(weap) {
        if(weap) {
          let ws = weap.instantiateSprite(g, state)
          player.weapons.push(ws)
          player.addChild(ws)
        }
      })
      if(player.selectWeapon) {
        player.selectWeapon(g.curWeaponIndex+1)
      }
    }
    player.resetWeapons()

    g.physics.enable(player)
    player.body.setSize(6, 16, 5, 0)
    g.slopes.enable(player)
    
    player.health = 100
    player.maxHealth = 100
    
    player.addHealth = function(amt) {
      player.health = Math.max(Math.min(player.health + amt, player.maxHealth), 0)
    }
    
    player.getHitTime = 0
    
    player.weaponButtons = ["ONE", "TWO", "THREE", "FOUR", "FIVE"].map(function(num) {
      return g.input.keyboard.addKey(Phaser.Keyboard[num])
    })

    debugButton = g.input.keyboard.addKey(Phaser.Keyboard.V)
    player.debugStartTime = 0
    player.debugRecording = []
    player.debugButton = debugButton
    
    player.debugCollisionChecks = []

    player.state = STATES.RUNNING
    player.jumpTime = 0
    player.rollTime = 0
    player.rollStunTime = 0
    player.rollDI = 0
    player.firedTime = 0
    player.airdodgeTime = 0
    player.airdodgeInitialVelocity = 0

    player.slideStartSpeed = 0

    player.facing = 1
    player.fireAngle = 0
    player.lastOnSurface = 0
    player.fakeAlpha = 1
    
    player.justTriggerFired = false
    
    player.bonusCritChance = 0
    
    player.justStartedLevel = false
    
    // special stat stuff
    player.vampireLife = 0
    player.shield = 0
    player.maxShield = 0
    
    player.jumpEffect = new Phaser.Graphics(g, 0, 0)
    state.effectLayer.add(player.jumpEffect)
    player.jumpEffectTime = 0
    player.jumpEffectStart = new Phaser.Point(0,0)
    
    player.rollEffect = new Phaser.Graphics(g, 0, 0)
    state.effectLayer.add(player.rollEffect)
    player.rollEffectTime = 0
    player.rollEffectStart = new Phaser.Point(0, 0)
    player.lastRollPos = new Phaser.Point(0, 0)
    
    player.wallJumpEffect = new Phaser.Graphics(g, 0, 0)
    state.effectLayer.add(player.wallJumpEffect)
    player.wallJumpEffectTime = 0
    player.wallJumpEffectStart = new Phaser.Point(0, 0)
    player.lastWJPos = new Phaser.Point(0, 0)    
    
    player.lastRunPoofTime = 0
    player.lastSlidePoofTime = 0
    
    player.startFloatTime = 0
    
    player.killed = false
    
    player.bonusFlash = new Phaser.Sprite(g, 0,0, "bonuseye")
    player.bonusFlash.animations.add('spark',[0,1], 15, true)
    player.bonusFlash.play('spark')
    player.bonusFlash.anchor.set(0.5, 0.5)
    player.bonusFlash.rotation = 0
    state.effectLayer.add(player.bonusFlash)    
    
    player.helpKeys = new Phaser.Group(g)
    player.zHelpKey = new Phaser.Sprite(g, 0, 0, 'helpkeys', 0)
    player.xHelpKey = new Phaser.Sprite(g, 0, 0, 'helpkeys', 2)
    player.cHelpKey = new Phaser.Sprite(g, 0, 0, 'helpkeys', 4)
    player.leftHelpKey = new Phaser.Sprite(g, 0, 0, 'helpkeys', 6)
    player.rightHelpKey = new Phaser.Sprite(g, 0, 0, 'helpkeys', 8)
    
    player.helpKeys.addChild(player.zHelpKey)
    player.helpKeys.addChild(player.xHelpKey)
    player.helpKeys.addChild(player.cHelpKey)
    player.helpKeys.addChild(player.leftHelpKey)
    player.helpKeys.addChild(player.rightHelpKey)  
    player.helpKeys.visible = false
    
    player.lastFrameX = player.body.x
    player.lastFrameY = player.body.y
    
    player.shadow = Shadow(state, player, 7, -1)
    
    player.selectWeapon = function(number) {
      let changed = g.setCurrentWeapon(number)
      if(changed) {
        player.weapons.forEach(function(w) {
          w.visible = false
        })
        player.currentWeapon = player.weapons[number-1]
        player.currentWeapon.visible = true
        state.setCurrentWeapon(number)
      }
    }
    
    player.selectWeapon(g.curWeaponIndex+1)

    player.resetButtons = function() {
      player.buttons = {
        'up':false,
        'down':false,
        'left':false,
        'right':false,
        'jump':false,
        'roll':false,
        'fire':false
      }
      player.lastButtons = Object.assign({}, player.buttons)
      player.buffered = Object.assign({}, player.buttons)
    }

    player.resetButtons()

    player.onSurface = function() {
      return player.body.blocked.down || player.body.touching.down

    }
    player.recentlyOnSurface = function() {
      let currentlyOn = player.onSurface()
      if(currentlyOn) {
        player.lastOnSurface = g.now
        return true
      }
      if(!currentlyOn) {
        if(g.now > player.lastOnSurface + SURFACE_FALLOFF_TIME_MS) {
          return false
        } else {
          return true
        }
      }
    }
    player.nearLeft = function() {
      return state.tilemapLayer.getTiles(
        Math.min(player.body.left, player.body.right) - 2,
        player.y,
        2,
        player.bottom - player.y,
        true
      ).filter(function(t) { return t.index != T_PLATFORM }).length > 0
    }
    player.nearRight = function() {
      return state.tilemapLayer.getTiles(
        Math.max(player.body.left, player.body.right),
        player.y,
        2,
        player.bottom - player.y,
        true
      ).filter(function(t) { return t.index != T_PLATFORM }).length > 0
    }
    player.nearSurface = function() {
      let tiles = state.tilemapLayer.getTiles(
        Math.min(player.body.left, player.body.right),
        player.bottom,
        Math.abs(player.body.right - player.body.left),
        8,
        true)
      //console.log(tiles)
      this.debugTiles = tiles
      if(tiles.length > 0) {
        return true
      }
      return false
    }
    
    player.recoil = function(x,y) {
      
    }
    
    player.getRollDuration = function() {
      return ROLL_DURATION_MS / g.pct(g.getStat('roll'))
    }
    
    player.canBeHit = function() {
      if(player.justStartedLevel) {
        return false
      }
      if(g.now <= player.getHitTime + RECOVER_INVUL_TIME_MS) {
        return false
      }
      if([STATES.ROLLING, STATES.AIRDODGING].includes(player.state)) {
        return false
      }
      return true
    }

    player.update = function() {
      if(player.health <= 0 && !player.killed) {
        player.killed = true
        g.camera.fade(0x000000, 2000, false)
        g.camera.onFadeComplete.add(function() {
          g.camera.fx.beginFill(0x000000);
          g.camera.fx.drawRect(0, 0, g.camera.width, g.camera.height);
          g.camera.fx.endFill();
          g.camera.fx.alpha = 1
          
        })
        return
      }
      if(player.vampireLife > 1) {
        player.addHealth(Math.floor(player.vampireLife))
        player.vampireLife -= Math.floor(player.vampireLife)
      }
      if(!player.mindControl || !player.mindControl.exists) {
        player.buttons['up'] = g.cursors.up.isDown
        player.buttons['down'] = g.cursors.down.isDown
        player.buttons['left'] = g.cursors.left.isDown
        player.buttons['right'] = g.cursors.right.isDown
        player.buttons['jump'] = g.jumpButton.isDown
        player.buttons['roll'] = g.dodgeButton.isDown
        player.buttons['fire'] = g.fireButton.isDown
      }
      let justPressed = {
        'up': player.buttons['up'] && !player.lastButtons['up'],
        'down': player.buttons['down'] && !player.lastButtons['down'],
        'left': player.buttons['left'] && !player.lastButtons['left'],
        'right': player.buttons['right'] && !player.lastButtons['right'],
        'jump': player.buttons['jump'] && !player.lastButtons['jump'],
        'roll': player.buttons['roll'] && !player.lastButtons['roll'],
        'fire': player.buttons['fire'] && !player.lastButtons['fire'],
      }
      for(key in player.buttons) {
        if(justPressed[key]) {
          player.buffered[key] = KEYBUFFER_TIME_MS
        } else if(player.buffered[key] > 0) {
          player.buffered[key] = player.buffered[key] - g.elapsed
        }
        if(player.buffered[key] <= 0) { player.buffered[key] = false }
        if(player.buttons[key]) { player.justStartedLevel = false }
      }
      
      player.weaponButtons.forEach(function(key, i) {
        if(key.isDown) {
          player.selectWeapon(i+1)
        }
      })
      
      // Update bonus crit chance
      if(player.state != STATES.ROLLSTUN && g.now < player.rollStunTime + ROLL_STUN_TIME_MS + g.getStat('roll') * 4) {
        player.bonusCritChance = g.getStat('roll')
      } else {
        player.bonusCritChance = 0
      }
      if(player.bonusCritChance > 0) {
        player.bonusFlash.visible = true
        player.bonusFlash.x = player.x + player.scale.x * 5
        player.bonusFlash.y = player.y - 6.5
      } else {
        player.bonusFlash.visible = false
      }
      
      player.currentWeapon.update()
      let fireThisFrame = false
      let statMaxSpeed = MAX_SPEED * g.pct(g.getStat('speed')/2)

      dt = g.elapsed / 1000.0
      let targetX = 0
      if(player.buttons.left) {
        targetX -= 1
      }
      if(player.buttons.right) {
        targetX += 1
      }
      let targetY = 0
      if(player.buttons.up) {
        targetY = -1
      }
      if(player.buttons.down) {
        targetY = 1
      }


      if(player.body.velocity.y < 0) {
        player.body.velocity.y += GRAVITY * g.elapsed
      } else {
        if(player.startFloatTime < player.jumpTime) {
          player.startFloatTime = g.now
        }        
        let floating = 1
        if(player.buttons['jump']) {
          if(g.now < player.startFloatTime + g.getStat('float') * 10) {
            let t = (g.now - player.startFloatTime) / (g.getStat('float') * 10)
            let maxyv = Math.max(75 - g.getStat('float'), 0)
            player.body.velocity.y = Math.min(player.body.velocity.y, maxyv)
          }
        }
        player.body.velocity.y += GRAVITY * g.elapsed * floating
      }
      player.body.offset.y = 0

      let nextState = null

      function getNormalMovementVelocityX(ms) {
        if(!ms) { ms = statMaxSpeed }
        if(targetX === 0) {
          if(player.body.velocity.x > 0) { return Math.max(0, player.body.velocity.x - HORIZ_DECEL * dt) }
          else { return Math.min(0, player.body.velocity.x + HORIZ_DECEL * dt) }
        } else {
          if(player.currentWeapon.playerSlowdown) { 
            ms *= (1 - player.currentWeapon.playerSlowdown)
          }
          return g.math.clamp(player.body.velocity.x + targetX * HORIZ_ACCEL * dt, -ms, ms)
        }
      }

      function isSurfaceAvailableForJump() {
        return player.onSurface() || (player.nearSurface() && player.body.velocity.y >= 0)
      }

      function executeJump() {
        let statJumpSpeed = JUMP_SPEED * g.pct(g.getStat('jump'))

        nextState = STATES.EARLYJUMP
        player.body.velocity.y = -statJumpSpeed
        player.jumpTime = g.now
        player.rotation = 0
        state.soundJump.play()
        player.jumpEffectStart.set(player.x, player.bottom)
        player.jumpEffectTime = g.now
      }
      
      if(g.now < player.jumpEffectTime + 100) {
        let t = (g.now - player.jumpEffectTime) / 200
        //player.jumpEffect.alpha = 1-t
        player.jumpEffect.visible = true
      } else {
        player.jumpEffect.visible = false
      }
      
      if(g.now < player.rollEffectTime + 100) {
        let t = (g.now - player.rollEffectTime) / 200
        //player.rollEffect.alpha = 1-t
        player.rollEffect.visible = true
      } else {
        player.rollEffect.visible = false
      }
      
      if(g.now < player.wallJumpEffectTime + 100) {
        let t = (g.now - player.wallJumpEffectTime) / 200
        //player.rollEffect.alpha = 1-t
        player.wallJumpEffect.visible = true
      } else {
        player.wallJumpEffect.visible = false
      }      
      
      if([STATES.RECOVERING].includes(player.state)) {
        //let bt = (player.getHitTime + RECOVER_TIME_MS - g.now) / 100
        //document.getElementById("game").style.webkitFilter="blur(" + bt + "px)"
        if(g.now > player.getHitTime + RECOVER_TIME_MS) {
          nextState = STATES.RUNNING
          //document.getElementById("game").style.webkitFilter="blur(0px)"
        }
      }

      // Basic left-right movement
      if([STATES.RUNNING, STATES.JUMPING, STATES.EARLYJUMP].includes(player.state)) {
        player.body.velocity.x = getNormalMovementVelocityX()
      }

      // Roll movement
      if([STATES.ROLLING].includes(player.state)) {
        player.updateRollEffect()
        player.rollEffectTime = g.now
        rollT = (g.now - player.rollTime) / player.getRollDuration()
        if((player.facing == 1 && player.buttons.right) || (player.facing == -1 && player.buttons.left)) {
          player.rollDI = Math.min(player.rollDI + ROLL_DI_ACCEL * dt, ROLL_DI_FASTER)
        }
        if((player.facing == 1 && player.buttons.left) || (player.facing == -1 && player.buttons.right)) {
          player.rollDI = Math.max(player.rollDI - ROLL_DI_ACCEL * dt, ROLL_DI_SLOWER)
        }
        // Adjust angle of fire
        if(rollT < ROLL_SPIN_RATIO) {
          let hopT = g.math.clamp(rollT / ROLL_SPIN_RATIO, 0, 1)
          //player.body.offset.y = Math.sin(hopT * 3.14159) * ROLL_HOP_HEIGHT
        }
        if(rollT > ROLL_SPIN_RATIO) {
          //player.body.offset.y = 0
          let angleT = (rollT - ROLL_SPIN_RATIO) * (1 / (1 - ROLL_SPIN_RATIO))
          let rawAngle = 0
          if(player.facing == 1) {
            rawAngle = angleT * 6.2818
          } else {
            rawAngle = -angleT * 6.2818 + 3.14159
          }
          let tempRotation = g.math.clamp(g.math.wrapAngle(rawAngle,true), -3.14159, 3.14159)
          tempRotation = g.math.snapTo(tempRotation, 6.2818 / ROLL_ANGLES)
          // temp
          //player.rotation = tempRotation + ((player.facing == 1) ? 0 : 3.14159)

          // wrong
          if(player.buttons.jump &&
              g.now > player.rollTime + player.getRollDuration() - PRECISE_JUMP_TIMING &&
              isSurfaceAvailableForJump()) {
            state.soundBonus.play()
            executeJump()
            player.body.velocity.y -= PRECISE_JUMP_BOOST
          }

        }
        if(rollT > ROLL_INTO_SLIDE_CANCEL_RATIO) {
          if(player.buffered['fire']) {
            nextState = STATES.SLIDING
            player.fireAngle = - 3.14159/2
            player.slideStartSpeed = Math.abs(player.body.velocity.x)
          }
        }
        if(rollT > 1) {
          let tempRotation = (player.facing == 1) ? 0 : 3.14159
          player.rotation = tempRotation + ((player.facing == 1) ? 0 : 3.14159)
          nextState = STATES.ROLLSTUN
          player.rollStunTime = g.now

        } else {
          rollSpeed = rollT * (ROLL_END_SPEED - ROLL_START_SPEED) + ROLL_START_SPEED + player.rollDI
          rollSpeed *= g.pct(g.getStat('roll'))
          player.body.velocity.x = player.facing * rollSpeed
        }
      }

      // Roll stun
      if([STATES.ROLLSTUN].includes(player.state)) {
        player.body.velocity.x = 0
        if(g.now > player.rollStunTime + (ROLL_STUN_TIME_MS / g.pct(g.getStat('roll')))) {
          if(player.onSurface()) {
            nextState = STATES.RUNNING
          } else {
            nextState = STATES.JUMPING
          }
        }
      }

      // Sliding
      if([STATES.SLIDING].includes(player.state)) {
        if(Math.abs(player.body.velocity.x) < SLIDE_END_SPEED) {
          nextState = STATES.RUNNING
        } else {
          if(player.body.velocity.x > 0) {
            player.body.velocity.x = g.math.clamp(player.body.velocity.x - SLIDE_DECEL * dt, 0, player.slideStartSpeed)
          } else {
            player.body.velocity.x = g.math.clamp(player.body.velocity.x + SLIDE_DECEL * dt, -player.slideStartSpeed, 0)
          }
        }
        player.fireAngle = - 3.14159 / 2
        player.rotation = 0
      }
      
      // Update rotation
      if(player.state != STATES.ROLLING) {
        player.rotation = 0
        player.angle = 0
      }

      // Update facing
      if([STATES.RUNNING, STATES.JUMPING, STATES.EARLYJUMP].includes(player.state)) {
        if(targetX != 0) { player.facing = targetX }
      }

      // Update desired angle for most states
      if([STATES.RUNNING, STATES.JUMPING, STATES.EARLYJUMP].includes(player.state)) {
        player.fireAngle = (player.facing == 1) ? 0 : 3.14159
      }
      
      // do step poof effect
      if([STATES.RUNNING].includes(player.state)) {
        if(g.now > player.lastRunPoofTime + 100 && RUN_POOF_FRAMES.includes(player.frame)) {
          player.lastRunPoofTime = g.now
          let fx = player.createEffect('effect-poof1',[0,1,2,3,4,5,6], 20, player.x + player.scale.x * -1, player.bottom - 3, 0xf5f4e5)
          fx.scale.set(g.rnd.frac() * 0.5 + 0.5)
        }
      }

      // do slide poof effect
      if([STATES.SLIDING].includes(player.state)) {
        if(g.now > player.lastSlidePoofTime + 110) {
          player.lastSlidePoofTime = g.now
          player.createEffect('effect-poof1',[0,1,2,3,4,5,6], 20, player.x + player.scale.x * -1, player.bottom - 3, 0xf5f4e5)
        }
      }

      // Air aiming
      if([STATES.JUMPING, STATES.EARLYJUMP].includes(player.state)) {
        if(targetY > 0) {
          player.fireAngle = 3.14159/2
        }
        
      }

      // Jump trigger
      if([STATES.RUNNING, STATES.ROLLING, STATES.SLIDING].includes(player.state)) {
        if(player.buffered.jump && isSurfaceAvailableForJump()) {
          // Make sure we don't override a superjump
          if(nextState !== STATES.EARLYJUMP) {
            executeJump()
          }
        }
      }

      // Jump extra height
      if([STATES.EARLYJUMP].includes(player.state)) {
        player.updateJumpEffect()
        player.jumpEffectTime = g.now        
        
        // If they let go of jump we're done with this state
        if(!player.buttons.jump) {
          nextState = STATES.JUMPING
        }
        else if(player.jumpTime + EARLYJUMP_DURATION_MS < g.now) {
          nextState = STATES.JUMPING
        }
        else {
          player.body.velocity.y -= GRAVITY * EARLYJUMP_BOOST_GRAVITY_RATIO * g.elapsed
        }
      }

      // Roll trigger
      if([STATES.RUNNING].includes(player.state)) {
        if(player.buffered.roll) {
          nextState = STATES.ROLLING
          state.soundDodge.play()
          player.rollTime = g.now
          player.body.velocity.y = -ROLL_HOP_FORCE * g.pct(g.getStat('roll'))
          player.rollDI = 0
          let px = player.x - player.width / 2
          let py = player.y
          player.lastRollPos.set(0, 0)
          player.rollEffect.clear()
          player.rollEffectTime = g.now
          player.rollEffectStart.set(px, py)
        }
      }

      // Airdodge trigger (it's like a wallkick that starts a spin)
      //if([STATES.EARLYJUMP, STATES.JUMPING].includes(player.state)) {
      //if([STATES.JUMPING, STATES.ROLLING].includes(player.state)) {
      if([STATES.JUMPING].includes(player.state)) {
        if(player.buffered.roll) {
          // now check if we're next to a wall but not a floor
          if(!isSurfaceAvailableForJump() && (player.nearLeft() || player.nearRight())) {
            nextState = STATES.AIRDODGING
            player.wallJumpEffect.clear()
            player.wallJumpEffectTime = g.now            
            player.wallJumpEffectStart.set(player.x, player.y)
            player.lastWJPos.set(0,0)
            state.soundDodge.play()
            player.airdodgeTime = g.now
            player.body.velocity.y = - AIRDODGE_VERTICAL_BOOST * g.pct(g.getStat('roll') / 2)
            if(player.nearLeft()) {
              player.body.velocity.x = AIRDODGE_HORIZONTAL_SPEED
              player.airdodgeInitialVelocity = AIRDODGE_HORIZONTAL_SPEED
              player.facing = 1
            }
            if(player.nearRight()) {
              player.body.velocity.x = -AIRDODGE_HORIZONTAL_SPEED
              player.airdodgeInitialVelocity = -AIRDODGE_HORIZONTAL_SPEED
              player.facing = -1
            }
          }
        }
      }

      // Airdodge control manip
      if([STATES.AIRDODGING].includes(player.state)) {
        player.updateWJEffect()
        player.wallJumpEffectTime = g.now        
        let controlT = g.math.clamp(
          (g.now - player.airdodgeTime) / AIRDODGE_RETURN_CONTROL_TIME_MS * g.pct(g.getStat('roll')), 0, 1)
        let controlVelocity = getNormalMovementVelocityX()
        desiredX = controlVelocity * controlT + player.airdodgeInitialVelocity * (1-controlT)
        if(Math.sign(player.airdodgeInitialVelocity) == Math.sign(targetX)) {
          let MAX = statMaxSpeed + AIRDODGE_HORIZONTAL_SPEED * (1-controlT)
          player.body.velocity.x = getNormalMovementVelocityX(MAX)
        } else {
          player.body.velocity.x = desiredX
        }
      }

      // Detect landing
      if([STATES.EARLYJUMP, STATES.JUMPING].includes(player.state)) {
        if(player.onSurface()) {
          nextState = STATES.RUNNING
        }
      }
      
      if([STATES.AIRDODGING].includes(player.state)) {
        if(player.onSurface()) {
          nextState = STATES.ROLLSTUN
          player.rollStunTime = g.now
        }
      }      

      // Detect firing
      if([STATES.RUNNING, STATES.EARLYJUMP, STATES.JUMPING, STATES.SLIDING].includes(player.state)) {
        if(player.buffered.fire && player.currentWeapon.canFire && !player.justTriggerFired) {
            player.justTriggerFired = true
            fireThisFrame = true
            player.firedTime = g.now
        }
        if(player.buttons.fire && player.currentWeapon.canAutoFire) {
          fireThisFrame = true
          player.firedTime = g.now
        }
        if(player.justTriggerFired && !player.buttons.fire && !player.buffered.fire) {
          player.justTriggerFired = false
        }
      }

      // Detect falling off a cliff
      if([STATES.RUNNING].includes(player.state)) {
        if(!player.nearSurface()) {
          nextState = STATES.JUMPING
        }
      }

      player.scale.x = player.facing

      // Detect if we're vulnerable
      if(player.canBeHit()) {
        g.physics.arcade.overlap(player, state.enemyLayer, function(p,e) {
          if(e.damage > 0) {
            player.getHit(e.damage, e.x)
          }
        })
      }
      if(g.now <= player.getHitTime + RECOVER_INVUL_TIME_MS) {
        player.fakeAlpha = 1-player.fakeAlpha
      } else {
        player.fakeAlpha = 1
      }

      if(nextState != null) {
        player.state = nextState
      }

      /*if(debugButton.downDuration(25)) {
        player.debugStartTime = g.now
        player.debugRecording = []
        player.body.velocity.x = g.rnd.between(-1500, 1500)
        player.body.velocity.y = g.rnd.between(-1500, 1500)
      }
      if(debugButton.upDuration(25)) {
        console.log(JSON.stringify(player.debugRecording))
      }
      if(debugButton.isDown) {
        // STUFF
        var btns = [
          "fire",
          "jump",
          "roll",
          "left",
          "right",
          "down",
          "up",
        ]
        for(var n in btns) {
          var btn = btns[n]
          if(player.buttons[btn] && !player.lastButtons[btn]) {
            player.debugRecording.push({time:g.now - player.debugStartTime, btn:btn, event:"down"})
          }
          if(!player.buttons[btn] && player.lastButtons[btn]) {
            player.debugRecording.push({time:g.now - player.debugStartTime, btn:btn, event:"up"})
          }
        }
      }
      */

      player.calcAnimation()
      
      player.updateHelpKeys()
      
      if(fireThisFrame) {
        player.currentWeapon.fireAngle = player.fireAngle
        player.currentWeapon.tryFire()
      }

      let oldMax = player.maxHealth
      player.maxHealth = DEFAULT_MAX_HEALTH * g.pct(g.getStat('health'))
      if(oldMax != player.maxHealth) {
        player.addHealth(0)
      }
      
      player.maxShield = g.getStat('shield')
      if(g.now > player.getHitTime + SHIELD_START_TIME) {
        player.shield = Math.min(
          player.shield + g.elapsed / 100 * (g.now-(player.getHitTime + SHIELD_START_TIME)) / 1000,
          player.maxShield)
      }

      // Make sure the vertical speed has a cap
      player.body.velocity.y = g.math.clamp(player.body.velocity.y, -MAX_VERTICAL_SPEED, MAX_VERTICAL_SPEED / g.pct(g.getStat('float')))

      player.lastButtons = Object.assign({},player.buttons)
    }
    
    player.updateHelpKeys = function() {
      player.helpKeys.scale.x = state.playerFake.scale.x
      player.zHelpKey.x = state.playerFake.scale.x * 14 - 5
      player.zHelpKey.y = -10
      
      player.xHelpKey.x = -5
      player.xHelpKey.y = -24
      
      player.cHelpKey.x = state.playerFake.scale.x * 22 - 5
      player.cHelpKey.y = -4    
      
      player.leftHelpKey.x = -22
      player.leftHelpKey.y = -4
      
      player.rightHelpKey.x = 11
      player.rightHelpKey.y = -4
      player.helpKeys.alpha = Math.sin(g.now / 100) * 0.15 + 0.7
      
      if(g.cursors.left.isDown) {
        g.helpActionCount['left'] += g.elapsed
        player.leftHelpKey.frame = 7
      } else {
        player.leftHelpKey.frame = 6
      }      
      
      if(g.cursors.right.isDown) {
        g.helpActionCount['right'] += g.elapsed
        player.rightHelpKey.frame = 9
      } else {
        player.rightHelpKey.frame = 8
      }

      if(g.dodgeButton.isDown) {
        player.zHelpKey.frame = 1
      } else {
        player.zHelpKey.frame = 0
      }
      
      if(g.jumpButton.isDown) {
        player.xHelpKey.frame = 3
      } else {
        player.xHelpKey.frame = 2
      }
      
      if(g.fireButton.isDown) {
        player.cHelpKey.frame = 5
      } else {
        player.cHelpKey.frame = 4
      }      
      
      if(g.helpActionCount['left'] >= 500) {
        player.leftHelpKey.visible = false
      }
      if(g.helpActionCount['right'] >= 500) {
        player.rightHelpKey.visible = false
      }
      if((g.helpActionCount['left'] < 500) || (g.helpActionCount['right'] < 500)) {
        player.xHelpKey.visible = false
        player.cHelpKey.visible = false
        player.zHelpKey.visible = false
      } else {
        player.xHelpKey.visible = true
        player.zHelpKey.visible = true
        
        if(g.jumpButton.downDuration(1)) {
          g.helpActionCount['jump'] += 1
        }
        if(g.helpActionCount['jump'] >= 2) {
          player.xHelpKey.visible = false
        }
        if(g.dodgeButton.downDuration(1)) {
          g.helpActionCount['roll'] += 1
        }        
        if(g.helpActionCount['roll'] >= 2) {
          player.zHelpKey.visible = false
        }        
        if(g.helpActionCount['jump'] >= 2 && g.helpActionCount['roll'] >= 2) {
          player.cHelpKey.visible = true
          
          if(g.fireButton.downDuration(1)) {
            g.helpActionCount['shoot'] += 1
          }
          
          if(g.helpActionCount['shoot'] >= 5) {
            player.cHelpKey.visible = false
          }
          
        }
        
      }
    }
    
    player.createEffect = function(key, frames, framerate, x, y, color) {
      let fx = new Phaser.Sprite(g, 0, 0, key)
      fx.anchor.set(0.5, 0.5)
      fx.animations.add('default', frames)
      fx.play('default', framerate, false, true)
      state.effectLayer.add(fx)
      fx.x = x
      fx.y = y
      fx.tint = color
      return fx
    }

    player.updateWJEffect = function() {
      let t = Math.max(g.now - player.airdodgeTime - 0, 0)
      a = -t / 250 * 6.2818
      let px = player.x + -Math.cos(a) * 6 * player.scale.x
      let py = player.y + Math.sin(a) * 6
      if(player.lastWJPos.x != 0 || player.lastWJPos.y != 0) {
        Aliased.drawLine(player.wallJumpEffect, 0xee83b8, 
          player.lastWJPos.x - player.wallJumpEffectStart.x, player.lastWJPos.y - player.wallJumpEffectStart.y,
          px - player.wallJumpEffectStart.x, py - player.wallJumpEffectStart.y
        )
      }
      player.wallJumpEffect.x = Math.floor(player.wallJumpEffectStart.x)
      player.wallJumpEffect.y = Math.floor(player.wallJumpEffectStart.y)
      player.lastWJPos.set(px, py)
    }
    
    player.updateRollEffect = function() {
      let rollStartFrame = 24
      let rollEndFrame = 28
      let t = ((g.now - player.rollTime) / player.getRollDuration())
      t = Math.cos(t * t * 3.14159) * -0.5 + 0.5
      
      a = -t * 7.5
      let px = player.x + -Math.cos(a) * 6 * player.scale.x
      let py = player.y + Math.sin(a) * 6
      if(player.lastRollPos.x != 0 || player.lastRollPos.y != 0) {
        Aliased.drawLine(player.rollEffect, 0xee83b8, 
          player.lastRollPos.x - player.rollEffectStart.x, player.lastRollPos.y - player.rollEffectStart.y,
          px - player.rollEffectStart.x, py - player.rollEffectStart.y
        )
      }
      player.rollEffect.x = Math.floor(player.rollEffectStart.x)
      player.rollEffect.y = Math.floor(player.rollEffectStart.y)
      player.lastRollPos.set(px, py)
    }
    
    player.updateJumpEffect = function() {
      player.jumpEffect.clear()
      let px = player.x// - player.body.velocity.x / 20
      let py = player.bottom// - player.body.velocity.y / 20
      
      Aliased.drawLine(player.jumpEffect, 0xee83b8, -1 * player.scale.x, 0, px - player.jumpEffectStart.x - 3 * player.scale.x, py - player.jumpEffectStart.y)
      Aliased.drawLine(player.jumpEffect, 0xee83b8, 1 * player.scale.x, 0, px - player.jumpEffectStart.x+1 * player.scale.x, py - player.jumpEffectStart.y - 1)
      
      player.jumpEffect.x = Math.floor(player.jumpEffectStart.x)
      player.jumpEffect.y = Math.floor(player.jumpEffectStart.y)
    }
    
    player.getHit = function(dmg, x) {
      if(g.now <= player.getHitTime + RECOVER_INVUL_TIME_MS) { return }
      g.multiplierTimer = g.now
      g.creditsMultiplier = 1.0
      state.soundHurt.play()
      player.state = STATES.RECOVERING // hack, should be using nextState but...
      if(player.shield > 0) {
        player.shield -= dmg
        if(player.shield < 0) {
          dmg = -player.shield
          player.shield = 0
        } else {
          dmg = 0
        }
      }
      player.addHealth(-dmg)
      player.getHitTime = g.now
      player.body.velocity.x = Math.sign(player.x - x) * 50
      player.body.velocity.y = -100      
    }

    player.calcAnimation = function() {
      let anim = "stand"
      if(g.now < player.firedTime + FIRED_SHOOT_ANIM_TIME_MS) {
        anim = "stand-fire"
      } else if(g.now < player.firedTime + FIRED_STAND_ANIM_TIME_MS) {
        anim = "stand-postfire"
      } else {
        anim = "stand"
      }
      let speed = 1.0
      if([STATES.RUNNING].includes(player.state)) {
        if(player.body.velocity.x != 0) {
          if(g.now < player.firedTime + FIRED_RUN_ANIM_TIME_MS) {
            anim = "run-fire"
          } else {
            anim = "run"
          }
          speed = Math.max(Math.abs(player.body.velocity.x) / 6.0, 6)
        }
      }
      else if ([STATES.SLIDING].includes(player.state)) {
        anim = "slide"
      }
      else if([STATES.JUMPING, STATES.EARLYJUMP].includes(player.state)) {
        /*
        if(Math.abs(player.body.velocity.x) > 50) {
          anim="jump1"
        } else {
          anim="jump3"
        }
        */
        if(player.body.velocity.y < -100) {
          anim="jump1"
        } else if (player.body.velocity.y < 100) {
          anim="jump2"
        } else {
          anim="jump3"
        }
        if(player.buttons.down) {
          anim += "-fire-down"
        }          
        else if(g.now < player.firedTime + FIRED_RUN_ANIM_TIME_MS) {
          anim += "-fire"
          /*if(Math.abs(Math.PI / 2 - player.fireAngle) < 0.1) {
            anim += "-down"
          } */         
        }
      
      }
      else if ([STATES.ROLLING].includes(player.state)) {
        anim = "roll"
        speed = 1000 / player.getRollDuration() * 9
      }
      else if ([STATES.ROLLSTUN].includes(player.state)) {
        anim = "rollstun"
        speed = 1
      }      
      else if([STATES.AIRDODGING].includes(player.state)) {
        if(g.now < player.airdodgeTime + 200) {
          anim = "airdodge-start"
          speed = 12
        } else {
          anim = "airdodge-roll"
          speed = 12
        }
      }


      
      if(player.currentWeapon.playerAnimation) {
        anim = player.currentWeapon.playerAnimation
        speed = player.currentWeapon.playerAnimationSpeed
      }
      
      if(player.justStartedLevel) {
        anim = "shadow"
      }
      
      if(player.animations.currentAnim.name == anim) {
        player.animations.currentAnim.speed = speed
      } else {
        player.animations.play(anim, speed)
      }      

      let gunpt = GUN_POINTS[player.animations.frame]
      player.currentWeapon.x = gunpt.x - 0.5
      player.currentWeapon.y = gunpt.y - 0.5
      player.currentWeapon.scale.x = gunpt.flipX ? -1 : 1
      player.currentWeapon.scale.y = gunpt.flipY ? -1 : 1
      player.currentWeapon.angle = gunpt.angle
    }

    player.render = function() {
      /*
      let ax = Math.cos(player.fireAngle) * 20
      let ay = -Math.sin(player.fireAngle) * 20
      g.debug.geom( new Phaser.Line(player.x, player.y, player.x + ax, player.y - ay), 'rgba(255,0,0,1)' )
      */
      /*
      if(this.debugTiles) {
        this.debugTiles.forEach(function(t) {
          g.debug.geom(new Phaser.Rectangle(t.worldX-1, t.worldY-1, 18, 18, 'rgba(0,255,0,0.5)'))
        })
      }
      */
    }

    return player
  }

  return ctor
})()
