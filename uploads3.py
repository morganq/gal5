import boto
from boto.s3.key import Key
import secrets
import os

# GROSS 
# https://github.com/boto/boto/issues/2836
import ssl

_old_match_hostname = ssl.match_hostname

def _new_match_hostname(cert, hostname):
   if hostname.endswith('.s3.amazonaws.com'):
      pos = hostname.find('.s3.amazonaws.com')
      hostname = hostname[:pos].replace('.', '') + hostname[pos:]
   return _old_match_hostname(cert, hostname)

ssl.match_hostname = _new_match_hostname


c = boto.connect_s3(secrets.AWS_ACCESS_KEY_ID, secrets.AWS_SECRET_ACCESS_KEY)

def upload_file_to_s3(bucket, filename, remote_folder=None):
    print "uploading %s to %s/%s" %(filename, bucket, remote_folder)
    b = c.get_bucket(bucket)
    k = Key(b)
    remote_file_name = filename.split('/')[-1]
    if remote_folder:
        remote_file_name = remote_folder + '/' + remote_file_name
    k.key = remote_file_name
    k.set_contents_from_filename(filename)

dirs = {
  '/':['.js', '.html'],
  '/audio/':['.wav'],
  '/bosses/':['.js'],
  '/fonts/':['.fnt','.png'],
  '/images/':['.png'],
  '/images/effects/':['.png'],
  '/images/stickers/':['.png'],
  '/images/weapons/':['.png'],
  '/level1/':['.json']
}

root = os.path.dirname(os.path.abspath(__file__))
for path,valid in dirs.items():
  for fname in os.listdir(root + path):
    for ending in valid:
      if fname.endswith(ending):
        upload_file_to_s3('morganquirk.com', root + path + fname, 'gal5' + path)
