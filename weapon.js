const allWeapons = []
const Weapon = (function() {
  
  let module = {}
  
  let Base = function(name, bigImage, bigImageAlpha, bigImageBackground, inventoryImage, spriteImage, mods, stats, instWrapper) {
    let weap = {}
    
    weap.name = name
    weap.bigImage = bigImage
    weap.bigImageAlpha = bigImageAlpha
    weap.bigImageBackground = bigImageBackground
    weap.currentImage = new Phaser.BitmapData(g, 'something', GAME_WIDTH, GAME_HEIGHT)
    weap.currentImage.copy(weap.bigImage)
    weap.currentImage.update()
    weap.inventoryImage = inventoryImage
    weap.spriteImage = spriteImage
    weap.stickersPasted = []
    weap.stickerPixels = []
    weap.bulletUpdate = null
    
    let imageData = new Phaser.BitmapData(g, null, GAME_WIDTH, GAME_HEIGHT)
    imageData.copy(weap.bigImage)
    imageData.update()
    
    let totalNull = 0
    for(let y = 0; y < GAME_HEIGHT; y++) {
      let row = []
      for(let x = 0; x < GAME_WIDTH; x++) {
        let cell = null
        if(imageData.getPixel(x, y).a == 255) {
          cell = {}
        } else {
          totalNull += 1
        }
        row.push(cell)
      }
      weap.stickerPixels.push(row)
    }    
    
    weap.mods = mods
    weap.stats = stats
    
    weap.instantiateSprite = function(g, state) {
      let self = new Phaser.Sprite(g, 0, 0, weap.spriteImage, 0)
      self.anchor = new Phaser.Point(0.5, 0.5)      
      self.nextFireTime = g.now
      self.canFire = false
      self.canAutoFire = false
      
      self.playerAnimation = null
      self.playerAnimationSpeed = null
      self.playerSlowdownTime = 0
      self.playerSlowdown = 0
      
      self.getReloadTime = function() {
        return weap.stats.RELOAD_TIME_MS / g.pct(g.getStat('reload'))
      }
      
      self.getBulletSpeed = function() {
        return weap.stats.BULLET_SPEED
      }
      
      self.getAimSpread = function() {
        return weap.stats.AIM_SPREAD / g.pct(g.getStat('aim'))
      }
      
      self.getShotgunSpread = function() {
        return weap.stats.SHOTGUN_SPREAD / g.pct(g.getStat('aim') / 2)
      }
      
      self.getNumBullets = function() {
        let base = weap.stats.NUM_BULLETS
        let mod = g.pct(g.getStat('bullets'))
        let intBullets = Math.floor(base * mod)
        let oddsOfExtra = Math.floor(base * mod * 100) % 100
        value = intBullets
        if(g.rnd.between(1, 100) <= oddsOfExtra) {
          value += 1
        }
        return value
      }
      
      self.getBulletProperties = function() {
        let totalCrit = g.getStat('critchance') + state.player.bonusCritChance
        let isCrit = g.discrete(totalCrit) > 0
        let damage = weap.stats.DAMAGE * g.pct(g.getStat('damage'))
        if(isCrit) {
          damage *= 1 + g.pct(totalCrit)
        }
        return {
          damage:damage,
          critical:isCrit,
          homing:g.getStat('aim') / 4,
          vampire:g.getStat('vampire'),
          piercing:g.getStat('piercing'),
          fires:g.discrete(g.getStat('fire')),
          lightning:g.discrete(g.getStat('lightning')),
          backstab:g.getStat('backstab'),
          melee:g.getStat('melee'),
          sniper:g.getStat('sniper'),
          graphic:weap.stats.BULLET_GRAPHIC,
          graphicFrames:isCrit ? weap.stats.CRIT_FRAMES : weap.stats.BULLET_FRAMES,
          color:weap.stats.MUZZLE_COLOR,
          gravity:!!weap.stats.GRAVITY,
          lifetime:weap.stats.BULLET_LIFETIME || 1000,
          radius:weap.stats.BULLET_RADIUS || 3,
          closecombat:!!weap.stats.CLOSECOMBAT,
          explosionRadius:weap.stats.EXPLOSION_RADIUS || null,
          weight:weap.stats.WEIGHT || 1
        }
      }
      
      self.customUpdate = function() {
        
      }
      
      self.update = function() {
        if(g.now >= self.nextFireTime) {
          self.canFire = true
          self.canAutoFire = true
        } else {
          self.canFire = false
          self.canAutoFire = false
        }
        if(g.now >= self.playerSlowdownTime) {
          self.playerSlowdown = 0
        }
        self.customUpdate()
      }
      
      self.tryFire = function() {
        if(self.canFire) { self.fire() }
      }
      
      self.fire = function() {
        self.nextFireTime = g.now + self.getReloadTime()
        self.customFire()
        let ax = Math.cos(self.fireAngle)
        let ay = Math.sin(self.fireAngle)        
        state.bulletShake(-ax,-ay)
        state.player.recoil(ax, ay)   
        let slowdown = (weap.stats.PLAYER_SLOWDOWN || 0) / g.pct(g.getStat('speed'))
        self.playerSlowdown =  slowdown
        let slowdownTime = weap.stats.PLAYER_SLOWDOWN_TIME / g.pct(g.getStat('speed'))
        self.playerSlowdownTime = g.now + slowdownTime
      }
      
      self.customFire = function() {
        self.ejectBullet(self.fireAngle + g.rnd.normal() * self.getAimSpread())
        state.soundLaser.play()
      }
      
      self.customBulletProperties = function(props) {
        return props
      }
      
      self.ejectBullet = function(angle) {
        let ax = Math.cos(angle)
        let ay = Math.sin(angle)
        let bullet = Bullet(
          state,
          self.customBulletProperties(self.getBulletProperties()),
          self.x + self.parent.x + ax*11 - 3,
          self.y + self.parent.y + ay*11,
          ax * self.getBulletSpeed(),
          ay * self.getBulletSpeed())
        state.bulletLayer.add(bullet)
        if(weap.bulletUpdate) {
          bullet.customUpdate = function() { weap.bulletUpdate(bullet) }
        }
        if(!weap.stats.NO_MUZZLE_FLASH) {
          let flash = new Phaser.Sprite(g,
            self.x + self.parent.x + ax*7,
            self.y + self.parent.y + ay*7,
            "muzzleflash", g.rnd.between(1,4))
          flash.anchor.set(0.5, 0.5)
          flash.rotation = angle
          flash.tint = weap.stats.MUZZLE_COLOR
          state.effectLayer.add(flash)
          fireTime = g.now
          flash.update = function() {
            flash.x = self.world.x + ax*7
            flash.y = self.world.y + ay*7
            if(g.now > fireTime + 100) {
              flash.destroy()
            }
          }
        }
      }
      
      if(instWrapper) {
        self = instWrapper(weap, self, state)
      }
      
      return self
    }
    
    weap.pasteSticker = function(sticker,stickerBD,zx,zy,mutate) {
      zx = Math.floor(zx)
      zy = Math.floor(zy)
      // copy existing pixel data
      let tempPixelData = []
      for(let y = 0; y < weap.stickerPixels.length; y++) {
        let row = []
        for(let x = 0; x < weap.stickerPixels[y].length; x++) {
          row.push(weap.stickerPixels[y][x])
        }
        tempPixelData.push(row)
      }

      let totalPixels = 0
      for(let y = 0; y < stickerBD.height; y++) {
        for(let x = 0; x < stickerBD.width; x++) {
          if(stickerBD.getPixel(x, y).a > 0) { totalPixels += 1 }
        }
      }

      let validPixels = 0
      let replaced = 0
      // now modify the overlapping ones
      for(let y = 0; y < stickerBD.height; y++) {
        for(let x = 0; x < stickerBD.width; x++) {
          let bdx = Math.floor(x)
          let bdy = Math.floor(y)
          let cwx = Math.floor(x + (zx))
          let cwy = Math.floor(y + (zy))
          if(cwx >= 0 && cwx < GAME_WIDTH && cwy >= 0 && cwy < GAME_HEIGHT) {
            validPixels += 1
            let pd = stickerBD.getPixel(bdx, bdy)
            if(pd.a > 0 && tempPixelData[cwy][cwx] != null) {
              tempPixelData[cwy][cwx] = {
                id:sticker.id,
                stats:sticker.stats,
                totalPixels:totalPixels
              }
              replaced += 1
            }
          }
        }
      }
      if(mutate) { console.log("total pixels on sticker", totalPixels) }
      if(mutate) { console.log("valid pixels", validPixels)}
      if(mutate) { console.log("replaced pixels", replaced)}
      if(mutate) {
        weap.currentImage.alphaMask(stickerBD, weap.currentImage,
          new Phaser.Rectangle(
            zx,
            zy,
            stickerBD.width, stickerBD.height
          ), new Phaser.Rectangle(0, 0, GAME_WIDTH, GAME_HEIGHT))
        weap.currentImage.update()
      }

      return tempPixelData
    }    
    
    weap.getData = function() {
      return {
        name:weap.name,
        stickers:weap.stickersPasted
      }
    }    
    
    return weap
  }
  
  allWeapons.push(function(mods) {
    return Base("Rifle", "w-testweap-big", "w-testweap-big-alpha", "w-testweap-big-back", "w-testweap-inv", "w-testweap-spr", mods, {
      DAMAGE:7,
      RELOAD_TIME_MS:140,
      AIM_SPREAD:0.035,
      BULLET_GRAPHIC:"bullet",
      BULLET_FRAMES:[0],
      CRIT_FRAMES:[1],      
      BULLET_SPEED:270,
      MUZZLE_COLOR:0xd01f02,
    },
    function(weap, self, state) {
      
      return self
    })
  })
  
  allWeapons.push(function(mods) {
    return Base("Zapper", "w-testweap-big", "w-testweap-big-alpha", "w-testweap-big-back", "w-testweap-inv", "w-testweap-spr", mods, {
      DAMAGE:7,
      RELOAD_TIME_MS:190,
      AIM_SPREAD:0.035,
      BULLET_GRAPHIC:"spark",
      BULLET_FRAMES:[0],
      CRIT_FRAMES:[1],      
      BULLET_SPEED:800, // here's the issue... want it fast and basically act as hitscan
      MUZZLE_COLOR:0xd01f02,
    },
    function(weap, self, state) {
      // have to do something to make this work.
      return self
    })
  })  
  
  allWeapons.push(function(mods) {
    return Base("Rocket Launcher", "w-testweap-big", "w-testweap-big-alpha", "w-testweap-big-back", "w-testweap-inv", "w-testweap-spr", mods, {
      DAMAGE:20,
      RELOAD_TIME_MS:800,
      AIM_SPREAD:0.035,
      BULLET_GRAPHIC:"spark",
      BULLET_FRAMES:[0],
      CRIT_FRAMES:[1],      
      BULLET_SPEED:200, // here's the issue... want it fast and basically act as hitscan
      MUZZLE_COLOR:0xd01f02,
      EXPLOSION_RADIUS:50
    },
    function(weap, self, state) {
      // have to do something to make this work.
      return self
    })
  })    
  
  allWeapons.push(function(mods) {
    return Base("Revolver", "w-revolver-big", "w-revolver-big-alpha", "w-revolver-big-back", "w-revolver-inv", "w-revolver-spr", mods, {
      DAMAGE:10,
      RELOAD_TIME_MS:340,
      AIM_SPREAD:0.035,
      BULLET_GRAPHIC:"bullet",
      BULLET_FRAMES:[0],
      CRIT_FRAMES:[1],      
      BULLET_SPEED:300,
      MUZZLE_COLOR:0xd01f02,
      WEIGHT: 1
    },
    function(weap, self, state) {
      
      self.getMaxBullets = function() {
        return Math.floor(6 * g.pct(g.getStat('bullets')))
      }
      
      self.bulletsLeft = self.getMaxBullets()
      self.bulletLoadTime = 0
      
      /*self.bulletDisplay = new Phaser.Sprite(g, 0, 0, "revolver-display", 0)
      
      state.UILayer.add(self.bulletDisplay)
      self.bulletDisplay.fixedToCamera = true
      self.bulletDisplay.cameraOffset.x = GAME_WIDTH / 2
      self.bulletDisplay.cameraOffset.y = GAME_HEIGHT / 2 - 16
      */
      
      self.customUpdate = function() {
        self.nextFireTime = g.now + 9999999
        if(self.bulletsLeft < self.getMaxBullets()) {
          if(g.now > self.bulletLoadTime) {
            self.canAutoFire = true
            self.bulletsLeft += 1
            self.bulletLoadTime = g.now + self.getReloadTime()
          }
        }
        if(self.bulletsLeft > 0) {
          self.canFire = true
        }
        
        //state.player.currentWeapon.frame = 6 - self.bulletsLeft
      }
      
      self.customFire = function() {
        self.ejectBullet(self.fireAngle + g.rnd.normal() * self.getAimSpread())
        state.soundLaser.play()
        self.bulletsLeft -= 1
        self.canFire = false
        self.canAutoFire = false
        self.bulletLoadTime = g.now + self.getReloadTime()
      }
      
      return self
    })
  })
  
  const plasmaGun = function(weap, self, state) {
    self.bulletsLeft = 0
    self.customUpdate = function() {
      if(self.bulletsLeft > 0 && g.now >= self.nextRefireTime) {
        self.ejectBullet(self.fireAngle + g.rnd.normal() * self.getAimSpread())
        self.nextRefireTime = g.now + weap.stats.REFIRE_TIME_MS
        self.bulletsLeft -= 1
      }
    }
    self.customFire = function() {
      self.bulletsLeft = self.getNumBullets()
      self.nextRefireTime = g.now
      state.soundLaser.play()
    }
    
    return self
  }
  
  allWeapons.push(function(mods) {
    return Base("Plasma Emitter", "w-testweap-big", "w-testweap-big-alpha", "w-testweap-big-back", "w-testweap-inv", "w-testweap-spr", mods, {
      DAMAGE:6,
      RELOAD_TIME_MS:1000,
      REFIRE_TIME_MS:20,
      AIM_SPREAD:0.3,
      BULLET_GRAPHIC:"plasma",
      BULLET_FRAMES:[0],
      CRIT_FRAMES:[1],
      BULLET_SPEED:220,
      NUM_BULLETS:7,
      MUZZLE_COLOR:0xbb499a,
      PLAYER_SLOWDOWN:0.5,
      PLAYER_SLOWDOWN_TIME:250,
    },
    plasmaGun)
  })  
  
  allWeapons.push(function(mods) {
    return Base("Plasma Mark III", "w-testweap-big", "w-testweap-big-alpha", "w-testweap-big-back", "w-testweap-inv", "w-testweap-spr", mods, {
      DAMAGE:13,
      RELOAD_TIME_MS:1000,
      REFIRE_TIME_MS:75,
      AIM_SPREAD:0.065,
      BULLET_GRAPHIC:"plasma",
      BULLET_FRAMES:[0],
      CRIT_FRAMES:[1],
      BULLET_SPEED:320,
      NUM_BULLETS:3,
      MUZZLE_COLOR:0xbb499a,
      PLAYER_SLOWDOWN:0.5,
      PLAYER_SLOWDOWN_TIME:250,
    },
    plasmaGun)
  })
  
  allWeapons.push(function(mods) {
    return Base("Plasma Mark II", "w-testweap-big", "w-testweap-big-alpha", "w-testweap-big-back", "w-testweap-inv", "w-testweap-spr", mods, {
      DAMAGE:13,
      RELOAD_TIME_MS:800,
      REFIRE_TIME_MS:85,
      AIM_SPREAD:0.085,
      BULLET_GRAPHIC:"plasma",
      BULLET_FRAMES:[0],
      CRIT_FRAMES:[1],
      BULLET_SPEED:300,
      NUM_BULLETS:2,
      MUZZLE_COLOR:0xbb499a,
    },
    plasmaGun)
  })  
  
  allWeapons.push(function(mods) {
    return Base("Flak Cannon", "w-testweap-big", "w-testweap-big-alpha", "w-testweap-big-back", "w-testweap-inv", "w-testweap-spr", mods, {
      DAMAGE:10,
      RELOAD_TIME_MS:1000,
      AIM_SPREAD:0.03,
      SHOTGUN_SPREAD:0.5,
      BULLET_GRAPHIC:"flak",
      BULLET_FRAMES:[0,1,2],
      CRIT_FRAMES:[3,4,5],
      BULLET_SPEED:280,
      NUM_BULLETS:5,
      MUZZLE_COLOR:0xdb8600,
    },
    
    function(weap, self, state) {
      self.getBulletSpeed = function() {
        return g.rnd.between(weap.stats.BULLET_SPEED * 0.8, weap.stats.BULLET_SPEED * 1.2)
      }
      
      self.customFire = function() {
        let nb = self.getNumBullets()
        let ss = self.getShotgunSpread()
        for(let i = 0; i < nb; i++) {
          let angleOffset = (ss / (nb-1)) * i - ss / 2
          self.ejectBullet(self.fireAngle + angleOffset + g.rnd.normal() * self.getAimSpread())  
        }
        state.soundLaser.play()
      }
      
      return self
    })
  })
  
  allWeapons.push(function(mods) {
    return Base("Railgun","w-testweap-big", "w-testweap-big-alpha", "w-testweap-big-back", "w-testweap-inv", "w-testweap-spr", mods, {
      DAMAGE:25,
      RELOAD_TIME_MS:650,
      AIM_SPREAD:0.015,
      BULLET_GRAPHIC:"flak",
      BULLET_FRAMES:[0,1,2],
      CRIT_FRAMES:[3,4,5],
      BULLET_SPEED:500,
      MUZZLE_COLOR:0xd01f02,
      PLAYER_SLOWDOWN:1,
      PLAYER_SLOWDOWN_TIME: 250
    },
    function(weap, self, state) {
      weap.bulletUpdate = function(bullet) {
        if(!bullet.railTrailA) { bullet.railTrailA = 2}
        bullet.railTrailA += g.elapsed / 20
        let xv = Math.abs(bullet.body.velocity.y / 150)
        let yv = Math.abs(bullet.body.velocity.x / 150)
        let rtx = Math.cos(bullet.railTrailA) * xv
        let rty = Math.sin(bullet.railTrailA) * yv
        let frames = [0,1,2,2,3,4]
        let gfx = new Phaser.Sprite(g, bullet.x + rtx - 2.5, bullet.y + rty - 2.5, 'effect-rail')
        state.effectLayer.addChild(gfx)
        gfx.animations.add('rail', frames)
       
        gfx.play('rail', 25, false, true)
      }
      return self
    })
  })
  
  allWeapons.push(function(mods) {
    return Base("Phaser", "w-phaser-big", "w-phaser-big-alpha", "w-phaser-big-back", "w-phaser-inv", "w-phaser-spr", mods, {
      DAMAGE:20,
      RELOAD_TIME_MS:350,
      AIM_SPREAD:0.035,
      BULLET_GRAPHIC:"phaser",
      BULLET_FRAMES:[0],
      CRIT_FRAMES:[1],
      BULLET_SPEED:80,
      MAX_BULLET_SPEED:500,
      MUZZLE_COLOR:0x005ede,
      BULLET_LIFETIME:2000,
    },
    function(weap, self, state) {
      
      weap.bulletUpdate = function(bullet) {
        let xv = bullet.body.velocity.x
        let yv = bullet.body.velocity.y
        if(xv*xv + yv*yv < weap.stats.MAX_BULLET_SPEED ** 2) {
          bullet.body.velocity.x *= 1 + g.elapsed / 300
          bullet.body.velocity.y *= 1 + g.elapsed / 300
        }
      }
      
      return self
    })
  })
  
  allWeapons.push(function(mods) {
    return Base("Long Bow", "w-bow-big", "w-bow-big-alpha", "w-bow-big-back", "w-bow-inv", "w-bow-spr", mods, {
      DAMAGE:30,
      RELOAD_TIME_MS:500,
      AIM_SPREAD:0.015,
      BULLET_GRAPHIC:"arrow",
      BULLET_FRAMES:[0],
      CRIT_FRAMES:[1],
      BULLET_SPEED:350,
      MUZZLE_COLOR:0xd01f02,
      GRAVITY:true,
      BULLET_LIFETIME:3000,
    },
    function(weap, self, state) {
      
      self.chargeTime = 0
      self.nextChargeStartTime = 0
      self.heldThisFrame = false
      
      self.tryFire = function() {
        if(g.now >= self.nextChargeStartTime) {
          self.chargeTime += g.elapsed
          self.heldThisFrame = true
        }
      }
      
      self.getBulletSpeed = function() {
        return weap.stats.BULLET_SPEED * g.math.clamp(self.chargeTime/500, 0.1, 1)
      }
      
      self.customUpdate = function() {
        self.canFire = false
        if(self.chargeTime > 0 && !self.heldThisFrame ) {
          self.fire()
          self.nextChargeStartTime = g.now + self.getReloadTime()
          self.chargeTime = 0
        }
        self.heldThisFrame = false
      }      
      
      return self
      })
    })
    
    allWeapons.push(function(mods) {
      return Base("Laser Axe", "w-axe-big", "w-axe-big-alpha", "w-axe-big-back", "w-axe-inv", "w-axe-spr", mods, {
        DAMAGE:30,
        RELOAD_TIME_MS:400,
        AIM_SPREAD:0,
        BULLET_GRAPHIC:"axe-particle",
        BULLET_FRAMES:[0],
        CRIT_FRAMES:[1],
        BULLET_SPEED:50,
        MUZZLE_COLOR:0xbb499a,
        NO_MUZZLE_FLASH:true,
        GRAVITY:false,
        BULLET_LIFETIME:50,
        BULLET_RADIUS:9,
        CLOSECOMBAT:true,
        PLAYER_SLOWDOWN:0.75,
        PLAYER_SLOWDOWN_TIME:333,
        WEIGHT:30,
      },
      function(weap, self, state) {
        self.animations.add("attack", [1,2,3,4], 16, false)
        self.animations.add("base", [2], 8, false)
        self.animations.play("base")
        
        self.attackEndTime = 0
        self.createdProjectile = false
        
        self.customUpdate = function() {
          if(g.now >= self.attackEndTime) {
            self.play("base")
            self.playerAnimation = null
            self.createdProjectile = false
          } else {
            if(!self.createdProjectile && g.now >= self.attackEndTime - 200) {
              self.ejectBullet(self.fireAngle + g.rnd.normal() * self.getAimSpread())
              self.createdProjectile = true
            }            
          }
        }
        
        self.customFire = function() {
          
          self.play("attack")
          self.attackEndTime = g.now + 333
          self.playerAnimation = "axe-attack"
          self.playerAnimationSpeed = 16
        }
        
        return self
      })    
  })
  
  //allWeapons.push(function(mods) {
  var lance = (function(mods) {
    return Base("Lance", "w-lance-big", "w-lance-big-alpha", "w-lance-big-back", "w-lance-inv", "w-lance-spr", mods, {
      DAMAGE:30,
      RELOAD_TIME_MS:600,
      AIM_SPREAD:0,
      BULLET_GRAPHIC:"lance-bullet",
      BULLET_FRAMES:[0],
      CRIT_FRAMES:[0],
      BULLET_SPEED:200,
      MUZZLE_COLOR:0xd01f02,
      NO_MUZZLE_FLASH:true,
      GRAVITY:false,
      BULLET_LIFETIME:70,
      BULLET_RADIUS:4,
      CLOSECOMBAT:true,
      PLAYER_SLOWDOWN:0.75,
      PLAYER_SLOWDOWN_TIME:333,      
    },
    function(weap, self, state) {
      self.animations.add("attack", [0,1,2], 12, false)
      self.animations.add("base", [0], 8, false)
      self.animations.play("base")
      
      self.attackEndTime = 0
      self.createdProjectile = false
      
      self.customUpdate = function() {
        if(g.now >= self.attackEndTime) {
          self.play("base")
          self.playerAnimation = null
          self.createdProjectile = false
        } else {
          if(!self.createdProjectile && g.now >= self.attackEndTime - 150) {
            self.ejectBullet(self.fireAngle + g.rnd.normal() * self.getAimSpread())
            self.createdProjectile = true
          }            
        }
      }
      
      self.customFire = function() {
        
        self.play("attack")
        self.attackEndTime = g.now + 333
        self.playerAnimation = "axe-attack"
        self.playerAnimationSpeed = 16
      }
      
      return self
    })    
})  
  
  return module
})()
