const MenuBase = (function() {
  const STATES = {
    CLOSED:'closed',
    OPENING:'opening',
    OPEN:'open',
    CLOSING:'closing',
    CHILDACTIVE:'childactive'
  }
  function ctor(state, color, x, y, width, height) {
    let self = new Phaser.Graphics(g, width, height)
    self.beginFill(color, 1)
    self.targetX = x
    self.targetY = y
    self.drawRect(0, 0, width, height)
    self.endFill()
    
    self.visible = false
    self.openDirection = "down"
    self.openSpeed = 2000 // pixels per second
    self.state = STATES.CLOSED
    
    self.parentMenu = null

    self.open = function() {
      if(!self.parentMenu) { 
        state.inMenu = true
      }
      switch(self.openDirection) {
        case "right":
          self.x = -self.width
          self.y = self.targetY
          break
        case "left":
          self.x = GAME_WIDTH
          self.y = self.targetY
          break
        case "up":
          self.x = self.targetX
          self.y = GAME_HEIGHT
          break
        case "down":
          self.x = self.targetX
          self.y = -self.height
          break
        default:
          self.x = self.targetX
          self.y = self.targetY
      }
      
      self.visible = true
      self.state = STATES.OPENING
      self.onOpen()
    }
    
    self.isOpen = function() { 
      return self.state == STATES.OPEN
    }
    
    self.isClosed = function() {
      return self.state == STATES.CLOSED
    }
    
    self.openChildMenu = function(childMenu) {
      self.childMenu = childMenu
      self.childMenu.parentMenu = self
      self.addChild(self.childMenu)
      
      self.state = STATES.CHILDACTIVE
      self.childMenu.open()
    }
    
    self.childMenuClosed = function() {
      self.state = STATES.OPEN
    }
    
    self.close = function() {
      self.state = STATES.CLOSING
      self.onClose()
    }

    self.openUpdate = function() {
      
    }
    
    self.closedUpdate = function() {
      
    }
    
    self.onClose = function() {
      
    }
    
    self.onOpen = function() {}

    self.update = function() {
      let nextState = null
      // Don't do anything if we're closed or if a child window is open
      if(self.state == STATES.CHILDACTIVE) {
        self.childMenu.update()
        return
      }
      if(self.state == STATES.CLOSED) {
        self.closedUpdate()
        return
      }
      
      // Do opening logic
      if(self.state == STATES.OPENING) {
        if(self.x < self.targetX) {
          self.x = Math.min(self.x + g.elapsed / 1000 * self.openSpeed, self.targetX)
        }
        else if(self.x > self.targetX) {
          self.x = Math.max(self.x - g.elapsed / 1000 * self.openSpeed, self.targetX)
        }
        if(self.y < self.targetY) {
          self.y = Math.min(self.y + g.elapsed / 1000 * self.openSpeed, self.targetY)
        }
        else if(self.y > self.targetY) {
          self.y = Math.max(self.y + g.elapsed / 1000 * self.openSpeed, self.targetY)
        }    
        if(self.x == self.targetX && self.y == self.targetY) {
          nextState = STATES.OPEN
        }
      }
      
      // Do closing logic 
      if(self.state == STATES.CLOSING) {
        switch(self.openDirection) {
          case "right":
            self.x -= g.elapsed / 1000 * self.openSpeed
            break
          case "left":
            self.x += g.elapsed / 1000 * self.openSpeed
            break   
          case "up":
            self.y += g.elapsed / 1000 * self.openSpeed
            break 
          case "down":
            self.y -= g.elapsed / 1000 * self.openSpeed
            break     
          default:
            self.x = -self.width - 1
        }
        if((self.x + self.width < 0) ||
          (self.x > GAME_WIDTH) ||
          (self.y + self.height < 0) ||
          (self.y > GAME_HEIGHT)) {
            nextState = STATES.CLOSED
            if(!self.parentMenu) { 
              state.inMenu = false
            }            
            self.visible = false
            if(self.parentMenu) {
              self.parentMenu.childMenuClosed()
            }
          }
      }
      
      // Allow closing etc.
      if(self.state == STATES.OPEN) {
        self.openUpdate()
        if(g.backButton.downDuration(1)) {
          g.backButton.reset()
          self.onClose()
          self.close()
        }
        self.children.forEach(function(c) { c.update() })
      }
      
      if(nextState && nextState != self.state) {
        self.state = nextState
      }
    }

    return self
  }
  
  return ctor
  
})()
