const Meter = (function() {
  
  function ctor(x, y, widthPer, height, color, trackObj, trackPropertyName, trackMaxPropertyName) {
    let self = new Phaser.Graphics(g, x, y)
    self.width = GAME_WIDTH
    self.height = height
    
    self.updateBar = function() {
      self.value = trackObj[trackPropertyName]
      self.maxValue = trackObj[trackMaxPropertyName]
      let w = Math.floor(self.maxValue * widthPer) + 2
      self.currentWidth = w
      self.clear()
      self.beginFill(0x020628)
      self.drawRect(0, 0, w, height)
      self.endFill()
      self.beginFill(color)
      let x = Math.floor(self.value * (w-2) / self.maxValue)
      self.drawRect(1, 1, x, height-2)
      self.endFill()
      self.beginFill(0xffffff)
      self.drawRect(x+1, 1, w - 2 - x, height-2)
      self.endFill()
    }
    
    self.updateBar()
    
    self.update = function() {
      if(!trackObj) { return }
      if(trackObj[trackPropertyName] != self.value) {
        self.updateBar()
      }
      if(trackObj[trackMaxPropertyName] != self.maxValue) {
        self.updateBar()
      }      
    }
    
    return self
  }
  
  return ctor
  
})()
