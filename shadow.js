const Shadow = function(state, spr, width, xo, yo) {
  let self = new Phaser.Group(g)
  if(!xo) { self.xo = 0 } else { self.xo = xo }
  if(!yo) { self.yo = 0 } else { self.yo = yo }
  if(!width) { self.shadowWidth = spr.width }
  else { self.shadowWidth = width }
  self.numSprites = Math.ceil(self.shadowWidth / 8) + 1
  self.sprites = []
  for(var i = 0; i < self.numSprites; i++) {
    let s = new Phaser.Sprite(g, 0, 0, 'shadow', 0)
    //s.alpha = 0.5
    //s.tint = Phaser.Color.toRGBA(g.rnd.between(0,255), g.rnd.between(0,255), g.rnd.between(0,255), 255)
    self.addChild(s)
    self.sprites.push(s)
  }
  
  
  self.owner = spr
  state.shadowLayer.add(self)
  
  self.updateShadow = function() {
    let ww = (Math.abs(self.owner.width) - self.shadowWidth) / 2
    let left = Math.floor(Math.min(self.owner.left, self.owner.right) + ww) + self.xo * self.owner.scale.x
    let right = Math.floor(Math.max(self.owner.left, self.owner.right) - ww) + self.xo * self.owner.scale.x
    let width = right - left
    
    let lt = Math.floor(left / GS) * GS
    
    let x = 0
    
    let steps = []
    
    for(var i = 0; i < self.numSprites; i++) {
      let tx = (lt + i * GS) / GS
      let ty = Math.floor((self.owner.y) / GS)
      self.sprites[i].visible = false
      for(var y = ty; y < ty + 5; y++) {
        let tile = state.tilemap.getTile(tx, y, 'main', false)
        if(tile && tile.index >= 0) {
          self.sprites[i].y = tile.worldY-2 + self.yo
          let ti = tile.index - TILESET_INDICES['main']
          if(ti == 2 || ti == 9) {
            self.sprites[i].frame = 3
          }
          else if(ti == 3 || ti == 10 || ti == 13) {
            self.sprites[i].frame = 4
          }
          else if(ti == 4 || ti == 11 || ti == 14) {
            self.sprites[i].frame = 1
          }
          else if(ti == 5 || ti == 12) {
            self.sprites[i].frame = 2
          } else {
            self.sprites[i].frame = 0
          }      
          self.sprites[i].visible = true
          if(self.sprites[i].y > self.owner.y) {
            self.sprites[i].alpha = g.math.clamp(1 - ((self.sprites[i].y - (self.owner.bottom - 1)) / 20),0,0.8)
          } else {
            self.sprites[i].alpha = 0
            //self.sprites[i].alpha = 1 - (((self.owner.bottom - 1) - self.sprites[i].y) / 4)  
          }
          
          break
        }
      } 
      steps.push(x)
      if(i == 0) {
        let sx = left % GS
        self.sprites[i].x = lt + sx // +sx because crop affects the x...
        self.sprites[i].crop(new Phaser.Rectangle(sx, 0, 8-sx, 9), false)
        x += (8-sx)
      } else {
        self.sprites[i].x = lt + i * GS
        if(x + GS <= width) {
          self.sprites[i].crop(new Phaser.Rectangle(0, 0, 8, 9), false) // reset crop
          x += GS
        } else if (x >= width) {
          self.sprites[i].visible = false
          x += GS
        } else {
          let rest = width - x
          //console.log(steps, x, width, rest)
          self.sprites[i].crop(new Phaser.Rectangle(0, 0, rest, 9), false)
          x += rest
        }
        
      }
    }    
  }
  
  self.updateShadow()
  
  self.update = function() {
    if(!self.owner.exists || !self.owner.alive) { self.destroy(); return }
    
    self.updateShadow()
    //console.log(prints)
    
  }
  
  return self
}
