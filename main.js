g = null;

window.onload = function() {
  // Helps it display properly on phone
  //document.getElementById('game').style.width = (window.innerWidth) + "px";
  //document.getElementById('game').style.height = (window.innerHeight) + "px";
  //document.getElementById('game').style.width = 640
  console.log(document.getElementById('game').style);

  var game = new Phaser.Game(GAME_WIDTH, GAME_HEIGHT, Phaser.WEBGL, 'game', false, false);
  g = game;  
  
  game.saveWeapon = function(key, weapon) {
    game.savedWeapons[key] = weapon
    localStorage.setItem('savedWeapons', JSON.stringify(game.savedWeapons))
    console.log(localStorage.getItem('savedWeapons'))
  }
  
  game.baseAlliances = function() {
    return {
      'mosaic':{
        'bonus':{unlocked:true},
        'gun':{unlocked:true, enabled:true},
        'dodge':{unlocked:true, enabled:true},
        'augment':{unlocked:false, enabled:false}
      },
      'homeworld':{
        'bonus':{unlocked:false},
        'gun':{unlocked:true, enabled:false},
        'dodge':{unlocked:false, enabled:false},
        'augment':{unlocked:false, enabled:false}
      },
      'roth':{
        'bonus':{unlocked:true},
        'gun':{unlocked:true, enabled:false},
        'dodge':{unlocked:false, enabled:false},
        'augment':{unlocked:false, enabled:false}
      },
      'fragment':{
        'bonus':{unlocked:false},
        'gun':{unlocked:false, enabled:false},
        'dodge':{unlocked:false, enabled:false},
        'augment':{unlocked:false, enabled:false}
      },
      'yz':{
        'bonus':{unlocked:false},
        'gun':{unlocked:false, enabled:false},
        'dodge':{unlocked:false, enabled:false},
        'augment':{unlocked:false, enabled:false}
      },
      'seers':{
        'bonus':{unlocked:false},
        'gun':{unlocked:false, enabled:false},
        'dodge':{unlocked:false, enabled:false},
        'augment':{unlocked:false, enabled:false}
      },
    }
  }
  
  game.numTags = JSON.parse(localStorage.getItem('numTags')) || 1
  game.savedWeapons = JSON.parse(localStorage.getItem('savedWeapons')) || {}
  game.alliances = JSON.parse(localStorage.getItem('alliances')) || game.baseAlliances()  

  var BootState = {
    preload: function() {
      game.sound.volume=0
      game.stage.backgroundColor = '#f2f2e3';
      game.clearBeforeRender = false
      //game.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
      game.scale.scaleMode = Phaser.ScaleManager.USER_SCALE;
      game.scale.setUserScale(3, 3)
      game.stage.smoothed=false
      game.antialias=false
      game.renderer.renderSession.roundPixels = true
      Phaser.Canvas.setImageRenderingCrisp(game.canvas)      
      
      // Levels
      
      game.load.tilemap('level_lobby', 'level1/lobby.json', null, Phaser.Tilemap.TILED_JSON)
      game.load.tilemap('level_store', 'level1/store.json', null, Phaser.Tilemap.TILED_JSON)
      game.load.tilemap('level_registry', 'level1/registry.json', null, Phaser.Tilemap.TILED_JSON)
      game.load.tilemap('level_boss', 'level1/boss1.json', null, Phaser.Tilemap.TILED_JSON)      
      
      game.load.tilemap('level_a', 'level1/myfirstlevel.json', null, Phaser.Tilemap.TILED_JSON)
      game.load.tilemap('level_b', 'level1/elevator.json', null, Phaser.Tilemap.TILED_JSON)
      game.load.tilemap('level_c', 'level1/dome.json', null, Phaser.Tilemap.TILED_JSON)
      game.load.tilemap('level_d', 'level1/twolayers.json', null, Phaser.Tilemap.TILED_JSON)
      game.load.tilemap('level_e', 'level1/linkedboxes.json', null, Phaser.Tilemap.TILED_JSON)
      game.load.tilemap('level_f', 'level1/spikehallway.json', null, Phaser.Tilemap.TILED_JSON)
      game.load.tilemap('level_g', 'level1/tower.json', null, Phaser.Tilemap.TILED_JSON)
      game.load.tilemap('level_h', 'level1/openhall.json', null, Phaser.Tilemap.TILED_JSON)
      game.load.tilemap('level_i', 'level1/arena.json', null, Phaser.Tilemap.TILED_JSON)
      game.load.tilemap('level_j', 'level1/doublehall.json', null, Phaser.Tilemap.TILED_JSON)
      game.load.tilemap('level_k', 'level1/simplehall.json', null, Phaser.Tilemap.TILED_JSON)
      
      //game.load.tilemap('level_debug', 'level1/myfirstlevel_wallpaper.json', null, Phaser.Tilemap.TILED_JSON)
      
      // Tilesets      
      game.load.spritesheet('bgtiles', 'images/bgtiles.png', 8, 8);
      game.load.spritesheet('tiles', 'images/hoteltiles.png', 8, 8);
      game.load.spritesheet('walkerhints', 'images/walkerhints.png', 8, 8);
      game.load.spritesheet('wallpapertiles', 'images/wallpapertiles.png', 8, 8);
      
      // Basics
      game.load.image('parallax1', 'images/parallax1.png')
      game.load.image('parallax2', 'images/parallax2.png')
      game.load.image('parallax3', 'images/parallax3.png')
      game.load.image('parallax4', 'images/parallax4.png')      
      game.load.image('registry', 'images/registry.png')
      game.load.spritesheet('bossdoor', 'images/bossdoor.png', 18, 22)
      game.load.spritesheet('coffeemaker', 'images/coffeemaker.png', 18, 22)      
      game.load.spritesheet('door', 'images/door.png', 18, 22)
      game.load.spritesheet('fragment-lobby', 'images/lobby/fragment.png', 16, 16)
      game.load.spritesheet('hero', 'images/player-16.png', 16, 16)
      game.load.spritesheet('item-randomsticker', 'images/item-randomsticker.png', 16, 16)
      game.load.spritesheet('mosaic-lobby', 'images/lobby/mosaic.png', 18, 18)      
      game.load.spritesheet('seer-lobby', 'images/lobby/seer.png', 21, 25)
      game.load.spritesheet('homeworld-lobby', 'images/lobby/apexhomeworld.png', 30, 24)
      game.load.spritesheet('roth-lobby', 'images/lobby/roth.png', 30, 30)
      
      // Boss
      game.load.image('cageboss-chain', 'images/enemies/boss1chain.png')
      game.load.image('cageboss-flame', 'images/enemies/boss1-flame.png')
      game.load.image('cageboss-turret-base', 'images/enemies/boss1turretbase.png')
      game.load.image('cageboss-turret', 'images/enemies/boss1turret.png')
      game.load.image('cageboss-weaklink', 'images/enemies/boss1weaklink.png')
      game.load.spritesheet('cageboss-inside', 'images/enemies/boss1inside.png', 48, 29)
      game.load.spritesheet('cageboss', 'images/enemies/boss1cage.png', 74, 57)
      
      // Enemies
      game.load.image('crystalspike', 'images/enemies/charger-spike.png')
      game.load.image('enemyspinnereye', 'images/enemies/spinner-eye.png')
      game.load.spritesheet('enemycharger-spikes', 'images/enemies/charger-spikes.png',16,16)
      game.load.spritesheet('enemycharger', 'images/enemies/charger.png',23,23)
      game.load.spritesheet('enemyfly', 'images/enemies/fly.png', 10, 10)
      game.load.spritesheet('enemyrollyturret', 'images/enemies/turret.png',19,19)
      game.load.spritesheet('enemyspinner', 'images/enemies/spinner.png',16,16)
      game.load.spritesheet('enemywalker', 'images/enemies/walker.png', 16, 16)
      game.load.spritesheet('turretbullet', 'images/enemies/turret-bullet.png', 7, 7)
      
      // Effects
      game.load.image('effect-backstab', 'images/effects/backstab.png')
      game.load.image('effect-melee', 'images/effects/melee.png')
      game.load.image('effect-snipe', 'images/effects/snipe.png')
      game.load.image('health-pickup', 'images/effects/healthpickup.png')
      game.load.spritesheet('coin-pickup', 'images/effects/coinpickups.png',8,8)
      game.load.spritesheet('crystaldust', 'images/effects/crystaldust.png', 2, 2)
      game.load.spritesheet('effect-hit', 'images/effects/hit.png', 15, 15)
      game.load.spritesheet('effect-hitwall', 'images/effects/hitwall.png', 9, 9)
      game.load.spritesheet('effect-poof1', 'images/effects/poof1.png', 9, 9)
      game.load.spritesheet('effect-rail', 'images/effects/rail.png', 5, 5)           
      game.load.spritesheet('effect-spiketrail', 'images/effects/spiketrail.png', 5, 5) 
      game.load.spritesheet('flame', 'images/effects/flame.png', 8, 8)
      game.load.spritesheet('muzzleflash', 'images/effects/muzzleflash.png', 8, 8)
      game.load.spritesheet('shadow', 'images/effects/shadow.png', 8, 9)
      game.load.spritesheet('spark', 'images/effects/spark.png', 6, 6)     
      game.load.spritesheet('bonuseye', 'images/effects/bonuseye.png', 7, 7)
      
      // UI
      game.load.spritesheet('helpkeys', 'images/ui/helpkeys.png', 9, 9)
      game.load.spritesheet('mapicons', 'images/ui/mapicons.png', 8, 8)
      game.load.spritesheet('rarity', 'images/ui/rarity.png', 5, 5)
      game.load.spritesheet('policenumber', 'images/ui/policenumber.png', 11, 11)
      game.load.spritesheet('policemap', 'images/ui/policemap.png', 8, 8)
      game.load.spritesheet('map-arrow', 'images/ui/map-arrow.png', 8, 8)
      game.load.spritesheet('staticons', 'images/ui/staticons.png', 8, 8)
      game.load.spritesheet('tags-small', 'images/ui/tags-small.png', 16, 16);      
      game.load.spritesheet('tags', 'images/ui/tags.png', 16, 16);
      game.load.spritesheet('youarehere', 'images/ui/youarehere.png', 10, 10)      
      game.load.spritesheet('civflags', 'images/ui/civflags.png', 16, 16)
      game.load.spritesheet('achievementicons', 'images/ui/achievementicons.png', 16, 16)
      game.load.image('alliancemenu','images/ui/alliancemenu.png')
      
      // Projectiles
      game.load.spritesheet('arrow', 'images/weapons/arrow.png', 13, 13)
      game.load.spritesheet('axe-particle', 'images/weapons/axe-particle.png',10,17)
      game.load.spritesheet('bullet', 'images/weapons/bullet.png', 14, 14)
      game.load.spritesheet('lance-bullet', 'images/weapons/lance-bullet.png', 15, 15)
      game.load.spritesheet('flak', 'images/weapons/flak.png', 11, 11)
      game.load.spritesheet('phaser', 'images/weapons/phaser.png', 11, 11)
      game.load.spritesheet('plasma', 'images/weapons/plasma.png', 11, 11)
      
      // Audio
      game.load.audio('bonusmove', 'audio/bonusmove.wav')
      game.load.audio('dodge', 'audio/dodge.wav')
      game.load.audio('enemy-armor-bounce', 'audio/enemyarmorbounce.wav')
      game.load.audio('enemy-hit', 'audio/enemyhit.wav')
      game.load.audio('hurt', 'audio/hurt.wav')
      game.load.audio('jump', 'audio/jump.wav')
      game.load.audio('laser', 'audio/laser.wav')
      
      // Fonts
      game.load.bitmapFont('visitor', 'fonts/visitor.png', 'fonts/visitor.fnt');      

      // Stickers
      stickerSpecs.forEach(function(spec) {
        game.load.image('sticker-' + spec.sprite, 'images/stickers/' + spec.sprite + '.png')
      })
      
      // Weapons
      let weaponNames = ['revolver', 'testweap', 'bow', 'phaser', 'axe', 'lance']
      
      weaponNames.forEach(function(name) {
        game.load.image('w-' + name +'-spr', 'images/weapons/' + name +'-spr.png')
        game.load.image('w-' + name +'-inv', 'images/weapons/' + name +'-inv.png')
        game.load.image('w-' + name +'-big', 'images/weapons/' + name +'-base.png')
        game.load.image('w-' + name +'-big-alpha', 'images/weapons/' + name +'-alpha.png')
        game.load.image('w-' + name +'-big-back', 'images/weapons/' + name +'-back.png')         
      })      
      game.load.spritesheet('w-axe-spr', 'images/weapons/axe-spr.png', 15, 15)
      game.load.spritesheet('w-lance-spr', 'images/weapons/lance-spr.png', 15, 15)

    },

    create: function() {      
      allWeapons.forEach(function(w) {
        Weapon[w().name] = w
      })
      // Load the save file here
      game.save = {}
      
      game.stickers = [ ]      
      
      game.createStickerGraphic = function(spec) {
        let img = new Phaser.Image(g, 0, 0, 'sticker-' + spec.sprite)
        let sprite = new Phaser.BitmapData(g, null, img.width, img.height)
        sprite.draw(img)
        if(spec.sprite.indexOf("generic") >= 0) {
          let frame = STATS.indexOf(Object.keys(spec.stats)[0]) * 8
          sprite.copy(
            'staticons',
            frame, 0, 8, 8,
            Math.floor(sprite.width / 2 - 4),
            Math.floor(sprite.height / 2 - 4),
            8, 8
          )
          let rarities = {'common':0,'uncommon':1,'rare':2}
          frame = rarities[spec.rarity] * 5
          sprite.copy(
            'rarity',
            frame, 0, 5, 5,
            Math.floor(2),
            Math.floor(2),
            5, 5
          )
        }
        return sprite        
      }
      
      game.grantSticker = function(spec) {
        console.log(spec)
        let instSpec = Object.assign({}, spec)
        // Paste the stat icon onto the sticker... temp
        let newSpr = game.createStickerGraphic(spec)
        instSpec.sprite = newSpr
        game.stickers.push(instSpec)
        return instSpec
      }
      game.NEXT_STICKER_ID = 0
      game.getRandomSticker = function(level, rarity, strict) {
        if(!strict) {
          if(rarity == 'common') {
            if(g.rnd.between(1,9) == 1) {
              rarity = 'uncommon'
            }
          }
        }
        let validStickers = stickerSpecs.filter(function(spec) {
          return (spec.level == level && spec.rarity == rarity)
        })
        return game.getSticker(g.rnd.pick(validStickers))
      }
      game.getSticker = function(spec) {
        let sticker = Object.assign({}, spec, {
          id:game.NEXT_STICKER_ID
        })
        game.NEXT_STICKER_ID += 1
        return sticker        
      }
      
      //game.grantSticker(game.getSticker(stickerSpecs[stickerSpecs.length-1]))
      

      for(var i =0 ;i < 1; i++) {
        game.grantSticker(game.getRandomSticker(1, g.rnd.pick(['common']), false))
      }
      
      
      
      game.makeFillSticker = function(weap, fill) {
        let sticker = Object.assign({}, fill)
        console.log(weap, weap.bigImage)
        let imageData = new Phaser.BitmapData(g, null, GAME_WIDTH, GAME_HEIGHT)
        imageData.copy(weap.bigImage)
        imageData.update()        
        
        let bd = new Phaser.BitmapData(g, null, GAME_WIDTH, GAME_HEIGHT)
        console.log(fill)
        
        let num = 0
        for(let y = 0; y < imageData.height; y++) {
          for(let x = 0; x < imageData.width; x++) {
            if(imageData.getPixel(x, y).a == 255) {
              num += 1
              bd.setPixel(x, y, fill.color)
            }
          }
        }
        
        bd.update()
        sticker.sprite = bd
        sticker.rarity = 'common'
        return sticker
      }
      
      game.applyFill = function(weap, fill) {
        weap.stickerPixels = weap.pasteSticker(fill, fill.sprite, 0, 0, true)
        weap.stickersPasted.push({
          fill:fill.name,
          x:0,
          y:0
        })
        weap.modifier = fill.name
        
        return weap        
      }
      
      game.makeRandomWeapon = function() {
        let ctor = g.rnd.pick(Object.values(Weapon))
        let weap = ctor()
        
        // apply a random fill
        let fill = game.makeFillSticker(weap, g.rnd.pick(fillStickerSpecs))

        return game.applyFill(weap, fill)
      }
      
      game.weapons = [
        Weapon.Revolver(),
        Weapon['Laser Axe'](),
        null,//Weapon['Plasma Mark II'](),
        null,//Weapon['Plasma Mark III'](),
        null,//Weapon['Railgun'](),
      ]
      
      game.curWeapon = game.weapons[0]
      game.curWeaponIndex = 0
      game.setCurrentWeapon = function(num) {
        if(game.weapons[num-1]) {
          game.curWeapon = game.weapons[num-1]
          game.curWeaponIndex = num-1
          return true
        } else {
          return false
        }
      }
      
      game.getTagPrice = function(tagIndex) {
        let weap = game.savedWeapons[tagIndex]
        let price = 50 + weap.stickers.length * 5
        return price
      }
      
      game.loadWeapon = function(tagIndex, slotIndex) {
        if(g.credits < game.getTagPrice(tagIndex)) { return false }
        g.credits -= game.getTagPrice(tagIndex)
        let weap = game.savedWeapons[tagIndex]
        let w = Weapon[weap.name]()
        console.log(w)
        weap.stickers.forEach(function(sticker) {
          let spec = stickerSpecs.filter(function(s){return s.name == sticker.sticker})[0]
          if(spec) {
            console.log(sticker, spec)
            spec = game.getSticker(spec)
            let gfx = game.createStickerGraphic(spec)
            let bd = new Phaser.BitmapData(g, null, gfx.width, gfx.height)
            bd.draw(gfx, 0, 0, null, null, null, true)
            bd.update()
            w.stickerPixels = w.pasteSticker(spec, bd, sticker.x, sticker.y, true)            
          }
          else {
            fill = fillStickerSpecs.filter(function(s){return s.name == sticker.fill})[0]
            fillSticker = game.makeFillSticker(w, fill)
            w = game.applyFill(w, fillSticker)
          }

        })
        w.stickersPasted = weap.stickers
        game.weapons[slotIndex] = w
        return true
      }
      
      game.renderer.renderSession.roundPixels = true;

      game.creditsMultiplier = 1.0
      game.multiplierTimer = 0
      game.credits = 0

      game.stats = {}
      game.nextStats = {}
      game.getStat = function(name) {
        if(!STATS.includes(name)) { console.log("BAD STAT NAME? " + name) }
        if(name in game.stats) {
          return game.stats[name]
        }
        return 0
      }
      game.getTargetStat = function(name) {
        if(!STATS.includes(name)) { console.log("BAD STAT NAME? " + name) }
        if(name in game.nextStats) { return game.nextStats[name] }
        return 0        
      }
      game.updateStats = function() {
        STATS.forEach(function(sn) {
          let s = game.getStat(sn)
          let target = 0
          if(sn in game.nextStats) {
            target = game.nextStats[sn]
          }
          if(s < target) {
            s = Math.min(s + g.elapsed / 30, target)
          } else if (s > target) {
            s = Math.max(s - g.elapsed / 30, target)
          }
          game.stats[sn] = s          
        })
      }
      game.pct = function(x) { return (1 + x/100.0) }
      game.discrete = function(x) {
        let times = Math.floor(x / 100)
        let remainder = x % 100
        let result = times + ((g.rnd.between(1, 100) <= remainder) ? 1 : 0)
        return result
      }
      
      game.elapsed = 0
      game.now = 0
      
      
      g.menuButton = g.input.keyboard.addKey(Phaser.Keyboard.Q)
      g.useButton = g.input.keyboard.addKey(Phaser.Keyboard.A)
      g.dodgeButton = g.input.keyboard.addKey(Phaser.Keyboard.Z)
      g.backButton = g.dodgeButton
      g.jumpButton = g.input.keyboard.addKey(Phaser.Keyboard.X)
      g.extraButton = g.jumpButton
      g.fireButton = g.input.keyboard.addKey(Phaser.Keyboard.C)
      g.selectButton = g.fireButton
      g.cursors = g.input.keyboard.createCursorKeys()

      game.time.advancedTiming = true;
      console.log("Boot");
      
      game.dungeon = Dungeon(0)
      
      game.changeRooms = function(direction) {
        game.state.start('GameState', true, false, direction);
      }
      
      //game.dungeon.x = game.dungeon.boss.x
      //game.dungeon.y = game.dungeon.boss.y
      game.state.start('GameState', true, false, 'right');
      
      game.playerHealth = 100
      game.playerShield = 0

      game.now = g.time.now
      
      game.helpActionCount = {
        'jump':9999,
        'left':9999,
        'right':9999,
        'roll':9999,
        'shoot':9999
      }      
    }
  }
  
  if(window.location.hash == "#reset") {
    localStorage.setItem('savedWeapons', JSON.stringify({}))
    localStorage.setItem('numTags', JSON.stringify(0))
    localStorage.setItem('alliances', JSON.stringify(game.baseAlliances()))
    window.location.hash = "#"
  }

  game.state.add('Boot', BootState);
  game.state.add('GameState', GameState);
  game.state.start('Boot', true, false, game);
}
