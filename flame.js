const Flame = (function() {
  const GRAVITY = 0.3
  const TICK_TIME = 500
  const TICK_DAMAGE = 3
  const NUM_TICKS = 5
  
  function ctor(state, x, y, attached) {
    let self = new Phaser.Sprite(g, x, y, 'flame')
    g.physics.enable(self)
    g.slopes.enable(self)
    self.anchor.set(0.5, 0.5)
    self.x = x
    self.y = y
    self.animations.add('burn', [0,1,2,3], 12, true)
    self.play('burn')
    if(attached) {
      self.startX = self.x - attached.x
      self.startY = self.y - attached.y
    }
    
    self.nextTickTime = g.now + TICK_TIME
    self.ticksLeft = NUM_TICKS
    
    self.update = function() {
      if(attached){
        self.x = attached.x + self.startX
        self.y = attached.y + self.startY
        if(!attached.exists || !attached.alive) { attached = false }
      } else {
        self.body.velocity.y += GRAVITY * g.elapsed
        g.physics.arcade.collide(self, state.tilemapLayer)
        self.body.velocity.x = 0
        g.physics.arcade.overlap(self, state.enemyLayer, function(f,e) {
          if(e.getHit) {
            attached = e
            self.startX = g.rnd.between(-4, 4)
            self.startY = g.rnd.between(-4, 4)
          }
        })
      }
      
      if(g.now > self.nextTickTime) {
        self.nextTickTime = g.now + TICK_TIME
        if(attached && attached.getHit) {
          attached.getHit(TICK_DAMAGE, 0, 0)
        }
        self.ticksLeft -= 1
        if(self.ticksLeft <= 0) { self.destroy() }
      }
    }
    
    return self
  }
  return ctor
})()
